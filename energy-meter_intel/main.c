/*
 * INTEL CONFIDENTIAL
 * Copyright 2011 - 2012 Intel Corporation All Rights Reserved.
 *
 * The source code contained or described herein and all documents related 
 * to the source code ("Material") are owned by Intel Corporation or its 
 * suppliers or licensors. Title to the Material remains with Intel Corporation 
 * or its suppliers and licensors. The Material may contain trade secrets and 
 * proprietary and confidential information of Intel Corporation and its 
 * suppliers and licensors, and is protected by worldwide copyright and trade 
 * secret laws and treaty provisions. No part of the Material may be used, 
 * copied, reproduced, modified, published, uploaded, posted, transmitted, 
 * distributed, or disclosed in any way without Intel's prior express written 
 * permission. 
 * 
 * No license under any patent, copyright, trade secret or other intellectual 
 * property right is granted to or conferred upon you by disclosure or delivery 
 * of the Materials, either expressly, by implication, inducement, estoppel or 
 * otherwise. Any license under such intellectual property rights must be 
 * express and approved by Intel in writing.
 *
 * Unless otherwise agreed by Intel in writing, you may not remove or alter 
 * this notice or any other notice embedded in Materials by Intel or Intel's 
 * suppliers or licensors in any way.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <IntelPowerGadget/EnergyLib.h>

int main(int argc, char* argv[]) {

	IntelEnergyLibInitialize();
	StartLog("/tmp/PowerGadgetLog.csv"); // causes a sample to be read
	
	int numMsrs = 0;
	GetNumMsrs(&numMsrs);
	
	for (int i = 0; i < 10; i++) {
		
		sleep(1);
		ReadSample();
		
		for (int j = 0; j < numMsrs; j++) {
			int funcID;
			char szName[1024];
			GetMsrFunc(j, &funcID);
			GetMsrName(j, szName);
			
			int nData;
			double data[3];
			GetPowerData(0, j, data, &nData);
			
			// Frequency
			if (funcID == MSR_FUNC_FREQ) {
				printf("%s = %4.0f", szName, data[0]);
			}
			
			// Power
			else if (funcID == MSR_FUNC_POWER) {
				printf(", %s Power (W) = %3.2f", szName, data[0]);
				printf(", %s Energy (J) = %3.2f", szName, data[1]);
				printf(", %s Energy (mWh) = %3.2f", szName, data[2]);
			}
			
			// Temperature
			else if (funcID == MSR_FUNC_TEMP) {
				printf(", %s Temp (C) = %3.0f", szName, data[0]);
			}
		}
		printf("\n");
	}
	
	sleep(1);
	StopLog(); // causes a sample to be read
	
	return 0;
}