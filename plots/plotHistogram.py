#from cycler import cycler
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import csv
import os
import re

def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

for dirname, dirnames, filenames in os.walk('./histos'):
    # print path to all subdirectories first.
    for filen in filenames:
        filename =  re.sub(r'.*_(.*).txt', r'\1', filen)
        print(os.path.join(dirname, filen))

        with open('histos/histo_' + filename + '.txt') as f:
            data = []
            for line in f:
                line = line.split() # to deal with blank

                if not isfloat(line[0]): # skip lines with text, probably headers
                    continue

                if line:            # lines (ie skip them)
                    line = [float(i) for i in line]
                    data.append(line)

        lines = np.array(data)
        yy = lines[:,[0,1,2]]
        # yy = lines[:,[0,2]]

        plt.rc('lines', linewidth=3)
        fig, ax0 = plt.subplots(nrows=1)

        ax0.plot(yy[:,0], label = 'Frontier', color='blue')
        ax0.plot(yy[:,2], label = 'Edges', color='red')
        ax0.plot(yy[:,1], label = 'Non-Redundant Edges', color='green')

        # ax0.set_title('Histogram for ' + filename + ' Graph')

        plt.xlabel('Iteration #', fontsize=18)
        plt.ylabel('# of vertices or edges', fontsize=18)

        if yy.shape[0] < 20:
            xticks = list(xrange(yy.shape[0]))
            plt.xticks(xticks)

        # yticks = list(xrange(yy.shape[0]))
        # plt.yticks(yticks)
        # ax0.set_yscale('log')
        plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

        plt.legend(loc="best", ncol=1, shadow=True, fancybox=True)

        # plt.tight_layout()

        # pp = PdfPages('test.pdf')
        newpath = 'histoPlots'
        if not os.path.exists(newpath):
            os.makedirs(newpath)

        histo_format = 'pdf' # 'png'
        plt.savefig('histoPlots/histo_' + filename + '.' + histo_format, format=histo_format)

# os.system("mv histoPlots/*.pdf ../journal/img/")
#os.system("mv histoPlots/histo_youtube.pdf ../paper/img/histo_youtube.pdf")
#os.system("mv histoPlots/histo_roadCA.pdf ../paper/img/histo_roadCA.pdf")
