#from cycler import cycler
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import csv
import os
import re

def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

def createPlot(filename, rootDir):

    format_output = 'pdf'

    inputfile  = rootDir + '/histoExes/histo_exe_' + filename + '.txt'
    outputfile = rootDir + '/histoExePlots/histo_' + filename + '.' + format_output

    with open(inputfile) as f:
        data = []
        for line in f:
            line = line.split() # to deal with blank

            if not isfloat(line[0]): # skip lines with text, probably headers
                continue

            if line:            # lines (ie skip them)
                line = [float(i) for i in line]
                data.append(line)

    lines = np.array(data)
    yy = lines

    plt.rc('lines', linewidth=3)
    fig, ax0 = plt.subplots(nrows=1)

    with open(inputfile) as f:
        for line in f:
            line = line.split('\t') # to deal with blank
            # print line
            break

    bar_width = 0.2
    n_groups = yy.shape[0]
    index = np.arange(n_groups)
    opacity = 0.7

    color = ['b', 'r', 'g', 'orange']
    error_config = {'ecolor': '0.3'}

    # print line
    line = ['F', 'CPU TD', 'C', 'CPU BU', 'C', 'GPU TD', 'G', 'GPU TD', 'G', 'GPU BU', 'G', '\n']

    toPlot = [1,3,7,9]
    counter = 0;

    for i in toPlot:

        plt.bar(index + counter*bar_width, yy[:,i], bar_width,
                alpha=opacity,
                color=color[counter],
                # yerr=yy[:,1+2*i+1],
                # error_kw=error_config,
                label=line[i])
        counter = counter + 1

    ax0.set_title('Histogram for ' + filename + ' Graph')

    plt.xlabel('Iteration #', fontsize=12)
    plt.ylabel('Execution Time (ms)', fontsize=12)

    if yy.shape[0] < 20:
        xticks = list(xrange(yy.shape[0]))
        plt.xticks(xticks)

    # plt.legend(loc="best", ncol=1, shadow=True, fancybox=True)
    plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.12),
      fancybox=True, shadow=True, ncol=5, fontsize=8)
    # plt.tight_layout()

    # pp = PdfPages('test.pdf')

    # plt.savefig('histoExePlots/histo_' + filename + '.pdf', format='pdf')
    plt.savefig(outputfile, format=format_output, bbox_inches='tight')
    plt.close()


dirs = ['results_i5', 'results_i7', 'results_odroid']

for rootDir in dirs:

    newpath = rootDir + '/histoExePlots'
    if not os.path.exists(newpath):
        os.makedirs(newpath)

    for dirname, dirnames, filenames in os.walk(rootDir + '/histoExes'):
        # print path to all subdirectories first.

            for filen in filenames:
                filename =  re.sub(r'.*_exe_(.*).txt', r'\1', filen)
                if filename != 'graph16M':
                    continue
                if filen == filename:
                    continue
                print(os.path.join(dirname, filen))
                createPlot(filename, rootDir)


# os.system('cp results_i7/histoExePlots/histo_graph16M.pdf ../paper/img/histo_exe_graph16M_i7.pdf')
# os.system('cp results_i5/histoExePlots/histo_graph16M.pdf ../paper/img/histo_exe_graph16M_i5.pdf')















