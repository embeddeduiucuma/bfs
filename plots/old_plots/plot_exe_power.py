#from cycler import cycler
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import csv
import os
import re

def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

# for dirname, dirnames, filenames in os.walk('./histos'):
#     # print path to all subdirectories first.
#     for filen in filenames:
#         filename =  re.sub(r'.*_(.*).txt', r'\1', filen)
#         print(os.path.join(dirname, filen))

filename = 'exe_test.txt'

with open(filename) as f:
    data = []
    for line in f:
        line = line.split() # to deal with blank 

        if not isfloat(line[0]): # skip lines with text, probably headers
            continue

        if line:            # lines (ie skip them)
            line = [float(i) for i in line]
            data.append(line)

with open(filename) as f:
    for line in f:
        line = line.split() # just read the first line
        label = line
        break;

graphs = []
counter = 0
with open(filename) as f:
    for line in f:
        line = line.split() # just read the first line

        if isfloat(line[0]): # skip lines with text, probably headers
            continue
        graphs.append(line[0])
        counter = counter + 1

print graphs
print data

data = np.array(data)
data = data.transpose()

bar_width = 0.1
n_groups = data.shape[0]
index = np.arange(n_groups)
opacity = 0.7

color = ['b', 'r', 'g', 'orange', 'purple', 'black']
error_config = {'ecolor': '0.3'}

print label
print data

plt.rc('lines', linewidth=3)
fig, ax0 = plt.subplots(nrows=1)

for i in range(data.shape[0]):

    plt.bar(index + i*bar_width, data[i,:], bar_width,
            alpha=opacity,
            color=color[i],
            # yerr=yy[:,1+2*i+1],
            # error_kw=error_config,
            label=label[i])

ax0.set_title('Histogram for ' + filename + ' Graph')

plt.xlabel('Iteration #', fontsize=12)
plt.ylabel('Execution Time (ms)', fontsize=12)

graphs.pop(0)
xticks = graphs
plt.xticks(range(data.shape[0]), xticks, rotation="vertical")

# plt.legend(loc="best", ncol=1, shadow=True, fancybox=True)
plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.12),
fancybox=True, shadow=True, ncol=6, fontsize=8)
# plt.tight_layout()

# pp = PdfPages('test.pdf')

newpath = 'histoExePlots' 
if not os.path.exists(newpath):
    os.makedirs(newpath)

# plt.savefig('histoExePlots/histo_' + filename + '.pdf', format='pdf')
plt.savefig('exe_power.png', format='png')
