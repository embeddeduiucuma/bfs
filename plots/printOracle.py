#from cycler import cycler
import numpy as np
import csv
import os
import re
import csv
import os
import re
import subprocess

graphsDir = '/home/luisremis/researchData/GaloisGraphs'
graphsHistoDir = '/home/luisremis/researchData/Graphs'
GaloisExe = '/home/luisremis/Galois-2.2.1/apps/bfs/bfs'
HistoExe  = '/home/luisremis/bfs/histogram/histo'

def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

def createPlot(filename, rootDir):

    inputfile  = rootDir + '/histoExes/histo_exe_' + filename + '.txt'
    outputfile = rootDir + '/histoExePlots/histo_' + filename + '.pdf'

    with open(inputfile) as f:
        data = []
        for line in f:
            line = line.split() # to deal with blank 

            if not isfloat(line[0]): # skip lines with text, probably headers
                continue

            if line:            # lines (ie skip them)
                line = [float(i) for i in line]
                data.append(line)

    lines = np.array(data)
    yy = lines
    dataonly = np.delete(yy, np.s_[::2], 1)

    maxis = np.amin(dataonly, 1)

    phisto = subprocess.Popen(HistoExe + ' 4 ' + os.path.join(graphsHistoDir, filename) + '.graph 0' ,
            shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    flagedges = False
    output = ''
    for line in phisto.stdout.readlines():
            edges =  re.sub(r'Graph Edges: (.*)', r'\1', line)
            output = output + line

            if edges != line:
                # print float(nodes)/1e6
                flagedges = True
                no_edges = float(edges)/1e6

    if flagedges:
            print filename  + '\t' + str(no_edges / (np.sum(maxis)*1e-3) )
            #print output
    else:
        print 'Error processing ' + filename
        #print output


    retval = phisto.wait()


dirs = ['results_i5', 'results_i7', 'results_odroid']

for rootDir in dirs:
    print rootDir + '-----------'

    newpath = rootDir + '/histoExePlots' 
    if not os.path.exists(newpath):
        os.makedirs(newpath)

    for dirname, dirnames, filenames in os.walk(rootDir + '/histoExes'):
        # print path to all subdirectories first.

            for filen in filenames:
                filename =  re.sub(r'.*_exe_(.*).txt', r'\1', filen)
                if filen == filename:
                    continue
                #print(os.path.join(dirname, filen))
                createPlot(filename, rootDir)





















        