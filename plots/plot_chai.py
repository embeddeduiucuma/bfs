#from cycler import cycler
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import csv
import os
import re

def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

def createPlotPerf(filename, form):

    inputfile  = filen

    platf =  re.sub(r'.*_(.*)_.*.txt', r'\1', filename)

    print platf

    with open(inputfile) as f:
        data = []
        for line in f:
            line = line.split('\t') # to deal with blank

            if line:            # lines (ie skip them)
                #line = [float(i) for i in line]
                data.append(line)

    graphs = []
    for i in range(len(data)-1):
        graphs.append(data[i+1][0])

    labels = []
    for i in range(len(data[0])-1):
        labels.append(data[0][i+1].rstrip())

    val = []

    for i in range(len(data)-1):
        # print data[i]
        new = []
        for j in range(len(data[0])-1):
            new.append(data[i+1][j+1])
        val.append(new)

    # print labels
    # print graphs

    for i in range(len(val)):
        val[i] = [float(j) for j in val[i]]
        # print val[i]


    lines = np.array(val)
    yy = lines

    fig, ax0 = plt.subplots(nrows=1)
    fig.set_size_inches(12, 3)


    bar_width = 0.12
    n_groups = yy.shape[0]
    index = np.arange(n_groups)
    opacity = 0.7

    color = ['b', 'r', 'g', 'orange', 'yellow', 'purple', 'pink', 'brown']
    error_config = {'ecolor': '0.3'}

    patterns = [ "/" ,"+" , "x", "o", "O", ".", "*",  "\\" , "|" , "-" ]


    for i in range(yy.shape[1]):

        plt.bar(index + i*bar_width, yy[:,i], bar_width,
                alpha=opacity,
                color=color[i],
                hatch=patterns[i],
                # yerr=yy[:,1+2*i+1],
                # error_kw=error_config,
                label=labels[i])

    # ax0.set_title('Performance on ' + platf)

    plt.ylabel('Millions of Traversed Edges per Second', fontsize=8)
    ax0.set_xticks(index + bar_width*2)
    ax0.set_xticklabels(graphs, rotation=45, fontsize=10)
    ax0.tick_params(axis='y', labelsize=8)

    plt.legend(loc="best", ncol=1, shadow=True, fancybox=True, fontsize=8)
    # plt.legend(loc='best', bbox_to_anchor=(0.5, 1.12),
    #   fancybox=True, shadow=True, ncol=5, fontsize=8)
    # plt.tight_layout()

    # pp = PdfPages('test.pdf')

    # plt.savefig('histoExePlots/histo_' + filename + '.pdf', format='pdf')
    for fo in form:
        outputfile = platf + '_perfo'+ '.' + fo
        plt.savefig(outputfile, format=fo, bbox_inches='tight')
    plt.close()

def createPlotComparison(filename, form):

    inputfile  = filename

    platf =  re.sub(r'.*_(.*)_.*.txt', r'\1', filename)

    print platf

    with open(inputfile) as f:
        data = []
        for line in f:
            line = line.split('\t') # to deal with blank

            if line:            # lines (ie skip them)
                #line = [float(i) for i in line]
                data.append(line)

    graphs = []
    for i in range(len(data)-1):
        graphs.append(data[i+1][0])

    labels = []
    for i in range(len(data[0])-1):
        labels.append(data[0][i+1].rstrip())

    val = []

    for i in range(len(data)-1):
        # print data[i]
        new = []
        for j in range(len(data[0])-1):
            new.append(data[i+1][j+1])
        val.append(new)

    # print labels
    # print graphs

    for i in range(len(val)):
        val[i] = [float(j) for j in val[i]]
        # print val[i]


    lines = np.array(val)
    yy = lines

    fig, ax0 = plt.subplots(nrows=1)
    fig.set_size_inches(12, 3)


    bar_width = 0.12
    n_groups = yy.shape[0]
    index = np.arange(n_groups)
    opacity = 0.7

    color = ['b', 'r', 'g', 'orange', 'yellow', 'purple', 'pink', 'brown']
    error_config = {'ecolor': '0.3'}

    patterns = [ "/" ,"+" , "x", "o", "O", ".", "*",  "\\" , "|" , "-" ]


    for i in range(yy.shape[1]):

        plt.bar(index + i*bar_width, yy[:,i], bar_width,
                alpha=opacity,
                color=color[i],
                hatch=patterns[i],
                # yerr=yy[:,1+2*i+1],
                # error_kw=error_config,
                label=labels[i])

    # ax0.set_title('Performance on ' + platf)

    plt.ylabel('Millions of Traversed Edges per Second', fontsize=8)
    ax0.set_xticks(index + bar_width*2)
    ax0.set_xticklabels(graphs, rotation=45, fontsize=10)
    ax0.tick_params(axis='y', labelsize=8)

    plt.legend(loc="best", ncol=1, shadow=True, fancybox=True, fontsize=8)
    # plt.legend(loc='best', bbox_to_anchor=(0.5, 1.12),
    #   fancybox=True, shadow=True, ncol=5, fontsize=8)
    # plt.tight_layout()

    # pp = PdfPages('test.pdf')

    # plt.savefig('histoExePlots/histo_' + filename + '.pdf', format='pdf')
    for fo in form:
        outputfile = platf + '_perfo_comp'+ '.' + fo
        plt.savefig(outputfile, format=fo, bbox_inches='tight')
    plt.close()

def createPlotPower(filename, form):

    inputfile  = filen

    platf =  re.sub(r'.*_(.*)_.*.txt', r'\1', filename)

    print platf

    with open(inputfile) as f:
        data = []
        for line in f:
            line = line.split('\t') # to deal with blank

            if line:            # lines (ie skip them)
                #line = [float(i) for i in line]
                data.append(line)

    graphs = []
    for i in range(len(data)-1):
        graphs.append(data[i+1][0])

    labels = []
    for i in range(len(data[0])-1):
        labels.append(data[0][i+1].rstrip())

    val = []

    for i in range(len(data)-1):
        # print data[i]
        new = []
        for j in range(len(data[0])-1):
            new.append(data[i+1][j+1])
        val.append(new)

    # print labels
    # print graphs

    for i in range(len(val)):
        val[i] = [float(j) for j in val[i]]
        # print val[i]


    lines = np.array(val)
    yy = lines

    fig, ax0 = plt.subplots(nrows=1)
    fig.set_size_inches(12, 3)


    bar_width = 0.12
    n_groups = yy.shape[0]
    index = np.arange(n_groups)
    opacity = 0.7

    color = ['b', 'r', 'g', 'orange', 'yellow', 'purple', 'pink']
    error_config = {'ecolor': '0.3'}

    patterns = [ "/" ,"+" , "x", "o", "O", ".", "*",  "\\" , "|" , "-" ]


    for i in range(yy.shape[1]):

        plt.bar(index + i*bar_width, yy[:,i], bar_width,
                alpha=opacity,
                color=color[i],
                hatch=patterns[i],
                # yerr=yy[:,1+2*i+1],
                # error_kw=error_config,
                label=labels[i])

    # ax0.set_title('Power on ' + platf)

    # plt.xlabel('Iteration #', fontsize=12)
    plt.ylabel('Millions of Traversed Edges per Joule', fontsize=8)
    ax0.set_xticks(index + bar_width*2)
    ax0.set_xticklabels(graphs, rotation=45, fontsize=10)
    ax0.tick_params(axis='y', labelsize=8)

    # plt.legend(loc="best", ncol=1, shadow=True, fancybox=True)
    plt.legend(loc='best',
      fancybox=True, shadow=True, ncol=5, fontsize=8)
    # plt.tight_layout()

    # pp = PdfPages('test.pdf')

    # plt.savefig('histoExePlots/histo_' + filename + '.pdf', format='pdf')
    for fo in form:
        outputfile = "res_plots/" + platf + '_power'+ '.' + fo
        plt.savefig(outputfile, format=fo, bbox_inches='tight')
    plt.close()


form = ['png','pdf']


createPlotComparison('chai_kaveri.txt', form)
createPlotComparison('chai_i7.txt', form)























