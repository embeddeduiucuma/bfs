#from cycler import cycler
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import csv
import os
import re

color = ['#000099', '#3333ff', 'g',
         '#ff6600', '#cc0000',
         'purple', 'pink', '#000000']

patterns = [ "/" ,"+" , "x", "o", "O", ".", "*",  "\\" , "|" , "-" ]

def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

def createPlotPerf(filename, form):

    inputfile  = filen

    platf =  re.sub(r'.*_(.*)_.*.txt', r'\1', filename)

    with open(inputfile) as f:
        data = []
        for line in f:
            line = line.split('\t') # to deal with blank

            if line:            # lines (ie skip them)
                #line = [float(i) for i in line]
                data.append(line)

    graphs = []
    for i in range(len(data)-1):
        graphs.append(data[i+1][0])

    labels = []
    for i in range(len(data[0])-1):
        labels.append(data[0][i+1].rstrip())

    val = []

    for i in range(len(data)-1):
        new = []
        for j in range(len(data[0])-1):
            new.append(data[i+1][j+1])
        val.append(new)

    for i in range(len(val)):
        val[i] = [float(j) for j in val[i]]
        # print val[i]

    lines = np.array(val)
    yy = lines

    fig, ax0 = plt.subplots(nrows=1)
    fig.set_size_inches(12, 3)

    bar_width = 0.09
    n_groups = yy.shape[0]
    index = np.arange(n_groups)
    opacity = 0.7

    error_config = {'ecolor': '0.3'}

    for i in range(yy.shape[1]):

        color_bar = color[i]
        if labels[i] == "Oracle":
            color_bar = '#000000'

        plt.bar(index + i*bar_width, yy[:,i], bar_width,
                alpha=opacity,
                color=color_bar,
                # hatch=patterns[i],
                # yerr=yy[:,1+2*i+1],
                # error_kw=error_config,
                label=labels[i])

    # ax0.set_title('Performance on ' + platf)

    plt.ylabel('MTEPS', fontsize=12)
    ax0.set_xticks(index + bar_width*4)
    ax0.set_xticklabels(graphs, rotation=45, fontsize=10)
    ax0.tick_params(axis='y', labelsize=10)

    plt.legend(ncol=10, shadow=True, fancybox=True, fontsize=9.5,
               bbox_to_anchor=(1.0, 1.15))
    # plt.tight_layout()

    # pp = PdfPages('test.pdf')

    # plt.savefig('histoExePlots/histo_' + filename + '.pdf', format='pdf')
    for fo in form:
        outputfile = "res_plots/" + platf + '_perfo'+ '.' + fo
        plt.savefig(outputfile, format=fo, bbox_inches='tight')
    plt.close()

def createPlotComparison(filename, form):

    inputfile  = filename

    platf =  re.sub(r".*\/(.*).txt", r'\1', filename)

    with open(inputfile) as f:
        data = []
        for line in f:
            line = line.split('\t') # to deal with blank

            if line:            # lines (ie skip them)
                #line = [float(i) for i in line]
                data.append(line)

    graphs = []
    for i in range(len(data)-1):
        graphs.append(data[i+1][0])

    labels = []
    for i in range(len(data[0])-1):
        labels.append(data[0][i+1].rstrip())

    val = []

    for i in range(len(data)-1):
        new = []
        for j in range(len(data[0])-1):
            new.append(data[i+1][j+1])
        val.append(new)

    for i in range(len(val)):
        val[i] = [float(j) for j in val[i]]
        # print val[i]

    lines = np.array(val)
    yy = lines

    fig, ax0 = plt.subplots(nrows=1)
    fig.set_size_inches(12, 3)

    bar_width = 0.1
    n_groups = yy.shape[0]
    index = np.arange(n_groups)
    opacity = 0.7

    error_config = {'ecolor': '0.3'}

    for i in range(yy.shape[1]):

        plt.bar(index + i*bar_width, yy[:,i], bar_width,
                alpha=opacity,
                color=color[i],
                # hatch=patterns[i],
                # yerr=yy[:,1+2*i+1],
                # error_kw=error_config,
                label=labels[i])

    # ax0.set_title('Performance on ' + platf)

    plt.ylabel('MTEPS', fontsize=12)
    ax0.set_xticks(index + bar_width*3)
    ax0.set_xticklabels(graphs, rotation=0, fontsize=12)
    ax0.tick_params(axis='y', labelsize=10)

    plt.legend(ncol=10,
               shadow=True,
               fancybox=True,
               fontsize=10,
               # loc='best', shadow=True,
               bbox_to_anchor=(0.9, 1.15))
    # plt.tight_layout()

    for fo in form:
        outputfile = "res_plots/" + platf + '_perfo'+ '.' + fo
        plt.savefig(outputfile, format=fo, bbox_inches='tight')
    plt.close()

def createPlotPower(filename, form):

    inputfile  = filen

    platf =  re.sub(r'.*_(.*)_.*.txt', r'\1', filename)

    with open(inputfile) as f:
        data = []
        for line in f:
            line = line.split('\t') # to deal with blank

            if line:            # lines (ie skip them)
                #line = [float(i) for i in line]
                data.append(line)

    graphs = []
    for i in range(len(data)-1):
        graphs.append(data[i+1][0])

    labels = []
    for i in range(len(data[0])-1):
        labels.append(data[0][i+1].rstrip())

    val = []

    for i in range(len(data)-1):
        new = []
        for j in range(len(data[0])-1):
            new.append(data[i+1][j+1])
        val.append(new)

    for i in range(len(val)):
        val[i] = [float(j) for j in val[i]]


    lines = np.array(val)
    yy = lines

    fig, ax0 = plt.subplots(nrows=1)
    fig.set_size_inches(12, 3)

    bar_width = 0.15
    n_groups = yy.shape[0]
    index = np.arange(n_groups)
    opacity = 0.7

    error_config = {'ecolor': '0.3'}

    for i in range(yy.shape[1]):

        plt.bar(index + i*bar_width, yy[:,i], bar_width,
                alpha=opacity,
                color=color[i],
                # hatch=patterns[i],
                # yerr=yy[:,1+2*i+1],
                # error_kw=error_config,
                label=labels[i])

    # ax0.set_title('Power on ' + platf)

    # plt.xlabel('Iteration #', fontsize=12)
    plt.ylabel('MTEPJ', fontsize=12)
    ax0.set_xticks(index + bar_width*2.5)
    ax0.set_xticklabels(graphs, rotation=0, fontsize=10)
    ax0.tick_params(axis='y', labelsize=10)

    plt.legend(ncol=10,
               shadow=True,
               fancybox=True,
               fontsize=10,
               # loc='best', shadow=True,
               bbox_to_anchor=(0.85, 1.15))
    # plt.tight_layout()

    for fo in form:
        outputfile = "res_plots/" + platf + '_power'+ '.' + fo
        plt.savefig(outputfile, format=fo, bbox_inches='tight')
    plt.close()


filesPerfo = ['results/plot_i7_perf.txt',  'results/plot_odroid_perf.txt']
filesPower = ['results/plot_i7_power.txt', 'results/plot_odroid_power.txt']

form = ['pdf']

print "Generating Performance Plot..."

for filen in filesPerfo:
    createPlotPerf(filen, form)

print "Generating Power Plot..."

for filen in filesPower:
    createPlotPower(filen, form)

# createPlotComparison('chai_kaveri.txt', form)
# createPlotComparison('chai_i7.txt', form)
createPlotComparison('results/i7_roads.txt', form)
createPlotComparison('results/kaveri_roads.txt', form)
