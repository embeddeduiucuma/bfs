#from cycler import cycler
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import csv
import os
import re

color = ['#000099', 'g','#ff6600']
patterns = [ "/" ,"+" , "x", "o", "O", ".", "*",  "\\" , "|" , "-" ]

def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

def createPlotMemory(filename, form, legend):

    inputfile  = filename
    filename =  re.sub(r".*\/(.*).txt", r'\1', filename)

    with open(inputfile) as f:
        data = []
        for line in f:
            line = line.split('\t') # to deal with blank

            if line:            # lines (ie skip them)
                #line = [float(i) for i in line]
                data.append(line)

    xlabels = []
    for i in range(len(data)-1):
        xlabels.append(data[i+1][0])

    labels = []
    for i in range(len(data[0])-1):
        labels.append(data[0][i+1].rstrip())

    val = []

    for i in range(len(data)-1):
        # print data[i]
        new = []
        for j in range(len(data[0])-1):
            new.append(data[i+1][j+1])
        val.append(new)

    # print labels
    # print xlabels

    for i in range(len(val)):
        val[i] = [float(j) for j in val[i]]
        # print val[i]


    lines = np.array(val)
    yy = lines

    fig, ax0 = plt.subplots(nrows=1)
    fig.set_size_inches(6, 3)

    bar_width = 0.3
    n_groups = yy.shape[0]
    index = np.arange(n_groups)
    opacity = 0.7

    error_config = {'ecolor': '0.3'}

    for i in range(yy.shape[1]):

        plt.bar(index + i*bar_width, yy[:,i], bar_width,
                alpha=opacity,
                color=color[i],
                #hatch=patterns[i],
                # yerr=yy[:,1+2*i+1],
                # error_kw=error_config,
                label=labels[i])

    plt.ylabel('Execution Time (ms)', fontsize=14)
    plt.xlabel('% of the frontier processed by the GPU ', fontsize=14)
    ax0.set_xticks(index + bar_width*1.5)
    ax0.set_xticklabels(xlabels, fontsize=12)
    ax0.tick_params(axis='y', labelsize=12)

    if legend:
        plt.legend(loc='center', bbox_to_anchor=(0.5, 1.09),
          fancybox=True, shadow=True, ncol=3, fontsize=11)

    for fo in form:
        outputfile = "res_plots/" + filename + '.' + fo
        plt.savefig(outputfile, format=fo, bbox_inches='tight')
    plt.close()

files = ['results/16M_memory_bound.txt', 'results/16M_compute_bound.txt']

form = ['pdf']
legend =[True, False]
counter = 0;

print "Generating Memory/Compute Bound Plot..."

for filen in files:
    createPlotMemory(filen, form, True)
    counter = counter+1























