/* ============================================================
University of Illinois at Urbana Champaign
Breadth-First-Search Optimized - Histograms
Author: Luis Remis
Date:	Feb 2016
============================================================ */
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>

#include "readGraph.h"
#include "Histogram.h"

int main(int argc, char * argv[])
{

	int num_omp_threads = atoi(argv[1]);
	std::string input_f (argv[2]);
		
	if (argc < 4)
	{
		std::cout << "Select mode: "<< std::endl;
		std::cout << "0 - Graph Profile" << std::endl;
		std::cout << "1 - Execution Time Profile" << std::endl;
		std::cout << "2 - Zero Copy test" << std::endl;
		std::cout << "3 - Repetition Histogram" << std::endl;
		std::cout << "4 - Run BFS from multiple source" << std::endl;
		std::cout << "5 - Run all histograms" << std::endl;
		exit(0);
	}

	int mode = atoi(argv[3]);	
	
	struct Graph* graph;
	graph = readGraphFromFile(input_f.c_str());

	Histogram  h(graph, num_omp_threads);

	switch(mode)
	{
		case 0: h.generateGraphProfile();
				break;

		case 1: h.generateExeTimeHisto();
				break;

		case 2: h.testZCPerformance();
				break;

		case 3: h.repetitionHisto();
				break;

		case 4: h.multiSource();
				break;
		case 5: h.run(graph);
				break;
	}

    return 0;
}

