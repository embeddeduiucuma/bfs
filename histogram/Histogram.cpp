/* ============================================================
University of Illinois at Urbana Champaign
Breadth-First-Search Optimized
Author: Luis Remis
Date:   Apr 5 2015
============================================================ */
#include <cstdlib>
#include <iostream>
#include <string>
#include <sstream>
#include <cstring>
#include <iomanip>
#include <fstream>

using namespace std;

#include "Histogram.h"

#include "util.h"
#include "CLHelper.h"
#include "readGraph.h"

#ifdef  PROFILING_ALL
#define PROFILING
#endif

#ifdef  PROFILING
#include "ChronoCpu.h"
#endif

#define MAX_THREADS_PER_BLOCK 32
#define NUMBER_OF_ITERATION 10

Histogram::Histogram(struct Graph* graph_, uint32_t num_omp_threads) :
    BFS_base(0,num_omp_threads)
{
    this->graph = graph_;
    uint32_t no_of_nodes = graph->no_of_nodes;
    for (int i = 0; i < num_omp_threads; ++i)
    {
        localFrontier[i] = new int[no_of_nodes / 2]; //May be dangerous!
    }

    std::cout << "Graph Name: " << graph->name << std::endl;
    std::cout << "Graph Nodes: "  << graph->no_of_nodes     << std::endl;
    std::cout << "Graph Edges: "  << graph->no_of_edges  << std::endl;
    std::cout << "Graph Source: " << graph->source          << std::endl;
    
    checkGraph(graph); // To detect unconnected nodes.
}

Histogram::~Histogram()
{
    for (int i = 0; i < num_omp_threads; ++i)
    {
        delete localFrontier[i];
    }
}

void Histogram::RWTest( std::vector<uint32_t> sizes)
{
    std::ofstream output("read_write_test.txt");

    output << "Size,CPU_malloc,Concurrent,GPU_only,CPU_ocl" << std::endl;

    for (int i = 0; i < sizes.size(); ++i)
    {
        uint32_t testSize = sizes.at(i);

        output << testSize / 1024 / 1024 << ",";

        char kernel_file[100]  = "Kernels.cl";
        int total_kernels = 2;
        std::string kernel_names[2] = {"simple_write_odd", "simple_write_all"};

        _clInit(kernel_file, total_kernels, kernel_names);

        cl_mem test_zc; 
        char* h_test;
        char* h_test_map;

        ChronoCpu chrono_cpu_only("total_timer");
        ChronoCpu chrono_concurrent("total_timer");
        ChronoCpu chrono_cpu("total_timer");
        ChronoCpu chrono_gpu("total_timer");

        h_test  = new char[testSize];
        memset((void *)h_test, 23,testSize);

        test_zc = _clMallocRWZC(testSize, h_test);

        // GPU warm up
        _clSetArgs(1, 0, test_zc);
        _clInvokeKernel(1, testSize/2, MAX_THREADS_PER_BLOCK);

        h_test_map = (char*)clEnqueueMapBuffer(getOCLHandler()->queue, test_zc, 
            CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, testSize, 0, NULL, NULL, NULL);

        _clFinish();
        for (uint32_t i = 0; i < testSize; i ++)
        {
            h_test_map[i] = 1;
        }
        memset((void *)h_test_map, 23,testSize);
        // End warming up

        // CPU MALLOC ONLY TEST
        chrono_cpu_only.tic();

        for (uint32_t i = 0; i < testSize; ++i)
        {
            h_test_map[i] = 1;
        }

        chrono_cpu_only.tac();
        output << chrono_cpu_only.getElapsedStats().lastTime_ms << ",";
        // END CPU MALLOC ONLY TEST

        chrono_concurrent.tic();

        int kernel_id  = 0;
        int kernel_idx = 0;
        int partition = testSize / 2;
        _clSetArgs(kernel_id, kernel_idx++, test_zc);
        _clInvokeKernel(kernel_id, partition, MAX_THREADS_PER_BLOCK);
        _clFlush();

        for (uint32_t i = 0; i < testSize; i = i+2)
        {
            //if(i%2 ==  0) h_test_map[i] = 1;
            h_test_map[i] = 23;
        }

        _clFinish();
        chrono_concurrent.tac();
        output << chrono_concurrent.getElapsedStats().lastTime_ms << ",";

        bool flag = false;
        int counter1 = 0;
        int counter2 = 0;

        for (int i = 0; i < testSize; ++i)
        {
            if ( ((uint32_t)h_test_map[i] %0xFF ) != 23 && i%2 == 0)
            // if ( i >= partition && ((uint32_t)h_test_map[i] %0xFF ) != 1)
            {
                flag = true;
                ++counter1;
                //break;
                // std::cout << i << " " << ((uint32_t)h_test_map[i])%0xFF  << " " << i%0xFF << std::endl;
            }
            if ( ((uint32_t)h_test_map[i] %0xFF ) != 2 && i%2 == 1)
            // if ( i < partition && ((uint32_t)h_test_map[i] %0xFF ) != 2)
            {
                flag = true;
                ++counter2;
                //break;
                // std::cout << i << " " << ((uint32_t)h_test_map[i])%0xFF  << " " << i%0xFF << std::endl;
            }
        }

        if(flag)
        {
            std::cout << "Correcness Concurrent failed! " << counter1 << " " << counter2 << std::endl;
        }


        clEnqueueUnmapMemObject(getOCLHandler()->queue, test_zc, h_test_map, 0, NULL, NULL);
        kernel_id  = 1;
        kernel_idx = 0;
        _clSetArgs(kernel_id, kernel_idx++, test_zc);
        _clInvokeKernel(kernel_id, testSize, MAX_THREADS_PER_BLOCK);
        _clFinish();

        chrono_gpu.tic();
        kernel_id  = 1;
        kernel_idx = 0;
        _clSetArgs(kernel_id, kernel_idx++, test_zc);
        _clInvokeKernel(kernel_id, testSize, MAX_THREADS_PER_BLOCK);
        _clFinish();
        chrono_gpu.tac();
        output << chrono_gpu.getElapsedStats().lastTime_ms << ",";

        flag = false;

        h_test_map = (char*)clEnqueueMapBuffer(getOCLHandler()->queue, test_zc, 
            CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, testSize, 0, NULL, NULL, NULL);
        _clFinish();

        for (int i = 0; i < testSize; ++i)
        {
            if ( ((uint32_t)h_test_map[i] %0xFF ) != 2 )
            {
                flag = true;
                break;
            }
        }

        if(flag)
        {
            std::cout << "Correcness GPU failed! " << std::endl;
        }


        chrono_cpu.tic();
        for (uint32_t i = 0; i < testSize; ++i)
        {
            h_test_map[i] = 1;
        }

        chrono_cpu.tac();
        output << chrono_cpu.getElapsedStats().lastTime_ms << ",";
        output << std::endl;

        _clFree(test_zc);
        delete h_test;
        _clRelease();

    }
    
}

void Histogram::CopyAndMapTest(std::vector<uint32_t> sizes)
{

    std::ofstream output("copy_and_map.txt");

    output << "Action,H2D,D2H,Map,UnMap" << std::endl;

    for (int i = 0; i < sizes.size(); ++i)
    {
        uint32_t testSize = sizes.at(i);

        output << testSize / 1024 / 1024 << ",";

        char kernel_file[100]  = "Kernels.cl";
        int total_kernels = 2;
        std::string kernel_names[2] = {"simple_write_odd", "simple_write_all"};

        // std::cout << "About to init GPU" << std::endl;
        _clInit(kernel_file, total_kernels, kernel_names);
        // std::cout << "GPU init done" << std::endl;

        cl_mem test_zc; 
        cl_mem d_test;
        char* h_test;
        char* h_test_map;

        ChronoCpu chrono("total_timer");

        h_test  = new char[testSize];

        d_test  = _clMallocRW(testSize, h_test);
        test_zc = _clMallocRWZC(testSize, h_test);

        chrono.tic();
        _clMemcpyH2D(d_test, testSize, h_test);
        _clFinish();
        chrono.tac();
        output << chrono.getElapsedStats().lastTime_ms << ",";

        for (uint32_t i = 0; i < testSize; ++i) h_test[i] = rand()%10;

        chrono.tic();
        _clMemcpyD2H(d_test, testSize, h_test);
        _clFinish();
        chrono.tac();
        output << chrono.getElapsedStats().lastTime_ms << ",";

        for (uint32_t i = 0; i < testSize; ++i) h_test[i] = rand()%10;

        chrono.tic();
        h_test_map = (char*)clEnqueueMapBuffer(getOCLHandler()->queue, test_zc, 
            CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, testSize, 0, NULL, NULL, NULL);    
        _clFinish();
        chrono.tac();
        // std::cout << "Map: " << chrono.getElapsedStats().lastTime_ms << std::endl;
        output << chrono.getElapsedStats().lastTime_ms << ",";

        for (uint32_t i = 0; i < testSize; ++i) h_test_map[i] = rand()%10;

        chrono.tic();
        clEnqueueUnmapMemObject(getOCLHandler()->queue, test_zc, h_test_map, 0, NULL, NULL);
        _clFinish();
        chrono.tac();
        output << chrono.getElapsedStats().lastTime_ms;

        output << std::endl;

        _clFree(d_test);
        _clFree(test_zc);

        delete h_test;
        _clRelease();
    }

}

void Histogram::testZCPerformance()
{
    std::vector<uint32_t> sizes;

    sizes.push_back(1024*1024*1);
    sizes.push_back(1024*1024*2);
    sizes.push_back(1024*1024*4);
    sizes.push_back(1024*1024*8);
    sizes.push_back(1024*1024*16);
    sizes.push_back(1024*1024*32);
    sizes.push_back(1024*1024*64);

    // sizes.push_back(1024*1024*128);
    // sizes.push_back(1024*1024*256);
    // sizes.push_back(1024*1024*256*2);
    // sizes.push_back(1024*1024*256*4);

    RWTest(sizes);
    CopyAndMapTest(sizes);
}

void Histogram::run(struct Graph* graph_)
{
    std::cout << "Graph Info: " << std::endl;
    std::cout << "No of Nodes: " << graph->no_of_nodes << endl;
    std::cout << "No of Edges: " << graph->no_of_edges << endl;

    std::cout << "Graph Profile" << std::endl;
    generateGraphProfile();
    std::cout << "Execution Time analysis" << std::endl;
    generateExeTimeHisto();
    std::cout << "Repetition Histogram" << std::endl;
    repetitionHisto();
}

bool compare(int* h_cost1, int* h_cost2, size_t size)
{
    uint32_t counter = 0;
    for(int i = 0; i < size; i++ )
    {
        if(h_cost1[i] != h_cost2[i]){
            counter ++;
        }
    }

    if (counter > 1){
        std::cout << "Counter Compare: " << counter << std::endl;
        return false;
    }

    return true;
}

//----------------------------------------------------------
//--breadth first search Histogram implementation 
//----------------------------------------------------------
void Histogram::generateGraphProfile() 
{
    uint32_t no_of_nodes = graph->no_of_nodes;  
    uint32_t* h_graph_nodes = graph->nodes; 
    uint32_t no_of_edges = graph->no_of_edges;
    uint32_t *h_graph_edges = graph->edges;
    uint32_t source = graph->source;

    char* h_graph_mask       = new char[no_of_nodes];
    int*  h_cost             = new int [no_of_nodes];
    int*  h_frontier         = new int [no_of_nodes];
    char* repeated_mask      = new char[no_of_nodes];

    for (uint32_t i = 0; i < no_of_nodes; ++i)
    {
        h_graph_mask[i] = false;
        h_cost[i] = NODE_UNVISITED;
    }

    h_graph_mask[source]=true;
    h_cost[source] = 0;

    std::stringstream toprint;
    toprint.str("");
    toprint << "Front"  << "\t" ;
    toprint << "Unique" << "\t" ;
    toprint << "Edges"  << "\t" ;

    toprint << "Max" << "\t";
    toprint << "Min" << "\t";
    toprint << "AVG" << "\t";

    std::cout << toprint.str() << std::endl;

    int level = 0;

    while(true)
    {
        int counter = 0;
        int counter_edges = 0;
        int counter_repeated = 0;

        level++;

        for (int tid = 0; tid < no_of_nodes; tid++ )
        {
            repeated_mask[tid] = 0;
            if ( h_graph_mask[tid] == true && h_cost[tid] != NODE_UNVISITED )
            {
                h_frontier[counter] = tid;
                ++counter;
                h_graph_mask[tid] = false; 
            } 
        }

        // std::cout << "Frontier calculated ..." << std::endl;

        if (counter == 0)
        {
            break;
        } 

        int max = 0;
        int min = 100000;
        float avg = 0;

        for (int i = 0; i < counter; ++i)
        {
            int node_idx = h_frontier[i];
            int counter_sons = 0;
            for(int i=h_graph_nodes[node_idx]; i<(h_graph_nodes[node_idx+1] - h_graph_nodes[node_idx] + 
                                                        h_graph_nodes[node_idx]); i++)
            {
                int id = h_graph_edges[i];

                if (repeated_mask[id] == 1)
                {
                    counter_repeated++ ;
                }
                repeated_mask [id] = 1; 
                
                counter_edges++;
                counter_sons++;
            }

            if (counter_sons > max)
            {
                max = counter_sons;
            }
            if (counter_sons < min)
            {
                min = counter_sons;
            }
            avg += counter_sons;

        }

        run_single_bfs_cpu(graph, h_graph_mask, h_cost, h_frontier, counter);

        int unique = counter_edges - counter_repeated;
        toprint.str("");
        toprint << std::fixed << std::setw(2) << std::setprecision(3) ;
        toprint << counter << "\t" ;
        toprint << unique  << "\t"  ;
        toprint << counter_edges  << "\t";
        toprint << max << "\t" ;
        toprint << min << "\t";
        toprint << avg / counter << "\t";

        std::cout << toprint.str() << std::endl;
    }

    //--result varification
    serial_correctness(graph, h_cost);

    delete h_graph_mask ;    
    delete h_cost;
    delete h_frontier;

    return;
}

void Histogram::multiSource()
{
    uint32_t no_of_nodes = graph->no_of_nodes;  
    uint32_t* h_graph_nodes = graph->nodes; 
    uint32_t no_of_edges = graph->no_of_edges;
    uint32_t *h_graph_edges = graph->edges;
    uint32_t source = graph->source;

    uint32_t samples = 500;

    char* h_graph_mask  = new char[no_of_nodes];
    int*  h_cost        = new int [no_of_nodes];
    int*  h_frontier    = new int [no_of_nodes];

    cl_mem d_graph_nodes; 
    cl_mem d_graph_edges; 
    cl_mem d_graph_mask1; 
    cl_mem d_graph_mask2; 
    cl_mem d_cost;

    char kernel_file[100]  = "../bfs/Kernels.cl";
    int total_kernels = 1;
    string kernel_names[1] = {"BFS_mask_with_over_flag"};
    _clInit(kernel_file, total_kernels, kernel_names);

    d_graph_nodes = _clMalloc( (no_of_nodes+1)*sizeof(uint32_t), h_graph_nodes);
    d_graph_edges = _clMalloc(no_of_edges*sizeof(uint32_t), h_graph_edges);
    d_graph_mask1 = _clMallocRW(no_of_nodes*sizeof(char), h_graph_mask);
    d_graph_mask2 = _clMallocRW(no_of_nodes*sizeof(char), h_graph_mask);
    d_cost        = _clMallocRW(no_of_nodes*sizeof(int), h_cost);
    
    char h_over;
    cl_mem d_over = _clMallocRW(sizeof(char), &h_over);

    // GPU warm up
    int aux_idx = 0;
    _clSetArgs(0, aux_idx++, d_graph_nodes);
    _clSetArgs(0, aux_idx++, d_graph_edges);
    _clSetArgs(0, aux_idx++, d_graph_mask1);
    _clSetArgs(0, aux_idx++, d_graph_mask2);
    _clSetArgs(0, aux_idx++, d_cost);
    _clSetArgs(0, aux_idx++, &no_of_nodes, sizeof(int));
    _clSetArgs(0, aux_idx++, &no_of_nodes, sizeof(int));
    _clSetArgs(0, aux_idx++, d_over);

    _clInvokeKernel(0, no_of_nodes, MAX_THREADS_PER_BLOCK);

    h_graph_mask[source]=true;
    h_cost[source] = 0;
    _clMemcpyH2D(d_cost, no_of_nodes*sizeof(int), h_cost);  
    _clMemcpyH2D(d_graph_mask1, no_of_nodes*sizeof(char), h_graph_mask);  
    h_graph_mask[source]=false; // must be all false initially for mask2s
    _clMemcpyH2D(d_graph_mask2, no_of_nodes*sizeof(char), h_graph_mask);  
    _clFinish();


    int level = 0;

    #ifdef  PROFILING
    ChronoCpu chrono_cpu("cpu_timer");
    ChronoCpu chrono_gpu("total_timer");

    #endif

    for (int i = 0; i < samples; ++i)
    {
        source = rand()%no_of_nodes;

        for (uint32_t i = 0; i < no_of_nodes; ++i)
        {
            h_graph_mask[i] = false;
            h_cost[i]       = NODE_UNVISITED;
        }

        h_graph_mask[source]=true;
        h_cost[source] = 0;


    #ifdef  PROFILING
        chrono_cpu.tic();
    #endif

            
        uint32_t frontier_size = createFrontier(h_graph_mask, h_cost, h_frontier, no_of_nodes);
        while (frontier_size > 0)
        {
            run_single_bfs_cpu(graph, h_graph_mask, h_cost, h_frontier, frontier_size);
            frontier_size = createFrontier(h_graph_mask, h_cost, h_frontier, no_of_nodes);
        }

    #ifdef  PROFILING
        chrono_cpu.tac();
    #endif

        //serial_correctness(graph, h_cost);

    }

    for (int i = 0; i < samples; ++i)
    {
        source = rand()%no_of_nodes;

        for (uint32_t i = 0; i < no_of_nodes; ++i)
        {
            h_graph_mask[i] = false;
            h_cost[i]       = NODE_UNVISITED;
        }

        h_cost[source] = 0;
        _clMemcpyH2D(d_cost, no_of_nodes*sizeof(int), h_cost);  
        _clMemcpyH2D(d_graph_mask1, no_of_nodes*sizeof(char), h_graph_mask);  
        h_graph_mask[source]=false; // must be all false initially for mask2s
        _clMemcpyH2D(d_graph_mask2, no_of_nodes*sizeof(char), h_graph_mask);  
        _clFinish();

    #ifdef  PROFILING
        chrono_gpu.tic();
    #endif  

        level = 1;

        h_over = false;
        bool flagMask = true;
        while (!h_over)
        {
            h_over = true;
            _clMemcpyH2D(d_over, sizeof(char), &h_over);

            int kernel_id  = 0;
            int kernel_idx = 0;
            _clSetArgs(kernel_id, kernel_idx++, d_graph_nodes);
            _clSetArgs(kernel_id, kernel_idx++, d_graph_edges);
            if (flagMask)
            {
                _clSetArgs(kernel_id, kernel_idx++, d_graph_mask1);
                _clSetArgs(kernel_id, kernel_idx++, d_graph_mask2);
            }
            else
            {
                _clSetArgs(kernel_id, kernel_idx++, d_graph_mask2);
                _clSetArgs(kernel_id, kernel_idx++, d_graph_mask1);
            }
            flagMask = !flagMask;
            
            _clSetArgs(kernel_id, kernel_idx++, d_cost);
            _clSetArgs(kernel_id, kernel_idx++, &level, sizeof(int));
            _clSetArgs(kernel_id, kernel_idx++, &no_of_nodes, sizeof(int));
            _clSetArgs(kernel_id, kernel_idx++, d_over);
            _clInvokeKernel(kernel_id, no_of_nodes, MAX_THREADS_PER_BLOCK);

            _clMemcpyD2H(d_over, sizeof(char), &h_over);
            //_clFinish();
            level++;
        }
        _clMemcpyD2H(d_cost, no_of_nodes*sizeof(int), h_cost);  
        _clFinish();

    #ifdef  PROFILING
        chrono_gpu.tac();
    #endif
    }


    std::cout << "Graph: " << graph->name << std::endl;
    std::cout << "Samples: " << samples<< std::endl;
    std::cout << chrono_cpu.getElapsedStats().averageTime_ms << "\t";
    std::cout << chrono_cpu.getElapsedStats().stdDevTime_ms << "\t";
    std::cout << chrono_gpu.getElapsedStats().averageTime_ms << "\t";
    std::cout << chrono_gpu.getElapsedStats().stdDevTime_ms << std::endl;
    // std::cout << "CPU TD AVG: " << chrono_cpu.getElapsedStats().averageTime_ms << std::endl;
    // std::cout << "CPU TD STD: " << chrono_cpu.getElapsedStats().stdDevTime_ms << std::endl;
    // std::cout << "GPU TD AVG: " << chrono_gpu.getElapsedStats().averageTime_ms << std::endl;
    // std::cout << "GPU TD STD: " << chrono_gpu.getElapsedStats().stdDevTime_ms << std::endl;


    _clFree(d_graph_nodes);
    _clFree(d_graph_edges);
    _clFree(d_graph_mask1);
    _clFree(d_graph_mask2);
    _clFree(d_cost);
    _clFree(d_over);
    _clRelease();

    delete[] h_cost;
    delete[] h_graph_mask;
    delete[] h_frontier;


}

//----------------------------------------------------------
//--breadth first search Histogram implementation 
//----------------------------------------------------------
void Histogram::generateExeTimeHisto() 
{
    uint32_t no_of_nodes = graph->no_of_nodes;  
    uint32_t* h_graph_nodes = graph->nodes; 
    uint32_t no_of_edges = graph->no_of_edges;
    uint32_t *h_graph_edges = graph->edges;
    uint32_t source = graph->source;

    char* h_graph_mask       = new char[no_of_nodes];
    char* h_graph_mask_ucb   = new char[no_of_nodes];
    char* h_graph_mask_ucb2  = new char[no_of_nodes];
    char* h_graph_mask_aux   = new char[no_of_nodes];
    char* h_graph_mask_gpu   = new char[no_of_nodes];
    int*  h_cost             = new int [no_of_nodes];
    int*  h_cost_ucb         = new int [no_of_nodes];
    int*  h_cost_aux         = new int [no_of_nodes];
    int*  h_frontier         = new int [no_of_nodes];

    for (uint32_t i = 0; i < no_of_nodes; ++i)
    {
        h_graph_mask[i] = false;
        h_cost[i] = NODE_UNVISITED;
    }

    h_graph_mask[source]=true;
    h_cost[source] = 0;

    char kernel_file[100]  = "../bfs/Kernels.cl";
    int total_kernels = 4;
    string kernel_names[4] = {"BFS_frontier", "BFS_mask", "BFS_mask_buttom", "BFS_mask_buttom_clean"};
    _clInit(kernel_file, total_kernels, kernel_names);

    // GPU INICIALIZATION
    cl_mem d_graph_nodes        = _clMalloc( (no_of_nodes+1)*sizeof(uint32_t), h_graph_nodes);         
    cl_mem d_graph_edges        = _clMalloc(no_of_edges*sizeof(int), h_graph_edges);         
    cl_mem d_graph_mask         = _clMallocRW(no_of_nodes*sizeof(char), h_graph_mask);         
    cl_mem d_graph_mask2        = _clMallocRW(no_of_nodes*sizeof(char), h_graph_mask);         
    cl_mem d_graph_mask_ucb     = _clMallocRW(no_of_nodes*sizeof(char), h_graph_mask);         
    cl_mem d_graph_mask_ucb_next= _clMallocRW(no_of_nodes*sizeof(char), h_graph_mask);         
    cl_mem d_graph_mask_next    = _clMallocRW(no_of_nodes*sizeof(char), h_graph_mask); // Does not matter this mask             
    cl_mem d_frontier           = _clMallocRW(no_of_nodes*sizeof(int), h_frontier); // worst case can be relaxed
    cl_mem d_cost               = _clMallocRW(no_of_nodes*sizeof(int), h_cost);
    cl_mem d_cost2              = _clMallocRW(no_of_nodes*sizeof(int), h_cost);    
    cl_mem d_cost_ucb           = _clMallocRW(no_of_nodes*sizeof(int), h_cost);    

    // END GPU INIT

    std::stringstream toprint;
    toprint.str("");
    toprint << "Front"  << "\t" ;

    toprint << "CPU AVG" << "\t";
    toprint << "CPU STD" << "\t";
    toprint << "CPU_UCB AVG" << "\t";
    toprint << "CPU_UCB STD" << "\t";
    toprint << "GPU AVG" << "\t";
    toprint << "GPU STD" << "\t";
    toprint << "GPU_M AVG" << "\t";
    toprint << "GPU_M STD" << "\t";
    toprint << "GPU_UCB AVG" << "\t";
    toprint << "GPU_UCB STD" << "\t";

    std::cout << toprint.str() << std::endl;

    int level = 0;

    //std::cout << "About to start simulation..." << std::endl;

    while(true)
    {
        level++;

        memcpy(h_graph_mask_aux, h_graph_mask, sizeof(char)*no_of_nodes );
        memcpy(h_cost_aux, h_cost, sizeof(int)*no_of_nodes );

        int counter = createFrontier(h_graph_mask, h_cost, h_frontier, no_of_nodes);
        if (counter == 0)
        {
            break;
        }

        ChronoCpu chrono_gpu("gpu_timer");
        ChronoCpu chrono_gpu_mask("gpu_timer");
        ChronoCpu chrono_gpu_ucb("gpu_timer");
        ChronoCpu chrono_cpu("cpu_timer");
        ChronoCpu chrono_cpu_ucb("cpu_timer");

        //std::cout << "Starting loop ..." << std::endl;

        for (int i = 0; i < NUMBER_OF_ITERATION; ++i)
        {
            memcpy(h_graph_mask, h_graph_mask_aux, sizeof(char)*no_of_nodes );
            memcpy(h_graph_mask_ucb, h_graph_mask_aux, sizeof(char)*no_of_nodes );
            memcpy(h_cost, h_cost_aux, sizeof(int)*no_of_nodes );
            memcpy(h_cost_ucb, h_cost_aux, sizeof(int)*no_of_nodes );
            _clMemcpyH2D(d_graph_mask2,    no_of_nodes*sizeof(char), h_graph_mask_aux);
            _clMemcpyH2D(d_graph_mask_ucb, no_of_nodes*sizeof(char), h_graph_mask_aux);
            _clMemcpyH2D(d_cost,     no_of_nodes*sizeof(int), h_cost_aux);  
            _clMemcpyH2D(d_cost2,    no_of_nodes*sizeof(int), h_cost_aux);  
            _clMemcpyH2D(d_cost_ucb, no_of_nodes*sizeof(int), h_cost_aux);  
            _clFinish();

            chrono_cpu_ucb.tic();
            run_single_bfs_cpu_buttom(graph, h_graph_mask_ucb, h_graph_mask_ucb2, h_cost_ucb, level);
            chrono_cpu_ucb.tac();

            chrono_cpu.tic();
            counter = createFrontier(h_graph_mask, h_cost, h_frontier, no_of_nodes);
            run_single_bfs_cpu(graph, h_graph_mask, h_cost, h_frontier, counter);
            chrono_cpu.tac();

            chrono_gpu.tic();

            _clMemcpyH2D(d_frontier, counter*sizeof(int), h_frontier);
            //--kernel 0
            int kernel_id = 0;
            int kernel_idx = 0;
            _clSetArgs(kernel_id, kernel_idx++, d_graph_nodes);
            _clSetArgs(kernel_id, kernel_idx++, d_graph_edges);
            _clSetArgs(kernel_id, kernel_idx++, d_graph_mask);
            _clSetArgs(kernel_id, kernel_idx++, d_cost);
            _clSetArgs(kernel_id, kernel_idx++, d_frontier);
            _clSetArgs(kernel_id, kernel_idx++, &counter, sizeof(int));
            
            _clInvokeKernel(kernel_id, counter, MAX_THREADS_PER_BLOCK);             
            
            _clFinish();
            chrono_gpu.tac();

            chrono_gpu_mask.tic();
            //--kernel 0
            kernel_id = 1;
            kernel_idx = 0;
            _clSetArgs(kernel_id, kernel_idx++, d_graph_nodes);
            _clSetArgs(kernel_id, kernel_idx++, d_graph_edges);
            _clSetArgs(kernel_id, kernel_idx++, d_graph_mask2);
            _clSetArgs(kernel_id, kernel_idx++, d_graph_mask_next);
            _clSetArgs(kernel_id, kernel_idx++, d_cost2);
            _clSetArgs(kernel_id, kernel_idx++, &level, sizeof(int) );
            _clSetArgs(kernel_id, kernel_idx++, &no_of_nodes, sizeof(int));
            
            _clInvokeKernel(kernel_id, no_of_nodes, MAX_THREADS_PER_BLOCK);
            
            _clFinish();
            chrono_gpu_mask.tac();
 
            chrono_gpu_ucb.tic();
 
            //--kernel 0
            kernel_id = 2;
            kernel_idx = 0;
            _clSetArgs(kernel_id, kernel_idx++, d_graph_nodes);
            _clSetArgs(kernel_id, kernel_idx++, d_graph_edges);
            _clSetArgs(kernel_id, kernel_idx++, d_graph_mask_ucb);
            _clSetArgs(kernel_id, kernel_idx++, d_graph_mask_ucb_next);
            _clSetArgs(kernel_id, kernel_idx++, d_cost_ucb);
            _clSetArgs(kernel_id, kernel_idx++, &level, sizeof(int) );
            _clSetArgs(kernel_id, kernel_idx++, &no_of_nodes, sizeof(int));
            
            _clInvokeKernel(kernel_id, no_of_nodes, MAX_THREADS_PER_BLOCK);

            kernel_id = 3;
            kernel_idx = 0;
            _clSetArgs(kernel_id, kernel_idx++, d_graph_mask_ucb);
            _clSetArgs(kernel_id, kernel_idx++, &no_of_nodes, sizeof(int));
            
            _clInvokeKernel(kernel_id, no_of_nodes, MAX_THREADS_PER_BLOCK);

            _clFinish();
            chrono_gpu_ucb.tac();
        }

        _clMemcpyD2H(d_cost, no_of_nodes*sizeof(int), h_cost_aux);
        _clFinish();
        if(!compare(h_cost, h_cost_aux, no_of_nodes))
        {
            std::cout << "GPU failed" << std::endl;
        }

        _clMemcpyD2H(d_cost2, no_of_nodes*sizeof(int), h_cost_aux);
        _clFinish();
        if(!compare(h_cost, h_cost_aux, no_of_nodes))
        {
            std::cout << "Mask failed" << std::endl;
        }

        _clMemcpyD2H(d_cost_ucb, no_of_nodes*sizeof(int), h_cost_aux);
        _clFinish();
        if(!compare(h_cost, h_cost_aux, no_of_nodes))
        {
            std::cout << "Berkeley failed" << std::endl;
        }

        memcpy(h_cost_aux, h_cost_ucb , no_of_nodes*sizeof(int));
        if(!compare(h_cost, h_cost_aux, no_of_nodes))
        {
            std::cout << "BerkeleyCPU failed" << std::endl;
        }

        // cout << "---- End iteration: " << level << endl;

        toprint.str("");
        toprint << std::fixed << std::setw(2) << std::setprecision(3) ;
        toprint << counter << "\t" ;

        toprint << chrono_cpu.getElapsedStats().averageTime_ms << "\t";
        toprint << chrono_cpu.getElapsedStats().stdDevTime_ms << "\t";
        toprint << chrono_cpu_ucb.getElapsedStats().averageTime_ms << "\t";
        toprint << chrono_cpu_ucb.getElapsedStats().stdDevTime_ms << "\t";
        toprint << chrono_gpu.getElapsedStats().averageTime_ms << "\t";
        toprint << chrono_gpu.getElapsedStats().stdDevTime_ms << "\t";
        toprint << chrono_gpu_mask.getElapsedStats().averageTime_ms << "\t";
        toprint << chrono_gpu_mask.getElapsedStats().stdDevTime_ms << "\t";
        toprint << chrono_gpu_ucb.getElapsedStats().averageTime_ms << "\t";
        toprint << chrono_gpu_ucb.getElapsedStats().stdDevTime_ms << "\t";

        std::cout << toprint.str() << std::endl;
    }

    //--result varification
    serial_correctness(graph, h_cost);

    _clMemcpyD2H(d_cost, no_of_nodes*sizeof(int), h_cost); // this
    _clFinish();
    //std::cout << "Checking GPU" << std::endl;
    serial_correctness(graph, h_cost);

    _clMemcpyD2H(d_cost2, no_of_nodes*sizeof(int), h_cost); // this
    _clFinish();
    //std::cout << "Checking GPU Mask" << std::endl;
    serial_correctness(graph, h_cost);

    _clMemcpyD2H(d_cost_ucb, no_of_nodes*sizeof(int), h_cost); // this
    _clFinish();
    //std::cout << "Checking GPU Berkeley" << std::endl;
    serial_correctness(graph, h_cost);

    //--4 release cl resources.
    _clFree(d_graph_nodes        );
    _clFree(d_graph_edges        );
    _clFree(d_graph_mask         );
    _clFree(d_graph_mask2        );
    _clFree(d_graph_mask_ucb     );
    _clFree(d_graph_mask_ucb_next);
    _clFree(d_graph_mask_next    );
    _clFree(d_frontier           );
    _clFree(d_cost               );
    _clFree(d_cost2              );
    _clFree(d_cost_ucb           );
    _clRelease();

    delete[] h_graph_mask     ;
    delete[] h_graph_mask_ucb ;
    delete[] h_graph_mask_ucb2;
    delete[] h_graph_mask_aux ;
    delete[] h_graph_mask_gpu ;
    delete[] h_cost           ;
    delete[] h_cost_ucb       ;
    delete[] h_cost_aux       ;
    delete[] h_frontier       ;

    return ;
}

int checkRepeated(char* first, char* second, int* counterEdges, Graph* graph)
{
    int counterNodes = 0;
    *counterEdges = 0;

    for (int i = 0; i < graph->no_of_nodes; ++i)
    {
        if ( first[i] && second[i] )
        {
            *counterEdges += graph->nodes[i+1] - graph->nodes[i];
            ++counterNodes;
        }
    }

    return counterNodes;
}

int mergeSolutions(int* first, int* second, int no_of_nodes)
{
    for (int i = 0; i < no_of_nodes; ++i)
    {
        first[i] = std::min(first[i], second[i]);
    }

    return 0;
}

void Histogram::repetitionHisto() 
{
    uint32_t no_of_nodes = graph->no_of_nodes;  
    uint32_t no_of_edges = graph->no_of_edges;
    uint32_t* h_graph_nodes = graph->nodes; 
    uint32_t* h_graph_edges = graph->edges;
    uint32_t source = graph->source;
    //srand(time(NULL));
    //source = rand()%(no_of_nodes-1 + 1) + 1;

    char* h_graph_mask  = new char[no_of_nodes];
    int*  h_cost        = new int [no_of_nodes];
    int*  h_frontier    = new int [no_of_nodes];

    char* h_graph_mask1 = new char[no_of_nodes];
    int*  h_cost1       = new int [no_of_nodes];
    int*  h_frontier1   = new int [no_of_nodes];
    char* processed1    = new char[no_of_nodes];

    char* h_graph_mask2 = new char[no_of_nodes];
    int*  h_cost2       = new int [no_of_nodes];
    int*  h_frontier2   = new int [no_of_nodes];
    char* processed2    = new char[no_of_nodes];

    for (uint32_t i = 0; i < no_of_nodes; ++i)
    {
        h_graph_mask[i]          = false;
        h_cost[i] = NODE_UNVISITED;
        processed1[i] = 0;
        processed2[i] = 0;
    }

    h_graph_mask[source]=true;
    h_cost[source] = 0;

    int level = 0;
    double totalProcNodes = 0;
    double totalProcEdges = 0;

    //std::cout << setw(14);
    std::cout << "frontierSize" << "\t" << "\% Repeated Nodes" << "\t" ;
    std::cout << "Compleated N" << "\t" << "\% Compleated Nodes" << "\t";
    std::cout << "frontierEdges"<< "\t" << "\% Repeated Edges" << "\t" << "\% Compleated Edges" << std::endl;
        

    while(true)
    {
        int frontierSize = createFrontier(h_graph_mask, h_cost, h_frontier, no_of_nodes);

        if (frontierSize == 0)
        {
            break;
        }
        //printf("front; %d \n", frontierSize);

        memcpy(h_graph_mask1, h_graph_mask, sizeof(char)*no_of_nodes); 
        memcpy(h_graph_mask2, h_graph_mask, sizeof(char)*no_of_nodes);

        memcpy(h_cost1, h_cost, sizeof(int)*no_of_nodes);
        memcpy(h_cost2, h_cost, sizeof(int)*no_of_nodes);

        float work_division = 0.5f;

        int* half_frontier = &h_frontier[(int) floor(frontierSize * (1.0f - work_division)) ];
        int frontierSize1 = ceil( frontierSize * work_division );    
        int frontierSize2 = frontierSize - frontierSize1;

        memcpy(h_frontier1, half_frontier, frontierSize1*sizeof(int));
        memcpy(h_frontier2, h_frontier, frontierSize2*sizeof(int));

        bool first  = false;
        bool second = false;

        while(true)
        {
            for (int i = 0; i < frontierSize1; ++i)
            {
                processed1[ h_frontier1[i] ] = 1;
            }

            if (first == false)
            {
                run_single_bfs_cpu(graph, h_graph_mask1, h_cost1, h_frontier1, frontierSize1);
                first = true;
                continue;
            }

            frontierSize1 = createFrontier(h_graph_mask1, h_cost1, h_frontier1, no_of_nodes);
            if (frontierSize1 == 0)
            {
                break;
            }
            run_single_bfs_cpu(graph, h_graph_mask1, h_cost1, h_frontier1, frontierSize1);
        }

        while(true)
        {
            for (int i = 0; i < frontierSize2; ++i)
            {
                processed2[ h_frontier2[i] ] = 1;
            }

            if (second == false)
            {
                run_single_bfs_cpu(graph, h_graph_mask2, h_cost2, h_frontier2, frontierSize2);
                second = true;
                continue;
            }

            frontierSize2 = createFrontier(h_graph_mask2, h_cost2, h_frontier2, no_of_nodes);
            if (frontierSize2 == 0)
            {
                break;
            }
            run_single_bfs_cpu(graph, h_graph_mask2, h_cost2, h_frontier2, frontierSize2);
        }

        int repeatedEdges; 
        int repeatedNodes = checkRepeated(processed1, processed2, &repeatedEdges, graph);

        mergeSolutions(h_cost1, h_cost2, no_of_nodes);
        serial_correctness(graph, h_cost1);

        for (uint32_t i = 0; i < no_of_nodes; ++i)
        {
            processed1[i] = 0;
            processed2[i] = 0;
        }

        int auxEdges = 0;
        for (int i = 0; i < frontierSize; ++i)
        {
            auxEdges += graph->nodes[h_frontier[i]+1] - graph->nodes[h_frontier[i]];
        }
        
        std::cout << frontierSize << "\t" <<(float)repeatedNodes/(float)no_of_nodes;
        std::cout << "\t" << totalProcNodes << "\t" << totalProcNodes/(float)no_of_nodes << "\t";
        std::cout << auxEdges << "\t" <<(float)repeatedEdges/(float)no_of_edges << "\t" << totalProcEdges/(float)no_of_edges << std::endl;
        
        totalProcNodes += frontierSize;
        totalProcEdges += auxEdges;
        // std::cout << "Level " << level++ << " Repeated till end: " << repeatedNodes << std::endl;

        //std::cout << level++ << std::endl;
        run_single_bfs_cpu(graph, h_graph_mask, h_cost, h_frontier, frontierSize);
    }

    //--result varification
    serial_correctness(graph, h_cost);

    delete h_graph_mask  ;
    delete h_cost        ;
    delete h_frontier    ;
    delete h_graph_mask1 ;
    delete h_cost1       ;
    delete h_frontier1   ;
    delete processed1    ;
    delete h_graph_mask2 ;
    delete h_cost2       ;
    delete h_frontier2   ;
    delete processed2    ;


    return ;
}