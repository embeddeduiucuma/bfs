/* ============================================================
University of Illinois at Urbana Champaign
Breadth-First-Search Optimized
Author: Luis Remis
Date:   Apr 5 2015
============================================================ */
#ifndef _Histogram_H_
#define _Histogram_H_

#include <vector>

#include "BFS_base.h"

class Histogram : public BFS_base
{

public:
    Histogram(struct Graph* graph, unsigned int num_omp_threads);
    ~Histogram();
    void run(struct Graph* graph_);

    /* This will generate the graph profile, indicating number of nodes and edges 
    processed in each frontier. */
    void generateGraphProfile();

    /* This will generate histogram for the execution time of each iteration using
    different approached. CPU|CPU using BottomUp|TopDown */ 
    void generateExeTimeHisto();


    /* This will generate histogram for checking how much work would be repeted 
    if the graph is splited in each frontier. Make sense only for Social Network graphs */ 
    void repetitionHisto();

    /* This will generate histogram for checking how much work would be repeted 
    if the graph is splited in each frontier. Make sense only for Social Network graphs */ 
    void multiSource();


     void testZCPerformance();
    


private:
	struct Graph* graph;
	
	/* This will generate histogram for checking the performace of the ZC buffer for a 
     platform */
	void RWTest(std::vector<uint32_t> v);
    void CopyAndMapTest(std::vector<uint32_t> v);

};

#endif