
import csv
import os
import re
import subprocess
import platform

platformInUse = platform.platform()

print platformInUse

if platformInUse.find('Darwin') >= 0:
    print 'Platform: MacOS'
    graphsHistoDir = '/Users/luisremis/research/bfs_git/data'
	HistoExe  = '/Users/luisremis/research/bfs_git/histogram/histo'
    os.system('make mac')
elif platformInUse.find('centos') >= 0 :
    print 'Platform: CentOS'
    graphsHistoDir = '/home/luisremis/researchData/Graphs'
	HistoExe  = '/home/luisremis/bfs/histogram/histo'
    os.system('make')
elif platformInUse.find('Ubuntu') >= 0:
    print 'Platform Odroid'
    graphsHistoDir = '/home/lremis/researchData/Graphs'
	HistoExe  = '/home/lremis/bfs/histogram/histo'
    os.system('make odroid')
else:
    print 'SYSTEM NOT FOUND!!!'
    quit()






for dirname, dirnames, filenames in os.walk(graphsHistoDir):
    # print path to all subdirectories first.

    for filen in filenames:
        filename =  re.sub(r'(.*).graph', r'\1', filen)
        if filen == filename:
            continue

        print filename

        phisto = subprocess.Popen(HistoExe + ' 4 ' + os.path.join(graphsHistoDir, filename) + '.graph 1 >' + 'histo_exe_' + filename + '.txt',
            shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

        retval = phisto.wait()