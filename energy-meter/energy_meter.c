/*
 * Energy meter library v1.0
 * for ODROID XU+E
 * 2014 DAC UMA andres@uma.es
 * 
 * Read header file for changes log
 */

#include "energy_meter.h"

// globals
int SENSORS_ENABLED=0;

// read sensors *********************************
#define read_sensors(a7W, a15W, gpuW, memW) {\
		int fa7, fa15,fgpu,fmem;  \
		char buf[256];\
		fa7 = open("/sys/bus/i2c/drivers/INA231/3-0045/sensor_W",O_RDONLY);  \
		fa15= open("/sys/bus/i2c/drivers/INA231/3-0040/sensor_W",O_RDONLY); \
		fgpu= open("/sys/bus/i2c/drivers/INA231/3-0044/sensor_W",O_RDONLY);\
		fmem= open("/sys/bus/i2c/drivers/INA231/3-0041/sensor_W",O_RDONLY);\
		read(fa7, &buf, 256);\
		sscanf(buf ," %lf ", &(a7W));\
		read(fa15, &buf, 256);\
		sscanf(buf ," %lf ", &(a15W));\
		read(fgpu, &buf, 256);\
		sscanf(buf ," %lf ", &(gpuW));\
		read(fmem, &buf, 256);\
		sscanf(buf ," %lf ", &(memW));\
		close(fa7);		close(fa15);		close(fgpu);		close(fmem);}
		
		
		
int set_sampler_affinity(struct energy_sample *sample, int cpu)
{
	cpu_set_t mask;
	__CPU_ZERO_S(sizeof(cpu_set_t),&mask);
	__CPU_SET_S(cpu,sizeof(cpu_set_t), &mask);
	
	return pthread_setaffinity_np(sample->th_meter, sizeof(cpu_set_t), &mask);
	
	
}

//-------------------------------------------------------------------

struct energy_sample * energy_meter_init(int sample_rate, int debug) // sample rate in miliseconds
{
	struct energy_sample * sample;
	sample=(struct energy_sample *) malloc(sizeof(struct energy_sample ));
	if (SENSORS_ENABLED==0)
	{
		int file;
		char c;
		int i,r;
		char *sensors[]={"/sys/bus/i2c/drivers/INA231/3-0045/enable","/sys/bus/i2c/drivers/INA231/3-0044/enable","/sys/bus/i2c/drivers/INA231/3-0040/enable","/sys/bus/i2c/drivers/INA231/3-0041/enable"};
		for(i=3;i!=0;i--)
		{	
			file=open(sensors[i],O_RDONLY);
			read(file,&c,1);
			close(file);
			if(c!='1')
			{ 
				enable_sensors();
				sleep(2);   // wait for start up
				break;
			}
		}
		SENSORS_ENABLED=1;
	}
		
	sample->sample_rate=sample_rate*1000; // in microseconds to use in usleep()
	sample->A7=0.0;
	sample->A15=0.0;
	sample->GPU=0.0;
	sample->MEM=0.0;
	sample->destroy=0;
	sample->stop=1;
	sample->samples=0;
	
	pthread_mutex_init(&(sample->mutex),NULL);
	pthread_mutex_lock(&(sample->mutex));
	if (debug)
	pthread_create(&(sample->th_meter), NULL, meter_function_debug , (void *)sample);	//thread
	else
	pthread_create(&(sample->th_meter), NULL, meter_function , (void *)sample);	//thread 
	
	return(sample);
}

//-------------------------------------------------------------------
void energy_meter_start(struct energy_sample *sample)
{
	clock_gettime(CLOCK_REALTIME, &(sample->start_time));
	
	pthread_mutex_unlock(&(sample->mutex)); //start energy sampling
}

//-------------------------------------------------------------------
void energy_meter_stop(struct energy_sample *sample)
{
	struct timespec res;
	double secs;
	struct timespec dif;
   
	pthread_mutex_lock(&(sample->mutex));  // stop energy sampling
	clock_gettime(CLOCK_REALTIME, &(sample->stop_time));
	res=diff(sample->start_time, sample->stop_time);
	
	sample->time=(double)res.tv_sec+ (double)res.tv_nsec/1000000000.0;
	
	
	//read_sensors(sample1->a7W, sample1->a15W, sample1->gpuW, sample1->memW);
    //sample->now=!sample->now;
	// get time now**********************************
	//clock_gettime(CLOCK_REALTIME, &dif );
	// get time interval    !!! only nanoseconds, sampling rate must be below 1 second
	dif.tv_nsec=sample->stop_time.tv_nsec - sample->res[sample->now].tv_nsec;
	if(	dif.tv_nsec <0)	dif.tv_nsec += 1000000000;
	// claculate energy until now **************************************
	secs= dif.tv_nsec/1000000000.0; // move to seconds
	sample->A7  += sample->a7W * secs ; // Watt*sec=Joules
	sample->A15 += sample->a15W * secs;	
	sample->GPU += sample->gpuW * secs;
	sample->MEM += sample->memW * secs;
	
}
//-------------------------------------------------------------------

void energy_meter_destroy(struct energy_sample *sample) // always after stop
{
	sample->destroy=1;
	pthread_mutex_unlock(&(sample->mutex));  
	pthread_join(sample->th_meter,NULL);
	pthread_mutex_destroy(&(sample->mutex));
	free(sample);
}
//-------------------------------------------------------------------

void energy_meter_printf(struct energy_sample *sample1, FILE * fout)
{
	struct timespec res;
	res=diff(sample1->start_time, sample1->stop_time);
	
	fprintf(fout,"+--------------------+\n");
	fprintf(fout,"| POWER MEASUREMENTS |\n");
	fprintf(fout,"+--------------------+\n");
	
	fprintf(fout,"A7 = %lf J :: A15= %lf J :: GPU= %lf J :: Mem= %lf J\n",sample1->A7, sample1->A15,sample1->GPU,sample1->MEM);
	fprintf(fout,"CPU= %lf J :: GPU= %lf J :: UNCORE= %lf J\n",sample1->A15+sample1->A7, sample1->GPU, sample1->MEM);
	fprintf(fout,"TOTAL E= %lf J\n",total_em((*sample1)));
	fprintf(fout,"CLOCK_REALTIME = %lf sec\n",(double)res.tv_sec+ (double)res.tv_nsec/1000000000.0);	
	fprintf(fout,"# of samples: %ld\n", sample1->samples);
	fprintf(fout,"sample every (real) = %lf sec\n",((double)res.tv_sec+ (double)res.tv_nsec/1000000000.0)/sample1->samples);	
	fprintf(fout,"sample every: %lf sec\n",(double)sample1->sample_rate/1000000);	
	
}
//-------------------------------------------------------------------

void energy_meter_read(struct energy_sample *sample, struct em_t * out)
{
	double secs;
	struct timespec dif;
   
	// mutex 
	pthread_mutex_lock(&(sample->mutex));
	
		sample->now=!sample->now;
		// get time now**********************************
		clock_gettime(CLOCK_REALTIME, sample->res+sample->now );
		read_sensors(sample->a7W, sample->a15W, sample->gpuW, sample->memW);
		// get time interval    !!! only nanoseconds, sampling rate must be below 1 second
		dif.tv_nsec=sample->res[sample->now].tv_nsec-sample->res[!sample->now].tv_nsec;
		if(	dif.tv_nsec <0)	dif.tv_nsec += 1000000000;
		
		
		// claculate energy  **************************************
		secs= dif.tv_nsec/1000000000.0; // move to seconds
        
		sample->A7  += sample->a7W * secs ; // Watt*sec=Joules
		sample->A15 += sample->a15W * secs;
		sample->GPU += sample->gpuW * secs;
		sample->MEM += sample->memW * secs;
				
		sample->samples++;
	
	
	
	out->A7=  sample->A7;
	out->A15= sample->A15;
	out->GPU= sample->GPU;
	out->MEM= sample->MEM;
	
	
	pthread_mutex_unlock(&(sample->mutex));
	
	
	//
	
}
//-------------------------------------------------------------------
void energy_meter_diff(struct energy_sample *sample, struct em_t * diff)
{
	double secs;
	struct timespec dif;
   
	double A7,A15,GPU,MEM;
	
	
	// mutex 
	pthread_mutex_lock(&(sample->mutex));
	
		sample->now=!sample->now;
		// get time now**********************************
		clock_gettime(CLOCK_REALTIME, sample->res+sample->now );
		read_sensors(sample->a7W, sample->a15W, sample->gpuW, sample->memW);
		// get time interval    !!! only nanoseconds, sampling rate must be below 1 second
		dif.tv_nsec=sample->res[sample->now].tv_nsec-sample->res[!sample->now].tv_nsec;
		if(	dif.tv_nsec <0)	dif.tv_nsec += 1000000000;
		
		
		// claculate energy  **************************************
		secs= dif.tv_nsec/1000000000.0; // move to seconds
        
		sample->A7  += sample->a7W * secs ; // Watt*sec=Joules
		sample->A15 += sample->a15W * secs;
		sample->GPU += sample->gpuW * secs;
		sample->MEM += sample->memW * secs;
				
		sample->samples++;
	
	
	
	diff->A7=  sample->A7 - diff->A7;
	diff->A15= sample->A15 - diff->A15;
	diff->GPU= sample->GPU - diff->GPU;
	diff->MEM= sample->MEM - diff->MEM;
	
	
	pthread_mutex_unlock(&(sample->mutex));

}
//-------------------------------------------------------------------
void energy_meter_read_printf(struct em_t * diff, FILE *fout)
{
	fprintf(fout,"POWER READ ----------------\n");
	fprintf(fout," A7= %lf J :: A15= %lf J :: GPU= %lf J :: Mem= %lf J\n",diff->A7, diff->A15,diff->GPU,diff->MEM);
}
//-------------------------------------------------------------------


void *meter_function(void *arg)
{
	struct energy_sample *sample=(struct energy_sample *) arg;
	
	char buf[256];
	//int fa7, fa15,fgpu,fmem;
	struct timespec dif;
	double secs;
	
    sample->now=0;
    // first sample
 	pthread_mutex_lock(&(sample->mutex));
 	clock_gettime(CLOCK_REALTIME, sample->res);
 	read_sensors(sample->a7W, sample->a15W, sample->gpuW, sample->memW);
    pthread_mutex_unlock(&(sample->mutex));
	
	usleep(sample->sample_rate);

	while(1)  // sampling on course
	{
		pthread_mutex_lock(&(sample->mutex));
		if(sample->destroy)
		{
			pthread_mutex_unlock(&(sample->mutex));
			pthread_exit(NULL);
		}
		sample->now=!sample->now;
		// get time now**********************************
		clock_gettime(CLOCK_REALTIME, sample->res+sample->now );
		read_sensors(sample->a7W, sample->a15W, sample->gpuW, sample->memW);
		// get time interval    !!! only nanoseconds, sampling rate must be below 1 second
		dif.tv_nsec=sample->res[sample->now].tv_nsec-sample->res[!sample->now].tv_nsec;
		if(	dif.tv_nsec <0)	dif.tv_nsec += 1000000000;
		
		
		// claculate energy  **************************************
		secs= dif.tv_nsec/1000000000.0; // move to seconds
        
		sample->A7  += sample->a7W * secs ; // Watt*sec=Joules
		sample->A15 += sample->a15W * secs;
		sample->GPU += sample->gpuW * secs;
		sample->MEM += sample->memW * secs;
		
		
		
	
		sample->samples++;
		// DEBUG
		// fprintf(stdout,"a7= %lf W : a15= %lf W : gpu= %lf W \n",a7W,a15W,gpuW);
		// fprintf(stdout,"CLOCK_REALTIME = %lld sec, %ld nsec\n",(long long) dif.tv_sec, (long)dif.tv_nsec);	
	
		
		
		pthread_mutex_unlock(&(sample->mutex));
		
		usleep(sample->sample_rate);
	}
	
}


//-------------------------------------------------------------------

void *meter_function_debug(void *arg)
{
	struct energy_sample *sample=(struct energy_sample *) arg;
	struct timespec dif;
	char buf[256];
	FILE *debugf;
	int fa7, fa15,fgpu, fmem;
	//double a7W=0.0, a15W=0.0, gpuW=0.0, memW=0.0;
	double secs;
    int c1,c2,c3,c4,cGPU;
    char fn[256];
    sprintf(fn,"debug_energy_meter%d.csv",getpid());
    debugf=fopen(fn,"w");
    fprintf(debugf,"#;sample;time;A15;A7;gpu;Mem;Mhz1;Mhz2;Mhz3;Mhz4;MhzGPU\n");	
    // first sample
    sample->now=0;
 	pthread_mutex_lock(&(sample->mutex));
 	clock_gettime(CLOCK_REALTIME, sample->res);
 	read_sensors(sample->a7W, sample->a15W, sample->gpuW, sample->memW);
	
    pthread_mutex_unlock(&(sample->mutex));
	
	usleep(sample->sample_rate);

	while(1)  // sampling on course
	{
		pthread_mutex_lock(&(sample->mutex));
		if(sample->destroy)
		{
			pthread_mutex_unlock(&(sample->mutex));
			fclose(debugf);
			pthread_exit(NULL);
		}
		sample->now=!sample->now;
		// get time now**********************************
		clock_gettime(CLOCK_REALTIME, sample->res+sample->now );
		// get time interval    !!! only nanoseconds, sampling rate must be below 1 second
		dif.tv_nsec=sample->res[sample->now].tv_nsec-sample->res[!sample->now].tv_nsec;
		if(	dif.tv_nsec <0)	dif.tv_nsec += 1000000000;
		
		read_sensors(sample->a7W, sample->a15W, sample->gpuW, sample->memW);
		
		// claculate energy  **************************************
		secs= dif.tv_nsec/1000000000.0; // move to seconds
        
		sample->A7  += sample->a7W * secs ; // Watt*sec=Joules
		sample->A15 += sample->a15W * secs;
		sample->GPU += sample->gpuW * secs;
		sample->MEM += sample->memW * secs;

		// read sensors ********************************* 
		
		fa7 = open("/sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq",O_RDONLY);  // dot it easy with open
		read(fa7, &buf, 256);
		sscanf(buf ," %d ", &c1);
		close(fa7);
		
		fa7 = open("/sys/devices/system/cpu/cpu1/cpufreq/scaling_cur_freq",O_RDONLY);  // dot it easy with open
		read(fa7, &buf, 256);
		sscanf(buf ," %d ", &c2);
		close(fa7);
		fa7 = open("/sys/devices/system/cpu/cpu2/cpufreq/scaling_cur_freq",O_RDONLY);  // dot it easy with open
		read(fa7, &buf, 256);
		sscanf(buf ," %d ", &c3);
		close(fa7);
		fa7 = open("/sys/devices/system/cpu/cpu3/cpufreq/scaling_cur_freq",O_RDONLY);  // dot it easy with open
		read(fa7, &buf, 256);
		sscanf(buf ," %d ", &c4);
		close(fa7);
		fa7 = open("/sys/module/pvrsrvkm/parameters/sgx_gpu_clk",O_RDONLY);  // dot it easy with open
		read(fa7, &buf, 256);
		sscanf(buf ," %d ", &cGPU);
		close(fa7);
		
		sample->samples++;
		// DEBUG
		fprintf(debugf,"%ld;", sample->samples);
		
		fprintf(debugf,"%ld;", (long)dif.tv_nsec);	
		
		dif=diff(sample->start_time, sample->res[sample->now]); 
	
	//sample->time=(double)res.tv_sec+ (double)res.tv_nsec/1000000000.0;

		fprintf(debugf,"%lf ;", (double)dif.tv_sec+ (double)dif.tv_nsec/1000000000.0);
	
		fprintf(debugf,"%lf;%lf;%lf;",sample->a15W,sample->a7W,sample->gpuW);
		fprintf(debugf,"%lf;",sample->memW);
		//if(sample->samples<20) printf("%lf;",memW);
		fprintf(debugf,"%d;", c1/1000);
		fprintf(debugf,"%d;", c2/1000);
		fprintf(debugf,"%d;", c3/1000);
		fprintf(debugf,"%d;", c4/1000);
		fprintf(debugf,"%d\n", cGPU);
		
		//read_sensors(sample->a7W, sample->a15W, sample->gpuW, sample->memW);
		
		pthread_mutex_unlock(&(sample->mutex));
		
		usleep(sample->sample_rate);
	}
	
}


//-------------------------------------------------------------------

void enable_sensors()
{
FILE *fa7_, *fa15_, *fgpu_, *mem_;

fa7_ = fopen("/sys/bus/i2c/drivers/INA231/3-0045/enable","w");
fa15_= fopen("/sys/bus/i2c/drivers/INA231/3-0040/enable","w");
fgpu_= fopen("/sys/bus/i2c/drivers/INA231/3-0044/enable","w");
mem_=  fopen("/sys/bus/i2c/drivers/INA231/3-0041/enable","w");

fprintf(fa7_, "1");
fprintf(fa15_,"1");
fprintf(fgpu_,"1");
fprintf(mem_,"1");

fclose(fa7_ );
fclose(fa15_);
fclose(fgpu_);
fclose(mem_);
}

//-------------------------------------------------------------------

struct timespec diff(struct timespec start, struct timespec end)
{
	struct timespec temp;
	if ((end.tv_nsec-start.tv_nsec)<0) {
		temp.tv_sec = end.tv_sec-start.tv_sec-1;
		temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
	} else {
		temp.tv_sec = end.tv_sec-start.tv_sec;
		temp.tv_nsec = end.tv_nsec-start.tv_nsec;
	}
	return temp;
}

//-------------------------------------------------------------------
