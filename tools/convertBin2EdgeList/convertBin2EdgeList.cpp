/* ============================================================
University of Illinois at Urbana Champaign
Breadth-First-Search Optimized - Histograms
Author: Luis Remis
Date:	Feb 2016
============================================================ */
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstring>

#include "../../common/readGraph.h"

int main(int argc, char * argv[])
{

	std::string input_f (argv[1]);
	std::string output_f (argv[2]);
	
	struct Graph* graph;
	graph = readGraphFromFile(input_f.c_str());

	std::ofstream out(output_f);


	for (int i = 0; i < graph->no_of_nodes; ++i)
	{

		for (int j = graph->nodes[i]; j < graph->nodes[i+1]; ++j)
		{
			out << i << " " << graph->edges[j] << std::endl;
		}
		
	}

	out.close();

    return 0;
}