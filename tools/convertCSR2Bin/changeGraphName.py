#from cycler import cycler
#import numpy as np
#import matplotlib.pyplot as plt
#from matplotlib.backends.backend_pdf import PdfPages
import csv
import os
import re


for dirname, dirnames, filenames in os.walk('../../data'):
    # print path to all subdirectories first.
    for filen in filenames:
        graphName = re.sub(r'(.*).graph.*', r'\1', filen)

        if graphName == filen: # meaning that it is not a .graph file
            continue

        filename = (os.path.join(dirname, filen))
        print filename
        print graphName

        newFile = open (filename, "rb+")

        newFile.write(graphName+'\0')

        newFile.close()


        # with open('histos/histo_' + filename + '.txt') as f:
        #     data = []
        #     for line in f:
        #         line = line.split() # to deal with blank 

        #         if not isfloat(line[0]): # skip lines with text, probably headers
        #             continue

        #         if line:            # lines (ie skip them)
        #             line = [float(i) for i in line]
        #             data.append(line)

