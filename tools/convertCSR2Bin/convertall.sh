g++ convertCSR2Bin.cpp ../../common/readGraph.cpp -I ../../common/ -o convertCSR2Bin

./convertCSR2Bin ../../data/snap/google.txt ../../data/snap/google.graph
./convertCSR2Bin ../../data/snap/youtube.txt ../../data/snap/youtube.graph
./convertCSR2Bin ../../data/snap/amazon.txt ../../data/snap/amazon.graph
./convertCSR2Bin ../../data/snap/roadCA.txt ../../data/snap/roadCA.graph
./convertCSR2Bin ../../data/snap/email.txt ../../data/snap/email.graph

./convertCSR2Bin ../../data/rodinia/graph1k.txt ../../data/rodinia/graph1k.graph
./convertCSR2Bin ../../data/rodinia/graph1M.txt ../../data/rodinia/graph1M.graph
./convertCSR2Bin ../../data/rodinia/graph2M.txt ../../data/rodinia/graph2M.graph
./convertCSR2Bin ../../data/rodinia/graph4M.txt ../../data/rodinia/graph4M.graph
./convertCSR2Bin ../../data/rodinia/graph8M.txt ../../data/rodinia/graph8M.graph
./convertCSR2Bin ../../data/rodinia/graph16M.txt ../../data/rodinia/graph16M.graph

./convertCSR2Bin ../../data/graph500/kron.txt ../../data/graph500/kron.graph	
./convertCSR2Bin ../../data/graph500/rmat.txt ../../data/graph500/rmat.graph
