/* ============================================================
University of Illinois at Urbana Champaign
Breadth-First-Search Optimized - Histograms
Author: Luis Remis
Date:	Feb 2016
============================================================ */
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>

#include "readGraph.h"

int main(int argc, char * argv[])
{

	std::string input_f (argv[1]);
	std::string output_f (argv[2]);
	
	struct Graph* graph;
	graph = read_graph_from_file_Rodinia(input_f);

	saveGraph2File(graph, output_f.c_str() );

	std::cout << "graph saved: " << output_f << std::endl;

	struct Graph* graph2 = readGraphFromFile(output_f.c_str());

	checkGraph(graph2);

    return 0;
}