
import csv
import os
import re
import subprocess
import platform

platformInUse = platform.platform()

print platformInUse

if platformInUse.find('Darwin') >= 0:
    print 'Platform: MacOS'
    print 'Galois does not run in mac, you idiot'
    quit()

elif platformInUse.find('centos') >= 0 :
    print 'Platform: CentOS'
    graphsDir = '/home/luisremis/researchData/GaloisGraphs'
    graphsHistoDir = '/home/luisremis/researchData/Graphs'
    GaloisExe = '/home/luisremis/Galois-2.2.1/apps/bfs/bfs'
    HistoExe  = '/home/luisremis/bfs/histogram/histo'

elif platformInUse.find('Ubuntu') >= 0:
    print 'Platform Odroid'
    graphsDir = '/home/lremis/researchData/GaloisGraphs'
    graphsHistoDir = '/home/lremis/researchData/Graphs'
    GaloisExe = '/home/lremis/Galois-2.2.1/apps/bfs/bfs'
    HistoExe  = '/home/lremis/bfs/histogram/histo'

else:
    print 'SYSTEM NOT FOUND!!!'
    quit()




GaloisAlgos = ['async', 'barrier', 'barrierWithCas', 'highCentrality', 'hybrid', 'serial']
    # =async                 -   Asynchronous
    # =barrier               -   Parallel optimized with barrier (default)
    # =barrierWithCas        -   Use compare-and-swap to update nodes
    # =detBase               -   Deterministic
    # =detDisjoint           -   Deterministic with disjoint optimization
    # =highCentrality        -   Optimization for graphs with many shortest paths
    # =hybrid                -   Hybrid of barrier and high centrality algorithms
    # =serial

for dirname, dirnames, filenames in os.walk(graphsDir):
    # print path to all subdirectories first.

    for filen in filenames:
        filename =  re.sub(r'(.*).gr', r'\1', filen)
        #if filen == filename:
            #continue
        # print(os.path.join(graphsDir, filen))
        # print filename
        exe_time = 9999999.0;
        flagtime  = False

        for algo in GaloisAlgos:

            p = subprocess.Popen(GaloisExe + ' ' + os.path.join(graphsDir, filen) + ' -t 4 -algo=' + algo ,
                shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

            flagtime  = False

            output = ''
            for line in p.stdout.readlines():
                # print line
                output = output + line;
                time  =  re.sub(r'STAT,\(NULL\),Time,4,(.*),.*,.*,.*,.*', r'\1', line)
                
                if time != line:
                    # print line
                    # print algo + ' ' +str(float(time))
                    flagtime = True
                    newtime = float(time)*1e-3
                    if newtime < exe_time:
                        exe_time = newtime
                        best = algo

            retval = p.wait()
        
        # print 'Best: ' + best

        phisto = subprocess.Popen(HistoExe + ' 4 ' + os.path.join(graphsHistoDir, filename) + '.graph 0' ,
            shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

        flagedges = False

        for line in phisto.stdout.readlines():
            edges =  re.sub(r'Graph Edges: (.*)', r'\1', line)
            output = output + line

            if edges != line:
                # print float(nodes)/1e6
                flagedges = True
                no_edges = float(edges)/1e6

        retval = phisto.wait()


        if flagedges & flagtime:
            print filename  + '\t' + str(no_edges / exe_time)
            #print output
        else:
            print 'Error processing ' + filename + ' flagedges: ' + str(flagedges) + ' flagtime: ' + str(flagtime)

            #print output


        
