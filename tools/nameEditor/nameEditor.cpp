/* ============================================================
University of Illinois at Urbana Champaign
Breadth-First-Search Optimized - Histograms
Author: Luis Remis
Date:	Feb 2016
============================================================ */
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <cstring>

#include "../../common/readGraph.h"

int main(int argc, char * argv[])
{

	std::string input_f (argv[1]);
	std::string name (argv[2]);
	
	struct Graph* graph;
	graph = readGraphFromFile(input_f.c_str());

	printf("Size: %d\n", strlen(argv[2]));
	size_t len = strlen(argv[2]);

	char nullchar = '\0';

	memcpy(graph->name, argv[2],len);
	memcpy(graph->name + len, &nullchar, 1 );

	saveGraph2File(graph, input_f.c_str());

    return 0;
}