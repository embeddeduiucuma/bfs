import csv
import os
import re
import subprocess


graphsDir = '/home/luisremis/researchData/Graphs'
edgeListDir = '/home/luisremis/researchData/edgeLists/'
convert2Gr = '/home/luisremis/Galois-2.2.1/tools/graph-convert/graph-convert'

# for dirname, dirnames, filenames in os.walk(graphsDir):
#     # print path to all subdirectories first.

#     for filen in filenames:
#         filename =  re.sub(r'(.*).graph', r'\1', filen)
#         #if filen == filename:
#             #continue
#         # print(os.path.join(graphsDir, filen))
#         print filename

#         p = subprocess.Popen('./convertBin2EdgeList ' + os.path.join(graphsDir, filen) + ' ' + edgeListDir + filename + '.txt', 
#             shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
#         for line in p.stdout.readlines():
#             print line,
#         retval = p.wait()

for dirname, dirnames, filenames in os.walk(edgeListDir):
    # print path to all subdirectories first.

    for filen in filenames:
        filename =  re.sub(r'(.*).txt', r'\1', filen)
        #if filen == filename:
            #continue
        # print(os.path.join(graphsDir, filen))
        print filename

        p = subprocess.Popen(convert2Gr + ' -edgelist2vgr ' + os.path.join(edgeListDir, filen) + ' ' + os.path.join(edgeListDir, filename + '.gr'), 
            shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        for line in p.stdout.readlines():
            print line,
        retval = p.wait()

