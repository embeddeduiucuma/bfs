#include <iostream>
#include <vector>
#include <fstream>
#include <map>
#include <cstdlib>

using namespace std;

int main(int argc, char* argv[] ){

	ifstream f(argv[1]);
	ofstream out(argv[2]);

	std::cout << "Input: " << argv[1] << endl;
	std::cout << "Output: " << argv[2] << endl;

	unsigned int total_edges = 0;
	unsigned int total_nodes = 0;

	int from, to, weight;
	while(!f.eof())
	{
		f >> from;
		if (from > total_nodes)
		{
			total_nodes = from;
		}
	}
	total_nodes += 1;
	cout << "Max node: " << total_nodes << endl;
	std::vector <unsigned int> nodes (total_nodes,-1);

	f.clear();
	f.seekg(0, f.beg);

	while(!f.eof())
	{
		f >> from;
		nodes[from] = 1;
	}

	int real_counter = 0;
	int counter_no = 0;
	for (int i = 0; i < total_nodes; ++i)
	{
		if(nodes[i] == 1) 
		{
			nodes[i] = real_counter;
			real_counter++;
		}
		else
		{
			// std::cout << "Alert! a -1!!! shoudln't be here!" << std::endl;
			// std::cout << i << std::endl;
			// exit(-1);
			counter_no ++;
		}
	}

	std::cout << "Counter no: " << counter_no << std::endl;

	std::cout << "Effective nodes: " << real_counter << std::endl;

	vector< std::vector<int> > edges (real_counter, vector<int>());

	f.clear();
	f.seekg(0, f.beg);


	int counter = 0;
	while( !f.eof() )
	{
		++counter;
		f >> from;
		int from2 = from;
		from = nodes[from];
		f >> to;
		int to2 = to;
		to = nodes[to];
		//f >> weight;

		if (from == -1  || to == -1 )
		{
			std::cout << "from == -1  || to == -1: THIS CANNOT HAPPEN" << std::endl;
			std::cout << "from: " << from << " to: " << to << std::endl;
			std::cout << "from: " << from2 << " to: " << to2 << std::endl;
			std::cout << "line " << counter << std::endl;
			//exit(-1);
		}

		int flag = 0;

		for (int i = 0; i < edges[from].size(); ++i)
		{
			if (edges[from][i] == to)
				flag = 1;
		}

		if (flag == 0)
			edges[from].push_back(to);

		flag = 0;
		for (int i = 0; i < edges[to].size(); ++i)
		{
			if (edges[to][i] == from)
				flag = 1;
		}

		if (flag == 0)
			edges[to].push_back(from);
	}

	total_edges = 0;
	out << real_counter << endl;
	int init = 0;
	for (int i = 0; i < edges.size(); ++i)
	{
		out << init << " " << edges.at(i).size() << endl;
		init += edges.at(i).size();
		if (edges.at(i).size() == 0)
		{
			std::cout << "Unconnected node???" << std::endl;
		}
		total_edges+= edges.at(i).size();
	}

	out << endl << "27" << endl << endl; // Source Node
	out << total_edges << endl;

	for (int i = 0; i < edges.size(); ++i)
	{
		for (int j = 0; j < edges.at(i).size(); ++j)
		{
			out << edges[i][j] << " " << 1 << endl;
		}
	}

	cout << "No of Nodes: " << real_counter << endl;
	cout << "No of Edges: " << total_edges << endl;

	return 0;
}
