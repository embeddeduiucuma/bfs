#include "header.h"

int main(int argc, char **argv){
	tEdge *buf;
	int scale = 24;
	int n = (1 << scale);
	int edge_factor = 10;
	int num_edges = n * edge_factor;

	buf = (tEdge*)malloc(sizeof(tEdge) * num_edges);
	assert(buf != NULL);
	RmatGenerateEdges(scale, num_edges, buf);
	PrintEdges(num_edges, buf);
}
