#include "header.h"

#define K_ONE	10000
#define K_LIMIT	(0xffffffffU % K_ONE)

/**
 * Used for generating weights on edges
 **/
int GetRand(int K){
    int val;

    if(K==1) return(0);
    double x = ((double) rand()) / ((double) RAND_MAX + 1);
    val = (int) floor(x * K);
    return(val);
}


/***
 * Generated 'num_edges' number of edges and stores in 'buf'
 **/
void RmatGenerateEdges(int scale, int num_edges, tEdge *buf){
	uint64_t ib, i, ii, jj, start, end, tmp;
	uint64_t val, val0, val1;
	rng_t my_rng;

	rng_seed(&my_rng, RMAT_SEED);
	val0 = rng_rand64(&my_rng);
	val1 = rng_rand64(&my_rng);

	for (i = 0; i < num_edges; i++){
		start = end = 0;
		for (ib = scale; ib > 0; ib--){
			val = rng_rand64( &my_rng );
	  
			if (val < K_LIMIT) {
				do {
					val = rng_rand64( &my_rng );
				} while (val < K_LIMIT);
			}

			val %= K_ONE;
			if (val < K_BC) {
				ii = 0;
				jj = 1;
			} else if (val < 2 * K_BC) {
				ii = 1;
				jj = 0;
			} else if (val < K_A + 2 * K_BC) {
				ii = 0;
				jj = 0;
			} else {
				ii = 1;
				jj = 1;
			}
			start += ii << ( ib - 1 );
			end += jj << ( ib - 1 );
		}
		buf[i].src = (int)start;
		buf[i].dest = (int)end;
	}

	/*** 
     * Scrambling and weight generation
     *****/
	rng_seed(&my_rng, WEIGHT_SEED);
	for (i = 0; i < num_edges; i++) {
		ii = (uint64_t)buf[i].src;
		jj = (uint64_t)buf[i].dest;
      
		buf[i].src = (int)scramble(scale, ii, val0, val1);
		buf[i].dest = (int)scramble(scale, jj, val0, val1);
		val = rng_rand64( &my_rng );
		buf[i].w = val % W_MAX;
	}
}

/***
 * Prints the generated edges to a file
 ****/
int PrintEdges(int num_edges, tEdge *buf){
	int u, e;
	tEdge *pe;

	FILE *fp = fopen("graph.txt", "w");
	for(e = 0; e < num_edges; e++){
		pe = buf + e;
		//fprintf(fp, "%d %d %d\n",pe->src, pe->dest, pe->w);
		fprintf(fp, "%d %d",pe->src, pe->dest);
		if (e != num_edges-1)
		{
			fprintf(fp,"\n");
		}
	}
	fclose(fp);
}
