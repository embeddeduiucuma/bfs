#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdint.h>
#include <unistd.h>
#include <assert.h>


#define RMAT_SEED 178999 	//Graph generation seed
#define WEIGHT_SEED 14			//Weight generation seed
#define W_MAX 256 // Maximum weight 

/*** Rmat parameters A and B=C 
 * Expressed as fraction over 10000
 * Probability is A = 0.57 and B = C = 0.19
 ****/
#define K_A 5700
#define K_BC 1900

#define TRUE 1
#define FALSE 0

typedef struct{
	int src;		//source
	int dest;		//destination
	int w;		    //weight
}tEdge;

void RmatGenerateEdges();
int PrintEdges(int num_edges, tEdge *buf);

/** Random number generator related stuff **/
#define RNG_MM		156
#define RNG_MATRIX_A	0xB5026F5AA96619E9ULL
#define RNG_UM		0xFFFFFFFF80000000ULL
#define RNG_LM		0x000000007FFFFFFFULL
#define RNG_NN  312

typedef struct rng {
    uint64_t mt[RNG_NN];
    int mti;
}rng_t;

void rng_seed(rng_t *rng, uint64_t seed);
uint64_t rng_rand64(rng_t *rng);
uint64_t scramble( int scale, uint64_t v0, uint64_t val0, uint64_t val1 );
/** Random number generator related stuff **/

