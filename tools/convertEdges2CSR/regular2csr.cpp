#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

int main(int argc, char* argv[] ){

	ifstream f(argv[1]);
	ofstream out(argv[2]);

	if (!f.good()){
		std::cout << "File: " << argv[1] << " bad." << std::endl;
		return -1;
	}

	std::cout << "Input: " << argv[1] << endl;
	std::cout << "Output: " << argv[2] << endl;

	string line;
	getline(f, line);
	getline(f, line);
	getline(f, line);
	getline(f, line);

	int total_edges = 0;
	int total_nodes = 0;

	int from, to, weight;

	while(!f.eof())
	{
		f >> from;
		if (from > total_nodes)
		{
			total_nodes = from;
		}
	}
	total_nodes += 1;
	cout << "Max node: " << total_nodes << endl;
	std::vector <int > nodes (total_nodes,-1);

	f.clear();
	f.seekg(0, f.beg);

	getline(f, line);
	getline(f, line);
	getline(f, line);
	getline(f, line);

	while(!f.eof())
	{
		f >> from;
		nodes[from] = 1;
	}

	int counter = 0;
	for (int i = 0; i < total_nodes; ++i)
	{
		if(nodes[i] == 1) 
		{
			nodes[i] = counter;
			counter++;
		}
	}
	std::cout << "Effective nodes: " << counter << std::endl;

	vector< std::vector<int> > edges (counter, vector<int>());

	f.clear();
	f.seekg(0, f.beg);

	getline(f, line);
	getline(f, line);
	getline(f, line);
	getline(f, line);

	while( !f.eof() )
	{
		f >> from;
		from = nodes[from];
		f >> to;
		to = nodes[to];

		if (from == -1  || to == -1 )
		{
			std::cout << "from == -1  || to == -1: THIS CANNOT HAPPEN" << std::endl;
		}

		int flag = 0;

		for (int i = 0; i < edges[from].size(); ++i)
		{
			if (edges[from][i] == to)
				flag = 1;
		}

		if (flag == 0)
			edges[from].push_back(to);

		flag = 0;
		for (int i = 0; i < edges[to].size(); ++i)
		{
			if (edges[to][i] == from)
				flag = 1;
		}

		if (flag == 0)
			edges[to].push_back(from);
	}

	out << counter << endl;
	int init = 0;
	for (int i = 0; i < edges.size(); ++i)
	{
		out << init << " " << edges.at(i).size() << endl;
		init += edges.at(i).size();
		if (edges.at(i).size() == 0)
		{
			std::cout << "Unconnected node???" << std::endl;
		}
		total_edges+= edges.at(i).size();
	}

	out << endl << "27" << endl << endl; // Source Node
	out << total_edges << endl;

	for (int i = 0; i < edges.size(); ++i)
	{
		for (int j = 0; j < edges.at(i).size(); ++j)
		{
			out << edges[i][j] << " " << 1 << endl;
		}
	}

	cout << "No of Nodes: " << counter << endl;
	cout << "No of Edges: " << total_edges << endl;

	return 0;
}
