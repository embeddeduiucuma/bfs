\section{Implementation Details}
\label{sec:impl}

Our CPU BFS implementations (\td and \bu) are written using C++ and
OpenMP. The data structure to store the graph is, as usual, CSR
(Compressed Sparse Representation)~\cite{CSR}. This is, we store the graph in two
vectors: i) the vector $e$ with $M$ edges, and ii) the vector $v$ that
stores $N$ vertices in a $N + 1$ array. According to the CSR format,
the edges connecting $v[i]$ are stored in $v$ in the indices indicated by
$e[v[i]]...e[v[i+1]-1]$.  Three additional vectors of length $N$ are
also needed: the distance vector $dist$ and two bitmask vectors
$bm_{old}$ and $bm_{new}$. The bitmasks identify the vertices of $v$ that
belong to the \textit{old} and \textit{new} frontiers, respectively, as described in
Algorithm~\ref{alg:bfs_td}. 

% RAFA:here it said \td and \bu algorithms, but \bu does not iterate
% over the frontier, right?
Originally, the CPU \td CPU algorithm was implemented using OpenMP
\emph{parallel for} in the loop $i$, going over the whole bitmask
vector, $bm_{old}$, but processing only the vertices $v[i]$ such that
$bm[i]==true$. If the frontier is small, this is suboptimal since
$bm[i]$ is usually false (since only a few $v[i]$ belong to the frontier). To
avoid this overhead, we optimized the implementation following an
inspector-executor approach. First, at the beginning of each iteration
of the \textit{Serial while}, the bitmask $bm_{old}$ is explored to
produce a dense frontier $F_{old}$ (inspector step). Then, this dense
frontier is processed in a parallel loop (executor step). Although the
inspector step is implemented in parallel relying on a prefix sum, we
found out that for small graphs (with less than 1M vertices), the
inspector step does not pay off. However, for larger graphs, which are
actually the ones requiring parallel processing, the overhead of inspector 
step is later amortized during the executor step. Our experiments show that 
for a graph of 16M vertices, the inspector-executor approach is 3.5 times faster than the
original implementation, and thus, this optimization was used in the
CPU \td implementation. With respect to the CPU \bu 
implementation, a \emph{parallel for}, $i$, traverses all the vertices,
$v[i]$, but only when $dist[i]==\infty$ the corresponding neighbors,
$e[v[i]]...e[v[i+1]-1]$, are checked. As soon as one of the neighbors
is found in the old frontier, the vertex $v[i]$ is added to the new
one by updating $bm_{new}[i]=true$. 

The GPU and heterogeneous \selective~ and \concurrent~ implementations
use OpenCL 1.2. In the case of the OpenCL \td kernel the
inspector-executor approach does not pay off due to three reasons:\\ 
i) the cost of the inspector step to generate the dense frontier; ii)
consecutive vertices in the dense frontier processed by the same GPU
warp contain non-coalesced memory accesses that are slower to process
due to memory divergence; and iii) the amount of parallelism in a
dense frontier is reduced with respect to processing the original
frontier. We have experimentally validated that the best performance is
achieved when the GPU process that original (sparse) frontier. Although with
this approach some of the threads of the GPU warp end up doing
nothing, this control divergence is not as expensive as the memory
divergence of the inspector-executor approach. The \bu OpenCL kernel
is essentially equivalent to the CPU \bu implementation.

% Since the GPU
% has fine-grained threads, iterating only over the vertices that are
% going to be processed allowed us to have less threads and avoid
% scheduling overheads, but introducing more irregularity in the memory
% access. This memory irregularity emerges since the frontier vector
% contains vertices that can be far away in terms of memory location,
% and locality cannot be exploited, harming the overall
% performance. Simple experiments showed that it is best in terms of
% performance to launch more GPU threads, some doing nothing, but
% keeping good memory locality, and we used the bitmask approach always
% in the case of OpenCL kernels.

Notice that OpenCL 1.2 version does not allow coherent read/write
operations over the same memory locations from both GPU and
CPU. However, in our \concurrent~implementation, during the CPU-GPU
\bu phase, the CPU and GPU
write in different non-overlapping regions of the output vectors
($dist$ and $bm_{new}$) so this is not a problem.

% In the \concurrent
% approach reads and writes are performed over the same memory locations, 
% concurrently, as part of the algorithm. Since the \concurrent approach 
% only uses the \bu method concurrently, the only possible conflict will 
% occur in those vertices that lays in the limit of the division between 
% the region assigned to CPU and the one assigned to GPU. By making sure 
% that this limit falls in an aligned memory address, no cache coherence 
% problem was found. 

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% End:
