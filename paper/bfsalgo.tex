\section {Background on BFS}
\label{sec:bfs_algo}

Given an undirected graph $G(V,E)$ and a source vertex $s_0$, 
a breadth-first search traverses $G$ starting from $s_0$ and exploring 
all the vertices at a given distance $d$ from $s_0$ before traversing further vertices.
The classic \td implementation of BFS is shown in
Algorithm~\ref{alg:bfs_td}, which results in an array 
of distances for every vertex with respect to $s_0$.

\begin{comment}
\begin{algorithm}
\textbf{Input:}  Graph ($V$,$E$), source vertex $s_0$ \\
\textbf{Output:} Distances from $s_0$ $Dist[1..|V|]$ \\
\begin{algorithmic}[1]
\FORALL{$  v \in V  $}
\STATE $dist[v] = \infty$
\ENDFOR
\STATE $dist[s_0] = 0 $
\STATE $ Q = \emptyset $
\STATE $ Q.enqueue(s_0) $ 
\WHILE{ $Q \neq \emptyset $ }
\STATE $i = Q.dequeue()$
\FOR{ each neighbor $v$ of $i$ } 
\IF { $dist[v] == \infty$ }
\STATE $dist[v] = dist[i] + 1$ \label{lst:plusone}
\STATE $Q.enqueue(v)$
\ENDIF
\ENDFOR 
\ENDWHILE
\end{algorithmic}
 \caption{Breadth-First Search}
 \label{alg:bfs_td}
\end{algorithm}
\end{comment}

A vertex frontier is the subset of vertices that are discovered for
the first time and are enqueued in each iteration of the serial
\textbf{while} loop
(lines~\ref{lst:serialloop}-\ref{lst:eserialloop}). The first
frontier, $F_{old}$ contains just the source vertex $s_0$.  After each
iteration of the parallel \textbf{for}
(lines~\ref{lst:outerloop}-\ref{lst:eouterloop}), a new frontier,
$F_{new}$ is discovered, which contains all the vertices at distance 1
from the vertices in $F_{old}$. When all the vertices have been
visited, $F_{new}$ becomes empty ($\nexists v \mid dist[v]==\infty$),
then $F_{old}=\emptyset $ and the algorithm ends.

\begin{algorithm}
\begin{small}
\textbf{Input:}  Graph ($V$,$E$), source vertex $s_0$ \\
\textbf{Output:} Distances from $s_0$ $Dist[1..|V|]$ \\
\begin{algorithmic}[1]
\ForAll{$  v \in V  $}
\State $dist[v] = \infty$
\EndFor
\State $dist[s_0] = 0 $
\State $ F_{old} = \emptyset $
\State $ F_{old}.enqueue(s_0) $ 
\While{ $F_{old} \neq \emptyset $} \Comment{Serial while}\label{lst:serialloop}
\ForAll{$i$ in $F_{old}$} \Comment{Parallel loop}\label{lst:outerloop}
\For{each neighbor $v$ of $i$ }\label{lst:innerloop} 
\If { $dist[v] ==\infty$ }
\State $dist[v] = dist[i] + 1$ \label{lst:plusone}
\State $F_{new}.enqueue(v)$
\EndIf
\EndFor
\State $F_{old}=F_{new}$
\State $F_{new}=\emptyset$
\EndFor \label{lst:eouterloop}
\EndWhile  \label{lst:eserialloop} 
\end{algorithmic}
\end{small}
 \caption{Breadth-First Search}
 \label{alg:bfs_td}
\end{algorithm}
\vspace*{-9mm}

Coarse-grained parallelism can be exploited using OpenMP to distribute
chunks of the iteration space of the parallel loop among the
cores. However, the GPU excels at exploiting fine-grained parallelism,
which can be put to work by processing each vertex $i$
of the parallel loop by a GPU thread. 

The \td algorithm just described may perform redundant work since
some neighbors $v$ of a vertex $i$ may have already been visited in
this or in previous iterations of the while loop (these $v$'s are already
part of the new frontier $F_{new}$ or of the previous $F_{old}$ frontiers). 
This incurs in a waste of computing time and power. 

An alternative implementation that avoids this problem is known as the
\bu algorithm. With this alternative, the vertices that have not been
discovered yet, check if they have a parent in the frontier. To do
that, each non-discovered vertex traverses its edges. If a vertex
finds a parent in the frontier it updates its distance and adds itself
to the new frontier. This way, a vertex that finds a
parent in the frontier, stops processing the rest of its edges,
eliminating the redundant work.  Such situation is likely to occur
when an important part of the graph has already been processed.  The
disadvantage of this method is that all unvisited vertices need to be
explored, and for those that do not find a parent in the frontier, the
algorithm performs useless work and wastes power.

An extensive exploration about which method (\td/\bu -- coarse/fine
grained) is more convenient to use has been done in
\cite{Beamer:2012:DBS:2388996.2389013}.  The \td method has the
advantage that only the vertices in the frontier are explored,
whereas the \bu method needs to explore all the unvisited vertices.
Using the \bu method in the first iterations of the graph traversal
will result in the exploration of the majority of the vertices (as
only a few would have been visited in the beginning) and in the
exploration of almost all the neighbors (as it would be difficult for
a vertex to find a parent in the frontier because this will be
small). However, using the \td method during the first iterations will
result in less redundant work, as only the vertices in the frontier
will be explored, and the majority of their neighbors will be
unvisited at that point.  As iterations execute vertices are visited and there is a point when the \bu method performs less useless work, whereas the \td performs a larger amount of redundant work.

\begin{figure}[]
    \begin{center}
        \subfigure[Roads of California: 1.96M Vertices]{%
            \label{fig:regular}
            \includegraphics[width=0.43\textwidth]{img/histo_roadCA}
        }\\ %  ------- End of the first row ----------------------%
\vspace*{-3mm}
        \subfigure[Youtube Graph: 1.17M Vertices]{%
           \label{fig:socialnetworks}
           \includegraphics[width=0.43\textwidth]{img/histo_youtube}
        }
    \end{center}
\vspace*{-4mm}
    \caption{%
            Comparison between Social Network and Road Graphs.   
     }%
   \label{fig:graph_comparison}
\vspace*{-3mm}
\end{figure}

\subsection{Social Network Graphs}

BFS applied to social network graphs started to gain relevance
recently due to the proliferation of social network services.
Figure~\ref{fig:graph_comparison} represents, for all the
iterations of the \textit{Serial while} of Algorithm~\ref{alg:bfs_td},
i.e. for each frontier, how many vertices it has, \emph{Frontier}
line, and how many edges are traversed when processing it,
\emph{Edges} line.  Figure~\ref{fig:regular} shows the case of a
regular graph like the roadCA graph. This graph represents a street
map, where every vertex has a similar and small number of edges. This
translates into several iterations (more than 500) needed to reach
every vertex in the graph, where all the iterations perform a similar
amount of work. As the figure shows, the number of visited vertices and traversed edges
are within the same order of magnitude across iterations.


On the other hand, Figure~\ref{fig:socialnetworks} depicts the
scenario on social networks.  In this case, the Youtube social network
is traversed in only 14 iterations.  The plot also shows that the
amount of edges explored by some iterations (2 to 4 in this graph) is
one order of magnitude higher than for the rest of the
iterations. This presents both an advantage and challenge for
heterogenous platforms, as the number of iterations (and thus,
synchronization) is small, but the workload is not evenly distributed
among the iterations. The need for fewer iterations when traversing
the graph brings new opportunities to platforms that are very
sensitive to synchronization barriers (implicit or explicit), such as
GPUs, and thus, heterogenous platforms may find its way for this kind
of graphs.  In particular, having fewer iterations means fewer
barriers and overheads; in addition, having a higher amount of work per
iteration might be profitable for GPU acceleration.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% End:
