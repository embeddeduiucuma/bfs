\section{Approaches}
\label{sec:approaches}

We have designed and implemented two heterogeneous approaches for performing BFS.
In the first approach, referred as \selective,  
some iterations of the \textit{Serial while} of
Algorithm~\ref{alg:bfs_td} (line~\ref{lst:serialloop}), i.e. some
frontiers, are processed on the CPU while others are processed on the GPU. The
device is selected based on the size of the frontier and 
on how many vertices have already been visited. With this
approach, both devices are never exploited simultaneously.
The second approach is called \concurrent, where 
both devices are used at the same time to process some specific frontiers. 
These frontiers are split between the devices and the graph is
traversed concurrently.
We further elaborate on these approaches next.
% Both method relies on observations over social network graph behavior. 
% In the initial iterations, only a few vertices are explored, but after 
% 10 to 15 percent of the graph has been traversed, the number of 
% vertices in the new frontiers increases exponentially. In this part 
% of the processing, the GPU will be used to explore the frontier alone (\selective), 
% or in collaboration with the CPU (\concurrent). 
\vspace*{-1mm}
\subsection {Selective}

The \selective~approach is based on the observation that the CPU can
process a given frontier faster than the GPU when the number of
vertices in the frontier is small. However, when the frontier is big
enough, the GPU can extract enough parallelism and explore
the frontier faster than the CPU cores. Therefore, the \selective~
approach starts processing the first frontiers on the CPU, and switch
to the GPU when the number of vertices in the frontier is above
a threshold. With this approach, a frontier is executed on the
device which fits best. A similar methodology has been proposed previously
by Munguia et al \cite{6507474}, but from the perspective of a
task-based framework using discrete GPUs and by Daga et al
\cite{7004254}, using a heuristic similar to the one proposed by Beamer et al
\cite{Beamer:2012:DBS:2388996.2389013} to switch from using \td on CPU
to use \bu on GPU.  Their methodology requires counting both vertices and edges in the frontier, 
which can add a significant overhead when processing a frontier on the GPU.
Counting edges requires exploring the frontier and adding up all the 
connections of each vertex, which is an expensive operation. 

To avoid counting the number of edges in the frontier, in our
heuristic we switch from the \td CPU phase to the \bu GPU phase when more than
10\% of the graph has been visited, which is cheaper to detect. We
have experimentally evaluated different thresholds, finding that for
our graphs this is the one that produces the best results. In fact, for
the big social network graphs that we have used in our experiments,
after 10\% of the graph is processed, the frontier has more than 40K
vertices, which makes the GPU the suitable device to process 
such a big frontier faster.  After this point, our \selective~approach
processes three frontiers on the GPU using \bu approach. To reduce the
overhead of GPU kernel launching, three GPU kernels
are dispatched to the OpenCL queue. This is an in-order queue where
the three frontiers are executed one after the other. In our
experiments, these three frontiers usually process around 80\% of the vertices and
the size of the remaining frontiers are likely to be again too small
to be processed on the GPU. If after processing 3 frontiers on the GPU
the amount of processed vertices is smaller than 90\% (10\% in
the CPU phase plus 80\% in the GPU one),
additional kernels are enqueued to the GPU one by one until this
conditions is met. Finally, when less than 10\% of the vertices remain, 
%to be processed, 
the CPU is back in charge of running the last
frontiers using the \bu algorithm.

% By doing this, overhead from the kernel
% launching is reduced at expenses of, possibly, performing more
% iterations than necessary, but all the graph analyzed will need at
% least 3 iterations after passing the 10-15\% barrier, meaning that
% this will rarely be the case.  Since during these critical iterations
% most of the graph is traversed, after the last kernel is executed
% likely more than 90\% of the graph will be traversed, so the following
% frontier are small enough to be processed in the CPU. If that is not
% the case, kernels are launched one by one until this condition occurs.

% It is important to note that the frontier that will be executed in the
% GPU, after 10-15\% of the graph has been traversed, will account for
% more than 80\% of the total execution time, making negligible the
% execution time of the last iterations.

%This approach can be implemented using different combinations of method (i.e. \td and \bu methods): 
%The first iterations can be explored by using \td method, then, when the frontier is bigger than 
%a threshold, process some iterations in the GPU using \td or \bu method, and during the final iterations 
%the \bu method can be used to complete the exploration in CPU. 
%The threshold will depend on several factors, 
%including the platform (which means the ratio between Traversed Edges Per Second on CPU and GPU), 
%as well as the method used (\td or \bu). In some cases, the use of the \td algorithm 
%will be faster than the \bu algorithm in CPU, and sometimes the opposite will happen in the GPU. 
\vspace*{-1mm}
\subsection {Concurrent}

In this approach, both devices are used at the same time to process
the frontiers that are large enough. The idea is to further reduce the
execution time by simultaneously exploiting the CPU and the GPU. As in
the \selective~approach, when 10\% of the vertices have been
processed, we switch to a CPU-GPU phase, in which the workload of a
parallel iteration is distributed between the GPU and the CPU
cores. Section~\ref{sec:memory} discuss the experimental results
obtained when applying different workload partitions. In this phase,
in principle, both \td and \bu methods can be used.  If we rely on the
\td method, the frontier is created in the CPU from a global bitmask,
indicating which are the vertices belonging to the frontier. Then, a
portion of those vertices are assigned to the GPU by creating a new
GPU-specific bitmask (this incurs a high overhead). On the other hand,
the \bu approach does not involve any new GPU-specific bitmask
creation. As we explain in the next section, the \concurrent~\bu
approach splits the vertices between the CPU and the GPU. Then, both
devices will process their own set of vertices concurrently. For each
vertex, $v$, the CPU or the GPU has to check if it has not been
visited yet ($dist[v]==\infty$ ) and in that case if it has a
neighbor, $i$, in the old frontier, $F_{old}$. For this, it reads the
corresponding flag in the global bitmask. If this is the case, the vertex's
distance is updated ($dist[v]=dist[i]+1$) and the vertex is added to
the new frontier, $F_{new}$, updating a different global bitmask (the
new frontier bitmask). Since the CPU and the GPU are traversing
different vertices, they are reading different regions of the distance
vector and of the old frontier bitmask, as well as writing to different
non-overlapping regions of the distance vector and new frontier
bitmask. Therefore this \bu alternative can be heterogeneously
implemented without introducing any overhead, and it is the chosen
approach to process the large frontiers of the graph. As in the
\selective~ approach, when more than 90\% of the vertices are
processed, we switch back to a \bu CPU-only phase.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% End:
