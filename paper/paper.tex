
\documentclass[conference]{IEEEtran}
% Add the compsoc option for Computer Society conferences.
%
% If IEEEtran.cls has not been installed into the LaTeX system files,
% manually specify the path to it like:
% \documentclass[conference]{../sty/IEEEtran}


\usepackage{subfigure, graphicx}
%\usepackage[bookmarks=false]{hyperref}
\usepackage[draft]{hyperref}
%\usepackage{nohyperref}  % This makes hyperref commands do nothing without errors
%\usepackage{url}
\usepackage{algorithm}
\usepackage{algorithmicx}
\usepackage{algpseudocode}
\usepackage{listings}
\usepackage{comment}
\usepackage{amssymb}
\usepackage{framed}


\lstset{ %
language=C++,                % choose the language of the code
basicstyle=\footnotesize,       % the size of the fonts that are used for the code
%numbers=left,                   % where to put the line-numbers
%numberstyle=\footnotesize,      % the size of the fonts that are used for the line-numbers
%stepnumber=1,                   % the step between two line-numbers. If it is 1 each line will be numbered
%numbersep=5pt,                  % how far the line-numbers are from the code
%backgroundcolor=\color{white},  % choose the background color. You must add \usepackage{color}
showspaces=false,               % show spaces adding particular underscores
showstringspaces=false,         % underline spaces within strings
showtabs=false,                 % show tabs within strings adding particular underscores
frame=single,           % adds a frame around the code
tabsize=2,          % sets default tabsize to 2 spaces
captionpos=b,           % sets the caption-position to bottom
breaklines=true,        % sets automatic line breaking
breakatwhitespace=false,    % sets if automatic breaks should only happen at whitespace
escapeinside={\%*}{*)}          % if you want to add a comment within your code
}

% correct bad hyphenation here
\hyphenation{op-tical net-works semi-conduc-tor}

\begin{document}
%
% paper title
% can use linebreaks \\ within to get better formatting as desired
\title{Breadth-First Search on Heterogeneous Platforms: \\
A Case of Study on Social Networks}
% A Case of Study on Highly-Connected Graphs}


% author names and affiliations
% use a multiple column layout for up to three different
% affiliations

\author{\IEEEauthorblockN{
	Luis Remis\IEEEauthorrefmark{1}\IEEEauthorrefmark{3}, 
	Maria Jesus Garzaran\IEEEauthorrefmark{1}\IEEEauthorrefmark{3}, 
	Rafael Asenjo\IEEEauthorrefmark{2} and 
	Angeles Navarro\IEEEauthorrefmark{2}
}
\IEEEauthorblockA{
\begin{tabular}{ccc}
   \begin{tabular}{@{}c@{}}
		\IEEEauthorrefmark{1}Department of Computer Science\\ University of Illinois at Urbana-Champaign
		% \{remis2, garzaran\}@illinois.edu
   \end{tabular} &
   \begin{tabular}{@{}c@{}}
		\IEEEauthorrefmark{2}Universidad de M\'{a}laga, Spain\\
		\{asenjo, angeles\}@ac.uma.es
	\end{tabular} &
   \begin{tabular}{@{}c@{}}
		\IEEEauthorrefmark{3}Intel Corp.\\
		\{luis.remis, maria.garzaran\}@intel.com
	\end{tabular} 
\vspace*{-3mm}
\end{tabular}
}
}


% make the title area
\maketitle
\newcommand{\td}{Top-Down }
\newcommand{\bu}{Bottom-Up }
\newcommand{\concurrent}{\texttt{Concurrent}}
\newcommand{\selective}{\texttt{Selective}}

\input{abstract}

\IEEEpeerreviewmaketitle
\vspace*{0mm}
\input{introduction}
\vspace*{0mm}
\input{bfsalgo}
\vspace*{0mm}
\input{approaches}
\vspace*{0mm}
\input{implementation}
\vspace*{-1mm}
%\input{setup}
\input{experiments}
\vspace*{0mm}
\input{previous}
\vspace*{0mm}
\input{conclusion}
\vspace*{0mm}
\section*{Acknowledgments}
This work is supported in part by NSF under grant \mbox{CNS-1319657}
and by  Spanish projects TIN2013-42253-P and  P11-TIC-08144. 
\vspace*{-3mm}

\bibliographystyle{IEEEtran}
\bibliography{paper}

\scriptsize
\begin{framed}
Intel and Intel Core are trademarks of Intel Corporation in the U.S. and/or other countries.

Software and workloads used in performance tests may have been optimized for
performance only on Intel microprocessors. Performance tests, such as SYSmark
and MobileMark, are measured using specific computer systems, components,
software, operations and functions. Any change to any of those factors may cause
the results to vary. You should consult other information and performance tests
to assist you in fully evaluating your contemplated purchases, including the
performance of that product when combined with other products. For more
information go to http://www.intel.com/performance.

% Intel's compilers may or may not optimize to the same degree for non-Intel
% microprocessors for optimizations that are not unique to Intel
% microprocessors. These optimizations include SSE2, SSE3, and SSSE3 instruction
% sets and other optimizations. Intel does not guarantee the availability,
% functionality, or effectiveness of any optimization on microprocessors not
% manufactured by Intel. Microprocessor-dependent optimizations in this product
% are intended for use with Intel microprocessors. Certain optimizations not
% specific to Intel microarchitecture are reserved for Intel microprocessors.
% Please refer to the applicable product User and Reference Guides for more
% information regarding the specific instruction sets covered by this notice.
\end{framed}

% that's all folks
\end{document}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
