\section{Experimental Evaluation}
\label{sec:experimental}

% \input{gpusize}

% \input{repeated}
\input{setup}

\subsection{Performance and energy efficiency evaluation}
\label{sec:performance}

An experimental evaluation was made over different platforms in order
to understand the impact of the approaches on performance and energy
efficiency. Figures~\ref{fig:performance} and~\ref{fig:power} show the
performance and energy evaluations. Performance results are shown
using the metric {\textit Millions of Traversed Edges Per Second}
(MTEPS)~\cite{7004254}. This metric, as used in the literature, results from
dividing the number of edges in a graph by the execution time, thus it
provides a normalization of performance across graphs with different
number of vertices and edges, and therefore, different running times.
For energy efficiency, the metric used was performance per watt, or
Millions of Traverse Edges per Joule (MTEPJ). 
Power is measured using libraries provided by the vendors 
(Intel\textsuperscript{\textregistered} PCM and an in-house library that access power 
dissipation readouts in the Odroid).
For collecting the
results, the BFS algorithm was run 500 times from different source
vertices, $s_0$, and the average was calculated.


In the figures, we show the evaluation when BFS is configured with the
method Top-Down and launched on CPU (\texttt{CPU TD}) or on GPU
(\texttt{GPU TD}), and when BFS is configured with the method
Bottom-Up and launched on CPU (\texttt{CPU BU}). Results for the
Bottom-Up method on GPU are not shown because the first iterations are
too slow, so the executions take too long. Also, for the performance
evaluation and to establish a baseline, we compare our approaches
named \selective~and \concurrent, against an \texttt{Oracle} execution
which consider both methods (TD, BU) and devices (CPU, GPU). In
particular, for each graph we collected the time that each method
spent on each iteration (or frontier) when running on each
device. Then, the oracle is calculated by taking, for each iteration,
the minimum execution time for each method and device, and adding up
those minimums for all the iterations. Thus, ideally the oracle
represents the minimum execution time when the best method and device
is chosen for each iteration, without incurring any
overhead. Also,
for the performance results another baseline is considered:
\texttt{Best Galois}. For each graph, it represents the best result
obtained among the different implementations that Galois offers for
BFS~\cite{galois}.  Galois is a well-optimized and state-of-the-art
suite of different algorithms for graph applications, which makes
efficient use of all CPU cores present in the system, so these results
represent a reference for the CPU executions.


Figure \ref{fig:results_odroid} shows the performance results obtained
after running different graphs on the Odroid platform. As we see, the
\selective~and the \concurrent~heterogeneous approaches work better than the
implementations where only one device is used. 
%The figure depicts,
%from left to right, the \bu and \td methods implemented for CPU cores,
%a \td approach for GPU only, and finally, the \selective~and the
%\concurrent~approaches. 
An average speedup of 1.41x and 1.40x was obtained for \selective~and
\concurrent~approaches respectively, with speedups of up to 1.52x and
1.56x for the Rodinia graphs. The speedups have been calculated
against the best of the only one device executions (\texttt{CPU TD},
\texttt{CPU BU} or \texttt{GPU TD}) for each graph.
%%% Ask Luis if reference for speedup is CPU TD 
%These speedups are due to
%a better use of the resources, since the \selective~approach, uses the
%best device to perform the work, and with the \concurrent~approach,
%there are certain iterations where all the resources are fully
%utilized.
As we see, although the \concurrent~approach uses both devices
simultaneously, it only does so in some of the iterations. Although these
iterations typically represent around 80\% of the execution
time, the improvement with regards \selective~is small on some of the
graphs (the larger ones), but on average it does not outperform
\selective. In section~\ref{sec:memory} we further explore this issue
pointing out that the congestion on the memory bus, when both devices are
working concurrently, increases the number of stall cycles seen by both
devices. This, in turn,  degrades performance. On the other hand, 
an average speedup of 1.57x was obtained by \texttt{Oracle}
which indicates that the overhead of the \selective~approach is below 10\%.

One interesting result is that Galois performed poorly when compared
with the heterogenous approaches in all the social network
graphs. However, Galois performed several times better in road
networks.  Let's remind that our approaches use CSR to represent the
graphs, while Galois uses more general and complex data structures
(sets and graphs) that take advantage of asynchronous graph traversals.
%This is because Galois has data structures that do not favor locality as much 
%as CSR, as it is a more general 
%framework with the ability to perform multiple graph algorithms. 
The use of CSR provides the ability of exploring a frontier 
fast because of locality, but there is a need for synchronization
after each iteration that Galois implementations may avoid. 
%when using 
%the \td and \bu methods. 
Because of this, graphs that are highly connected but need a small
number of iterations like social network graphs,   
will benefit more from locality and will likely be less affected due
to synchronization barriers when the data structure used is CSR. 
In the case of road networks with lowly connected graphs and a high
number of iterations, the amount of work per iteration will be small
and the synchronization overhead will be more significant in
approaches based on CSR and thus these graphs will
benefit from the asynchronous approaches implemented in Galois. 

% In the case of the Amazon and the roads networks, 
% the oracle is slower than the \selective~and/or \concurrent~approaches, as the 
% oracle is calculated considering all the synchronization barriers after each iteration, 
% whereas in the case of the heterogeneous approaches, several kernels are 
% launched together, reducing kernel 
% launching overhead.

In the case of the Core i7 (Figure \ref{fig:results_i7}), the results
of the heterogeneous approaches are not as good as the ones obtained
for Odroid, but this is an expected outcome as the GPU capabilities are
not comparable to the computing power of the CPU cores. For this
particular platform, an average speedup of 1.07x and 1.08x are achieved
for the \selective~and \concurrent~approaches respectively, with the
best results obtained by the Rodinia graphs with speedups of 1.14x and 1.46x. On
this architecture, \concurrent~performs better than \selective~because
the memory bus exhibits higher memory bandwith than on the Odroid, so it is not as
congested when both devices work concurrently.  

\begin{figure*}[]
    \begin{center}
       \subfigure[Performance evaluation on Odroid]{%
           \label{fig:results_odroid}
           \includegraphics[width=0.85\textwidth]{img/odroid_perfo}
\vspace*{-3mm}
        } \\%
        \subfigure[Performance evaluation on Core i7]{%
            \label{fig:results_i7}
            \includegraphics[width=0.85\textwidth]{img/i7_perfo}
\vspace*{-3mm}
        } %       
    \end{center}
\vspace*{-3mm}
    \caption{%
            Performance on Odroid and Core i7.
     }%
   \label{fig:performance}
\vspace*{-3mm}
\end{figure*}


Another platform where we performed the same experiments is Core i5
(not shown due to space constraints).
%Finally, the middle point between the Odroid platform and the Core i7
%platform can be the Core i5 processor.  
In this platform, both the CPU and GPU capabilities are bigger than in
the Odroid. The GPU is also bigger in this case when compared with the
Core i7 but the Core i5 has smaller CPU cores compared to the 4
high-end cores in the Core i7. Thus, it is a platform which
potentially can benefit more from heterogeneous approaches. In fact,
we find that improvements of the \selective~and 
  \concurrent~approaches are 1.31x and 1.33x respectively, on average for all the graphs. 
Figure \ref{fig:performance} summarizes the different
performance improvements for all the platforms.


Another important factor to consider on all platforms is the energy
efficiency obtained.  Figure~\ref{fig:power} shows how both the
\selective~and \concurrent~heterogeneous approaches are on average
1.28x and 1.32x more energy efficient on the Odroid, and 1.23x and
1.17x more energy efficient on Core i7 when they are compared to any
other approach that only uses one device. This energy efficiency can
be attributed to two main factors.  First, these approaches traverse
the graph in less time than any other approach, thus draining less
power for less time. Second, GPU at peak performance drains less power
than the CPU cores under similar stress, and these approaches make
more use of the GPU. However, we also notice that the
\concurrent~approach can be less energy efficient that \selective~when
the former can provide more performance (see for instance Youtube
graph in Odroid or rmat graph in Core i7). This reinforce the fact
that in heterogeneous computing not always minimum execution time
results in minimum energy consumption. Figure~\ref{fig:powerComp}
summarizes the energy efficiency improvements for all the platforms
(including Core i5 where we get 1.21x and 1.23x of
efficiency). Interestingly, on the Core i7 the improvement in energy
efficiency is larger than in performance for the heterogeneous
approaches. On the other hand, on the Odroid and Core i5, the energy
efficiency of the \concurrent~approach is slightly better than
\selective. In any case, the differences between the heterogenous
approaches are not big.
%The reason is
%becuase latter will 
%be using both devices at the same time during some iterations. But it is not 
%expected to find a big difference, as even if both are working at the same time, 
%the amount of work that each device is performing is less than in the case of 
%the \emph{Selective}, which will put a single device to work to its full capacity. 


\begin{figure*}[]
    \begin{center}
        \subfigure[Energy efficiency  evaluation on Odroid]{%
           \label{fig:power_odroid}
            \includegraphics[width=.85\textwidth]{img/odroid_power}
        } \\%
        \subfigure[Energy efficiency evaluation on Core i7]{%
            \label{fig:power_i7}
            \includegraphics[width=.85\textwidth]{img/i7_power}
        } %
    \end{center}
\vspace*{-3mm}
    \caption{%
            Energy efficiency evaluation on Odroid and Core i7.
     }%
   \label{fig:power}
\vspace*{-3mm}
\end{figure*}


\begin{figure*}[h]
    \begin{center}

        \subfigure[Performance comparison]{%
            \label{fig:performanceComp}
            \includegraphics[width=0.35\textwidth]{img/avg_perfo}
        } 
        \subfigure[Energy efficiency comparison]{%
           \label{fig:powerComp}
            \includegraphics[width=0.35\textwidth]{img/avg_power}
        } %

    \end{center}
\vspace*{-1mm}
    \caption{%
            Performance and Energy efficiency normalized for comparison between platforms.
     }%
   \label{fig:performance_power}
\vspace*{-1mm}
\end{figure*}

\subsection{Memory Bandwidth limitations}
\label{sec:memory}
 
Since heterogenous platforms provide a shared-memory environment, 
the data in memory can be used by both devices at the same time without 
requiring memory copies (\concurrent~approach). 
This is why it is so appealing to explore algorithms in which both devices
 can traverse the graph in parallel. But after implementing the 
approach aimed to exploit this characteristic and finding no notorious improvement, 
a more comprehensive study was performed around this matter,
in particular on the Odroid platform. 

\begin{figure}[]
    \begin{center}

        \subfigure[Memory Bound]{%
            \label{fig:memory_bound}
            \includegraphics[width=0.35\textwidth]{img/16M_memory_bound}
        } \\%
        \subfigure[Compute Bound]{%
           \label{fig:compute_bound}
            \includegraphics[width=0.35\textwidth]{img/16M_compute_bound}
        } %

    \end{center}
\vspace*{-1mm}
    \caption{%
            Comparison between memory bound (regular BFS) and compute
            bound (artificial BFS) in Odroid.
     }%
   \label{fig:memory_bandwidth}
\vspace*{-1mm}
\end{figure}


Figure~\ref{fig:memory_bound} shows how the \concurrent~
approach behaves
%reaches a memory bandwidth limitation
when both devices are working at the same time
(\texttt{CPU+GPU}). This experiment consisted on running the iteration
with the heaviest workload on both devices and measuring times when a
different percentage of the workload is offloaded to the GPU while the
CPU is working on the remaining load. The x-axis represents the
different partitions considered. We also measured the time that each
device would take to finish its part of the workload when working
alone in the system (\texttt{CPU Only} and \texttt{GPU Only}
results). The tests were performed using as input the Rodinia 16M
graph.  
%The first bar in the figure corresponds to the execution time
%of the CPU working on its part alone, the second bar corresponds to
%the execution time of the GPU working on its part alone, and finally,
%the last bar shows what is the execution time when both devices are
%working at the same.  
In the ideal case, the last bar is expected to
be as big as the max of the first two.  In other words, when both the
CPU and the GPU are working, it is expected that all the work will be
finished after the slowest device is done. But this is not the case,
and when both devices work at the same time, the memory system bus cannot
provide the devices with the necessary memory bandwidth.  When the number
of memory requests per time increases because both devices are
working at the same time, the bus gets congested, producing an
important number of stall cycles on both devices and 
preventing the devices to work at their peak
performance. We measured more than 40\% of performance degradation
in this case. This behavior is due to the
nature of the BFS processing, which is heavily memory bound.
%Each explored vertex
%only perform reads and writes to memory, and little to zero
%computation. 
To compare this benchmark with a similar but compute bound one, we added dummy operations on the BFS
kernels (several square root calculations).
% in order
%to transform the application into compute bound. 
Figure~\ref{fig:compute_bound} shows how in the case of this new compute bound
application, a \concurrent~approach would have the potential to achieve
close to ideal performance on the Odroid platform. Similar results
were observed on the Core i7 and Core i5 platforms.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% End:
