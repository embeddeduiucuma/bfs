#include "util.h"

//----------------------------------------------------------
//--Serial BFS on CPU for correctness check
//----------------------------------------------------------
int serial_correctness( struct Graph* graph, int *h_cost_result)
{
    char stop;
    int k = 0;

    uint32_t no_of_nodes = graph->no_of_nodes;
    uint32_t no_of_edges = graph->no_of_edges;
    uint32_t* h_graph_nodes = graph->nodes;
    uint32_t* h_graph_edges = graph->edges;
    uint32_t source = graph->source;

    char *h_graph_visited;
    char *h_graph_mask;
    char *h_updating_graph_mask;
    int* h_cost;

    h_graph_visited         = new char[no_of_nodes];
    h_graph_mask            = new char[no_of_nodes];
    h_updating_graph_mask   = new char[no_of_nodes];
    h_cost                  = new int [no_of_nodes];

    for (int i = 0; i < no_of_nodes; ++i){
        h_graph_visited[i]       = false;
        h_graph_mask[i]          = false;
        h_updating_graph_mask[i] = false;
        h_cost[i] = NODE_UNVISITED;
    }

    h_graph_mask[source]=true;
    h_cost[source] = 0;
    h_graph_visited[source] = true;

    do{
        //if no thread changes this value then the loop stops
        stop=false;
        for(int tid = 0; tid < no_of_nodes; tid++ )
        {
            if (h_graph_mask[tid] == true){
                h_graph_mask[tid]=false;

                for(int i=h_graph_nodes[tid]; i< h_graph_nodes[tid+1]; i++){
                    int id = h_graph_edges[i];  //--cambine: node id is connected with node tid
                    if(!h_graph_visited[id]){   //--cambine: if node id has not been visited, enter the body below
                        h_cost[id]=h_cost[tid]+1;
                        h_updating_graph_mask[id]=true;
                    }
                }
            }
        }

        for(int tid=0; tid< no_of_nodes ; tid++ )
        {
            if (h_updating_graph_mask[tid] == true){
                h_graph_mask[tid]=true;
                h_graph_visited[tid]=true;
                stop=true;
                h_updating_graph_mask[tid]=false;
            }
        }
        k++;
    }
    while(stop);

    int failed = compare_results<int>(h_cost, h_cost_result, no_of_nodes);

    delete h_graph_visited;
    delete h_graph_mask;
    delete h_updating_graph_mask;
    delete h_cost;

    return failed;
}


