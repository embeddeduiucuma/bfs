#ifndef _Graph_UTIL_
#define _Graph_UTIL_

#include <iostream>
#include <string>
#include "Graph.h"

void checkGraph(struct Graph* graph);

struct Graph* read_graph_from_file_Rodinia(std::string input_f);

void saveGraph2File(struct Graph* graph, const char* filename);
struct Graph* readGraphFromFile(const char* filename);


#endif