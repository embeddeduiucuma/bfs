#include <iostream>
#include <fstream>
#include <string>
#include <list>
#include "readGraph.h"
#include <string.h>
#include <stdlib.h>

/* Method for reading a file containing a grap in the following format:

NumberOfNodes
Node1StartIdx Node1NumberOfEdges
Node2StartIdx Node2NumberOfEdges
Node3StartIdx Node3NumberOfEdges
.
.
.

SourceNode

NumberOfEdges
Edge1 Cost
Edge2 Cost
.
.
.
*/

struct Graph* readGraphFromFile(const char* filename)
{
    std::ifstream file;
    file.open(filename, std::ios::binary);

    if (!file.good())
    {
        std::cout << "File " << filename << " not good" << std::endl;
        exit(0);
    }
    file.seekg (0, file.end);
    int length = file.tellg();
    file.seekg (0, file.beg);

    struct Graph* graph = new struct Graph;

    file.read( (char*) graph->name, NAME_SIZE);
    file.read( (char*) &(graph->no_of_nodes)   , sizeof(uint32_t));
    file.read( (char*) &(graph->no_of_edges), sizeof(uint32_t));
    file.read( (char*) &(graph->source)        , sizeof(uint32_t));

    graph->nodes = new uint32_t[graph->no_of_nodes+1];
    graph->edges = new uint32_t[graph->no_of_edges];
    file.read( (char*) graph->nodes, (graph->no_of_nodes+1) * sizeof(uint32_t));
    file.read( (char*) graph->edges, graph->no_of_edges * sizeof(uint32_t));
    file.close();

    return graph;
}

void saveGraph2File(struct Graph* graph, const char* filename)
{
    std::ofstream file;
    file.open (filename, std::ios::out | std::ios::binary);

    file.write( (char*) graph->name, NAME_SIZE);
    file.write( (char*) &graph->no_of_nodes   , sizeof(uint32_t));
    file.write( (char*) &graph->no_of_edges, sizeof(uint32_t));
    file.write( (char*) &graph->source        , sizeof(uint32_t));

    if (graph->nodes == NULL || graph->edges == NULL)
    {
        std::cout << "saveGraph2File: Graph not allocated correctly " << std::endl;
        exit(0);
    }

    file.write( (char*) graph->nodes, (graph->no_of_nodes+1) * sizeof(uint32_t));
    file.write( (char*) graph->edges, graph->no_of_edges * sizeof(uint32_t));
    file.close();
}

void checkGraph(struct Graph* graph)
{
    uint32_t no_of_nodes = graph->no_of_nodes;
    uint32_t no_of_edges = graph->no_of_edges;
    uint32_t source = graph->source;
    uint32_t *nodes = graph->nodes;
    uint32_t *edges = graph->edges;

    std::list<uint32_t > q;

    int*  h_cost = new int [no_of_nodes];
    int unvisited = 2000;

    for (uint32_t i = 0; i < no_of_nodes; ++i)
    {
        h_cost[i] = unvisited;
    }

    q.push_back( source );
    h_cost[source] = 0;

    while (!q.empty())
    {
        uint32_t idx = q.front();

        for(int id=nodes[idx]; id< nodes[idx] + (nodes[idx+1] - nodes[idx]); ++id)
        {
            int i = edges[id];

            if( h_cost[i] == unvisited )
            {
                q.push_back(i);
                h_cost[i]= h_cost[idx]+1 ;
            }
        }

        q.pop_front();
    }

    uint32_t counterUnconnected = 0;

    for (int i=0; i<no_of_nodes; i++)
    {
        if (h_cost[i] == unvisited)
        {
            ++counterUnconnected;
        }
    }

    if (counterUnconnected)
    {
        std::cout << "WARNING!!! Unconnected nodes: " << counterUnconnected << std::endl;
    }

    delete[] h_cost;
}

struct Graph* read_graph_from_file_Rodinia(std::string input_f)
{
    std::ifstream input(input_f.c_str());

    if(!input){
      std::cout << "Error Reading graph file " << input_f << std::endl;
      exit(-1);
    }

    // std::cout << "Reading graph " << input_f << std::endl;

    struct Graph* graph = new struct Graph;

    memcpy(graph->name, input_f.c_str(), input_f.length()+1 );

    input >> graph->no_of_nodes;

    graph->nodes = new uint32_t[graph->no_of_nodes+1];

    uint32_t aux;

    for(int i = 0; i < graph->no_of_nodes; i++)
    {
        input >> graph->nodes[i];
        input >> aux; // NOT BEING USED, DESCARTED
    }
    //read the source node from the file
    input >> graph->source;
    input >> graph->no_of_edges;

    graph->edges = new uint32_t[graph->no_of_edges];
    uint32_t cost;

    for(int i=0; i < graph->no_of_edges ; i++)
    {
        if (input.eof())
        {
            std::cout << "Reached en of the file when processing edge " << i << std::endl;
            break;
        }

        input >> graph->edges[i];
        input >> cost; // NOT BEING USED, DESCARTED
    }

    input.close();

    // std::cout << "Correct graph file reading: " << input_f << std::endl;
    // std::cout << "Graph Nodes: "  << graph->no_of_nodes     << std::endl;
    // std::cout << "Graph Edges: "  << graph->no_of_edges  << std::endl;
    // std::cout << "Graph Source: " << graph->source          << std::endl;

    // checkGraph(graph); // To detect unconnected nodes, not necessary for now.

    return graph;
}
