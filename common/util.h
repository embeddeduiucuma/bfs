#ifndef _C_UTIL_
#define _C_UTIL_

#include <math.h>
#include <stdlib.h>     /* srand, rand */
#include <iostream>
#include "Graph.h"

#define NODE_UNVISITED   (20000)
#define NODE_VISITED_GPU (10000)

//-------------------------------------------------------------------
//--initialize array with maximum limit
//-------------------------------------------------------------------
template<typename datatype>
void fill(datatype *A, const int n, const datatype maxi){
    for (int j = 0; j < n; j++)
    {
        A[j] = ((datatype) maxi * (rand() / (RAND_MAX + 1.0f)));
    }
}

//--print matrix
template<typename datatype>
void print_matrix(datatype *A, int height, int width){
    for(int i=0; i<height; i++){
        for(int j=0; j<width; j++){
            int idx = i*width + j;
            std::cout<<A[idx]<<" ";
        }
        std::cout<<std::endl;
    }

    return;
}
//-------------------------------------------------------------------
//--verify results
//-------------------------------------------------------------------
#define MAX_RELATIVE_ERROR  .002
template<typename datatype>
void verify_array(const datatype *cpuResults, const datatype *gpuResults, const int size)
{
    char passed = true;
    #pragma omp parallel for
    for (int i=0; i<size; i++)
    {
      if (fabs(cpuResults[i] - gpuResults[i]) / cpuResults[i] > MAX_RELATIVE_ERROR)
      {
        passed = false;
        //break; // No break inside omp for linux
      }
    }
    if (passed){
        std::cout << "--cambine:passed:-)" << std::endl;
    }
    else{
        std::cout << "--cambine: failed:-(" << std::endl;
    }
    return ;
}

template<typename type>
int compare_results(const type *golden, const type *result, const int size)
{
    char passed = true;
    int counterFailed = 0;

#ifdef OPENMP
    #pragma omp parallel for
#endif
    for (int i=0; i<size; i++) {
        if (golden[i] != result[i])
        {
            passed = false;
        }
    }

    if (!passed)
    {
        for (int i=0; i<size; i++)
        {
            if (golden[i] != result[i])
            {
                ++counterFailed;
            }
        }

        std::cout << "ERROR - CORRECTNESS FAIL: "
                  << counterFailed << std::endl;
    }

    return counterFailed;
}


//----------------------------------------------------------
//--Serial BFS on CPU for correctness check
//----------------------------------------------------------
int serial_correctness( struct Graph* graph, int *h_cost_result);

#endif

