//Structure to hold a node information

#ifndef _GRAPH_
#define _GRAPH_
#include <stdint.h>

#define NAME_SIZE 50
struct Graph
{
    char name[NAME_SIZE];
    uint32_t   no_of_nodes;             
    uint32_t   no_of_edges;
    uint32_t   source;
    uint32_t*  nodes;
    uint32_t*  edges;
};


#endif
