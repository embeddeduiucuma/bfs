//------------------------------------------
//--cambine:helper function for OpenCL
//--programmer:	Jianbin Fang
//--date:	27/12/2010
//------------------------------------------
#ifndef _CL_HELPER_
#define _CL_HELPER_

#ifdef __MACH__
#include <OpenCL/cl.h>
#else
#include <CL/cl.h>
#endif

#define __CL_ENABLE_EXCEPTIONS
#define ERRMSG

#define CL_USE_DEPRECATED_OPENCL_2_0_APIS

#include <string>
#include <vector>

//#pragma OPENCL EXTENSION cl_nv_compiler_options:enable

struct oclHandleStruct
{
    cl_context       context;
    cl_device_id     *devices;
    cl_command_queue queue;
    cl_program       program;
    cl_event		 event;
    cl_int		cl_status;
    std::string error_str;
    std::vector<cl_kernel>  kernel;
};

struct oclHandleStruct* getOCLHandler();

/*
 * Converts the contents of a file into a std::string
 */
std::string FileToString(const std::string fileName);

//---------------------------------------
//Read command line parameters
//
void _clCmdParams(int argc, char* argv[]);

//---------------------------------------
//Initlize CL objects
//--description: there are 5 steps to initialize all the OpenCL objects needed
//--revised on 04/01/2011: get the number of devices  and 
//  devices have no relationship with context

void _clInit(char* kernel_file, int total_kernels, std::string* kernel_names);

//---------------------------------------
//release CL objects
void _clRelease();
//--------------------------------------------------------
//--cambine:create buffer and then copy data from host to device
cl_mem _clCreateAndCpyMem(int size, void * h_mem_source) throw(std::string);

//-------------------------------------------------------
//--cambine:	create read only  buffer for devices
//--date:	17/01/2011	
cl_mem _clMallocRW(int size, void * h_mem_ptr) throw(std::string);
cl_mem _clMallocRWZC(int size, void * h_mem_ptr) throw(std::string);

//-------------------------------------------------------
//--cambine:	create read and write buffer for devices
//--date:	17/01/2011	
cl_mem _clMalloc(int size, void * h_mem_ptr) throw(std::string);

cl_mem _clMallocZC(int size, void * h_mem_ptr) throw(std::string);

//-------------------------------------------------------
//--cambine:	transfer data from host to device
//--date:	17/01/2011
void _clMemcpyH2D(cl_mem d_mem, int size, const void *h_mem_ptr) throw(std::string);
//--------------------------------------------------------
//--cambine:create buffer and then copy data from host to device with pinned 
// memory
cl_mem _clCreateAndCpyPinnedMem(int size, float* h_mem_source) throw(std::string);


//--------------------------------------------------------
//--cambine:create write only buffer on device
cl_mem _clMallocWO(int size) throw(std::string);

//--------------------------------------------------------
//transfer data from device to host
void _clMemcpyD2H(cl_mem d_mem, int size, void * h_mem) throw(std::string);

//--------------------------------------------------------
//set kernel arguments
void _clSetArgs(int kernel_id, int arg_idx, void * d_mem, int size = 0) throw(std::string);

void _clFinish() throw(std::string);
void _clFlush() throw(std::string);
//--------------------------------------------------------
//--cambine:enqueue kernel
void _clInvokeKernel(int kernel_id, int work_items, int work_group_size) throw(std::string);

void _clInvokeKernel2D(int kernel_id, int range_x, int range_y, int group_x, int group_y) throw(std::string);

//--------------------------------------------------------
//release OpenCL objects
void _clFree(cl_mem ob) throw(std::string);

#endif 
//_CL_HELPER_
