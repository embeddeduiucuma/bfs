%!TEX root = journal.tex
\section {Input Characterization}
\label{sec:inputcharacterization}
\vspace*{-0.2cm}
%BFS is an iterative algorithm. After each iteration,
%a new portion of the graph will be discovered
%and distance values will be updated.
%The traversing of the graph will take as many iterations as
%it is necessary in order to reach every node in the graph.
%In a graph where all edges have a distance
%of one, the number of iterations will be equal
%to the diameter of the graph (i.e., the longest shortest
%path between two vertices).


Graphs have different shapes and characteristics.
The number of vertices and the number of edges are just two of the many
dimensions that can characterize a graph.
Two graphs with similar number of vertices and edges but with a different
distribution of the edges can be notoriously
different in terms of the complexity when computing its BFS.
This is because graphs can represent problems of completely different
natures.
A typical example is a graph that describes the map of a city,
where each intersection of 2 or more streets
represents a vertex and each street itself represents an edge.
A city has a regular pattern of streets,
which  means that most of the vertices will have roughly
the same amount of edges, and that around 4 edges reach each vertex.


On the other hand, social networks, such us Facebook, Google+, or LinkedIn, are also represented by graphs. Here, each vertex
corresponds to a different person, and each edge represents a connection
(friendship or professional relation) between two people. In this case, some people are highly connected, i.e., they have numerous edges in the graph. Similarly, other people
are not very active in the network, or do not have many
connections, and have only a few edges in the graph. It is clear then that in this case, we will find a very irregular structure, with some vertices having
several connections while some others having an small number of connections.

\begin{figure*}[h!]
% \begin{figure*}[]
    \begin{center}

        \subfigure[Roads of California: 4M Vertices]{%
            \label{fig:graph_SF}
            \includegraphics[width=0.45\textwidth]{img/histo_roadCA}
        }
        \subfigure[New York Map Graph: 271K Vertices]{%
           \label{fig:graph_NY}
           \includegraphics[width=0.45\textwidth]{img/histo_ny}
        }\\ %  ------- End of the first row ----------------------%

        \subfigure[Google Graph: 875K Vertices]{%
           \label{fig:graph_google}
           \includegraphics[width=0.45\textwidth]{img/histo_google}
        }%
        \subfigure[Youtube Graph: 1.17M Vertices]{%
           \label{fig:graph_youtube}
           \includegraphics[width=0.45\textwidth]{img/histo_youtube}
        }\\%  ------- End of the first row ----------------------%

    \end{center}
  \vspace*{-4mm}
    \caption{%
            Regular Graphs (maps of New York and California, top) and
            Irregular Graphs (social networks, bottom).
     }%
   \label{fig:graphs}
  \vspace*{-1mm}
\end{figure*}


\subsection{Regular Graphs vs. Social Network Graphs}

For this work, well-know sources of graphs were used, such as the SNAP
collection~ \cite{snapnets}, the Graph500 benchmark graphs
generators~\cite{graph500}, and synthetic generated graphs from Rodinia
Benchmark~\cite{5306797}.
These sets of inputs provide a wide range of different
graphs, including regular and irregular graphs.
%scale-free and non-scale-free graphs.
For these graphs, we decided to collect profile information
such as the number of iterations (i.e. frontiers) that the \td
BFS method takes to traverse the graphs, the number of
edges that each iteration explores (\texttt{Edges}), the number of edges that
visit a vertex for the first time in each iteration (\texttt{Non-redundant
Edges}), and the number of vertices in the frontier ({\texttt{Frontier}).
Figure~\ref{fig:graphs} shows this data for graphs that
represent roads and social networks, respectively.
Non-redundant edges are illustrated in
Figure~\ref{fig:tdbu_comparison} with a dashed green line that connects some of
the vertices in the frontier.
For example, if in the \td method we traverse de frontier with
vertices 5 to 10, vertex 11 is visited for the
first time when exploring the neighbors in vertex 5, so this
edge is non-redundant.
However, the edge from vertex 7 to vertex 11 is redundant
because vertex 11 has already been visited. Note that in this figure,
no-redundant edges are depicted only for the edges that are traversed when
iterating over the vertices of the frontier with $dist=2$. For a different
frontier a different set of non-redundant
edges would have been obtained.


% \begin{figure*}[]
%     \begin{center}

%  %       \subfigure[Orkut Graph: 3M Vertices]{%
%  %           \label{fig:graph_orkut}
%  %           \includegraphics[width=0.5\textwidth]{img/histo_orkut}
%  %       }%
%         \subfigure[Google Graph: 875K Vertices]{%
%            \label{fig:graph_google}
%            \includegraphics[width=0.5\textwidth]{img/histo_google}
%         }%
%         \subfigure[Youtube Graph: 1.17M Vertices]{%
%            \label{fig:graph_Youtube}
%            \includegraphics[width=0.5\textwidth]{img/histo_youtube}
%         }\\%  ------- End of the first row ----------------------%

%     \end{center}
%   \vspace*{-4mm}
%     \caption{%
%             Irregular Graphs: Social Networks.
%      }%
%    \label{fig:social_network_graphs}
%   \vspace*{-1mm}
% \end{figure*}

Figure~\ref{fig:graphs} shows that there is a notorious difference in how
the BFS algorithm will perform when traversing regular graphs
in comparison with social network graphs.
In the case of very regular graphs (i.e. a graph
representing a street map, where every vertex has roughly the same number of
edges), a large number of iterations will be needed in
order to reach every vertex in the graph.
On the other hand, social network graphs (also referred as scale-free
graphs in some specific occasions) will only need around 15 or less iterations
before reaching every vertex. This behavior can be explained by following the
theory of six degree of separation or more formally, an experimental study of
the small-world problem, which demonstrated empirically how two people are
connected in a social graph after connecting, on average, 5.2 other people
~\cite{travers1969experimental}.
Due to this high connectivity, one can expect that after a small number
of iterations, all the vertices in the graph will be reached.
It is important to
note that due to the incompleteness of real graphs (as not every person in
the world is present in every social network),
more iterations than 6 are needed to reach every vertex.


Additionally, regular graphs (Figures ~\ref{fig:graph_SF} and
~\ref{fig:graph_NY}) have a similar small number of vertices in the frontier
(blue line) across iterations.
Thus, several iterations are necessary to completely traverse the
graph (more than 500).
Similarly, the number of edges traversed on each iteration is comparable across
iterations (red line), therefore all the iterations perform a comparable amount
of work. However, in social network (Figures ~\ref{fig:graph_google} and
~\ref{fig:graph_youtube}) graphs,
we see that in addition to the smaller number of iterations,
there are some iterations (5 to 7 in the Google graph and
2 to 4 in the Youtube graph) where the amount of
edges explored is one order of magnitude higher than in the rest of the
iterations.  The need for fewer iterations when traversing the entire graph
brings new opportunities to platforms that are very
sensitive to synchronization barriers (implicit or explicit),
such as GPUs and heterogenous platforms.
Having fewer iterations means fewer barriers,
and relatively high amount of work per iteration.
However, despite this advantage, there are still challenges for
heterogenous platforms, as the number of iterations (and thus, synchronization)
is small and the load is not evenly distributed among the iterations.


%We can imagine a graph of 1 million vertices
%and two scenarios: a very regular graph and a social network graph.
%In the first case, the traversing of the graph will take
%several iterations, but each iteration
%will perform comparable amount of work.
%In the latter case, only a few iterations will be needed,
%but each iteration will entitle considerable more work than in the first case.
%What it is more,
%some iterations will only process a small number of vertices,
%whereas some other iteration will
%perform work over frontiers that may take up to 50\% of the total
%amount of vertices.
%This means a very irregular work flow, as the amount of work
%performed by each iteration will not be comparable.


%Graphs generated by the Rodinia Generator are fully traversed in less than
%15 iterations, even though they have very different number of nodes
%(2M, 4M, and 8M).
%Besides, in only a small part of the iterations most of these
%graphs are traversed (from 7th to 12th iteration).
%On the other hand, both NY and SF graphs
%are traversed in a very different way, taking around 600 and 1000 iterations
%respectively.
%It is important to notice that NY graphs have only 271K vertices,
%and takes several iterations to complete compared to
%the Rodinia Generated 8M graph.
%As mentioned before, more iterations will result in more points
%for synchronization, which will affect
%the performance on implementations that have considerable
%overhead at the end of each iteration.


\subsection{Algorithmic inefficiencies}\label{sec:algineff}

When implementing parallel versions of the \td method to process a
frontier, some inefficiencies appear.
Consider the discovery of vertices in the frontier. In the
case of the \td method, for each vertex in the frontier,
all its neighbors are visited.
This means that vertices that have already been
discovered will be visited again.
These vertices can be visited under
two conditions: either they are visited on a previous iteration, or
are visited on the current iteration by some other vertex that is in
the frontier. In any case, there is redundant work involved that can
be quantified.
%As part of this work, an analysis of the amount of
%redundant work was performed.

\begin{comment}
\begin{figure*}[ht!]
    \begin{center}

        \subfigure[Google Graph: 875K Vertices]{%
           \label{fig:graph_google}
           \includegraphics[width=0.5\textwidth]{img/histo_google}
        }%
        \subfigure[Amazon Graph: 224K Vertices]{%
           \label{fig:graph_amazon}
           \includegraphics[width=0.5\textwidth]{img/histo_amazon}
        }\\%  ------- End of the first row ----------------------%

    \end{center}
  \vspace*{-4mm}
    \caption{%
            Redundant work analysis on BFS.
     }%
   \label{fig:redundant}
  \vspace*{-1mm}
\end{figure*}
\end{comment}
%\ref{fig:redundant}

For instance, if we pay attention to the green lines in
Figures~\ref{fig:graphs}, which quantify the number of non-redundant edges,
we can draw the following conclusion.
For social network graphs, the green line is significantly bigger
than the red one (one order of magnitude in the
case of the Google Graph) which indicates that most of the edges
traversed do not lead to new discoveries (i.e. they are redundant).
However, in the case of regular graphs,
these two lines are relatively close to each other.

This analysis helps us to optimize the implementation of the BFS algorithm when
it is used with social network graphs. The idea is to better decide when it is
convenient to switch to the \bu method in order to avoid redundant work.
The \bu method only needs to explore one of the neighbors of a vertex to
incorporate that vertex to the frontier.
Once this occurrence happens, there is no need to keep exploring
other neighbors and so the amount of redundant work performed reduces.

In the next section we explain the approaches we follow to implement a
parallel BFS for heterogenous platforms, taking into account the specific
features of the social network graphs that we have discussed in this section.
Note that the proposed algorithms will also be suitable for highly-connected
graphs since this is actually the feature of
the social network graphs that we are leveraging.
