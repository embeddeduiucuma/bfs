%!TEX root = journal.tex
\section {Introduction}
\label{sec:intro}

Graphs can be used to represent many practical problems, such as
social networks, computer networks, or data organization.
Breadth-First Search (BFS) is the core of many graph analysis
algorithms used in flow network analysis, shortest-path problem, and
in many others graph traversal algorithms.

Parallel implementations of BFS are very challenging due to its
irregular behavior, heavy memory access, and very low arithmetic
intensity. The complexity of BFS
is $O(N + M)$, where $N$ is the number of vertices and $M$ is the
number of edges. However, the execution time depends not only on the
number of vertices and edges, but also on how the data structure is
traversed. Efficient implementations need to consider
the large data structures and the lack of memory locality, since random
memory access prevents locality optimizations.


The use of GPUs coupled to multicores to speed up applications has
become very common, especially on
data parallel algorithms with regular memory accesses
\cite{Hiragushi:2013:EHB:2696303.2696309,Merrill:2010:RSG:1854273.1854344}.
The use of GPUs to compute irregular algorithms, such as BFS, is
tempting because of GPU's high throughput, although an efficient
parallelization is harder.

One of the main reasons why this platforms can help to accelerate
applications is the use of shared memory. In a system where a discrete
GPU is present, the work flow involves transferring data from main
memory to GPU memory, perform the computation in the GPU, and move the
results back to main memory.  This memory movement is done through the
PCI express bus, which is orders of magnitude slower than making
copies within the main memory.  If we consider that the number of
problems that efficiently fit in a discrete GPU is small,
the number of problems that are big enough to justify all this data
movement is even smaller. What it is more, the programmer is involved
in all this process of moving the data around the system. GPU
architects and library developers are creating mechanisms to make all
this process transparent to the programmer, and more efficient in some
cases, but data movement at some point is inevitable.


Heterogeneous platforms, on the other hand, provide an environment of
shared memory between the CPU and the accelerators (in this case, an
OpenCL capable GPU), where the data movement can be avoided, as
the memory space is shared between the CPU and the accelerator.  The
OpenCL specification offers the programmer the necessary APIs to both
create different allocated memory blocks for both devices, as well as
the allocation of memory blocks that can be addressed from both
devices~\cite{ocl}. Section \ref{sec:impl} will cover the specific
details about how we take advantage of this feature in our approaches.

% Shorter version of the above 2 paragraphs:
% In a system where a discrete GPU is present, the work flow involves
% transferring data from main memory to GPU device memory, performing
% the computations on the GPU, and moving the results back to main
% memory. This data movement is done through the PCI express bus, which
% is orders of magnitude slower than making copies within the main
% memory. Heterogenous platforms with an integrated GPU, on the other
% hand, offer an environment where memory is shared between the CPU and
% the accelerator (in this case,  OpenCL capable GPU). Thus, all the
% data movement can be avoided. The OpenCL specification offers the
% programmer the necessary APIs to allocate shared memory blocks that
% can be addressed from both devices~\cite{ocl}.


The goal of this work is to explore how to efficiently implement BFS
on heterogeneous systems, delving into different algorithm versions
that exploit the characteristics of social network graphs, and mapping these versions
into the CPU cores and the GPU to take advantage of the specific features of each device. It also analyzes
different collaborative strategies for distributing the work among the
CPU and the GPU devices. In particular, we make the following
contributions. First, three heterogeneous approaches have been
implemented: \selective, that dynamically selects the best device to
run (CPU or GPU), \concurrent, that uses both devices concurrently and
synchronously, and finally, \async, that uses both devices concurrently
and asynchronously  (see Section~\ref{sec:approaches}).  All the algorithms
were implemented in C++ and OpenCL and optimized using OpenMP.  They are 
portable across all the platforms we used (Intel and ARM  CPUs),
and different software stacks, including MacOS and two different
Linux distributions.  The relevant details of the implementation are
summarized (Section~\ref{sec:impl}).
%relying on C++, OpenMP, and OpenCL
%to produce portable code for the different platforms used.
Our strategies are assessed with 11 social network
graphs and 3 road network graphs on different heterogeneous
platforms (Section~\ref{sec:experimental}). When using Only-CPU
(including state-of-the-art Galois~\cite{galois}) and Only-GPU
implementations as baseline, our heterogeneous approaches obtain
up to 1.56x speedup and 1.32x energy efficiency.  We also
compare our approaches with a recent heterogenous BFS implementation
from Chai benchmark~\cite{Chai} finding that our heterogeneous
strategies can perform up-to 3.6x better. However, memory
congestion is a key bottleneck when both devices run simultaneously,
which represents still an open problem and is left for future work.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% End:
