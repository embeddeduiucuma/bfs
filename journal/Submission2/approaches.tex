%!TEX root = journal.tex
\section{Heterogeneous Approaches}
\label{sec:approaches}
\vspace*{-0.2cm}
Figure~\ref{fig:approaches2} shows the three heterogeneous approaches that we have designed and implemented to perform
BFS. These approaches differ on how the devices collaborate.
In the first approach, referred as \selective, some iterations of the
serial \textbf{while} of Algorithm~\ref{alg:bfs_td} (line~\ref{lst:serialloop}),
i.e. some frontiers, are processed on the CPU while others are processed on the
GPU. The device is selected based on the size of the frontier and on how many
vertices have already been visited. With this approach, both devices are never
exploited simultaneously, as we can see in Figure~\ref{fig:approaches2}a). The
second approach is called \concurrent, where both devices are used at the same
time to process some specific frontiers. In Figure~\ref{fig:approaches2}b),
these frontiers processed concurrently are
those with vertices at distances 2, 3, and 4.
These frontiers are split between the two devices and the graph is
traversed concurrently. In both approaches, we keep the serial loop of
Algorithms 1 and 2, so vertices in frontier $i+1$ are processed only after all the vertices in frontier $i$ have been processed. This requires a
barrier or synchronization after each serial loop iteration, which is a
system-wide barrier in the \concurrent~approach. In order to avoid this
synchronization overhead, we explore a third approach called \async~and
illustrated in Figure~\ref{fig:approaches2}c). In this example, the third
frontier is split in two parts, vertices 5--7 and vertices 8--10, to be computed on
the CPU and the GPU, respectively. However, in this approach, each device processes the sub-graph that can be
reached from the assigned vertices at its own pace (without system-wide
synchronizations).
% Lastly, a third approach, called \async, was developed in order
% to exploit fundamental characteristics associated with scale-free graphs,
% as described in section \ref{sec:repeated}.
%Although we further elaborate on these approaches next,
The three approaches exploit our observations about the social network graph
characteristics. In the initial iterations, only a few vertices are explored,
but after 10 to 15 percent of the graph has been traversed, the number of
vertices in the new frontiers increases exponentially. In this part of the
processing, the GPU will be used to explore the frontier alone (\selective), or
in collaboration with the CPU (\concurrent~and \async).

%\vspace*{-1mm}

\begin{figure}[]
  \begin{center}
    \includegraphics[width=\textwidth]{approaches2}
  \end{center}
  \vspace*{-4mm}
  \caption{Illustrative example of the different heterogeneous approaches.}%
  \label{fig:approaches2}
  \vspace*{-3mm}
\end{figure}

\subsection {Selective}

The \selective~approach is based on the observation that the CPU can
process a given frontier faster than the GPU when the number of
vertices in the frontier is small. However, when the frontier is big
enough, the GPU can extract enough parallelism and explore the
frontier faster than the CPU cores. Therefore, the \selective~
approach starts processing the first frontiers on the CPU, and switch
to the GPU when the number of vertices in the frontier is above a
threshold. With this approach, a frontier is executed on the device
which fits best. A similar methodology has been proposed previously by
Munguia et al.~\cite{6507474}, but from the perspective of a task-based
framework using discrete GPUs and by Daga et al.~\cite{7004254}, using
a heuristic similar to the one proposed by Beamer et
al~\cite{Beamer:2012:DBS:2388996.2389013} to switch from using \td on
CPU to use \bu on GPU. Their methodology requires counting both
vertices and edges in the frontier, which can result in a significant
overhead when processing a frontier on the GPU. Counting edges
requires exploring the frontier and summing up the edges of
each vertex, which is an expensive operation.

To avoid counting the number of edges in the frontier, in our heuristic we
switch from the \td CPU phase to the \bu GPU phase when more than 10\% of the
vertices have been visited, which is cheaper to detect. We have experimentally
evaluated different thresholds, finding that for our graphs,
thresholds between 8--15\% produce the best results without
noticeable differences, being 10\% the one that
slightly outperforms in most cases. This is mainly because,
for the big social network graphs that we have used in our experiments,
after 10\% of the graph is processed, the frontier has more than 40K vertices,
which makes the GPU the suitable device to process such a big frontier faster.
This happens because of the scale-free
nature of the graphs, in which highly connected vertices are very likely
to be found soon, causing a fast increase in the frontier size after just a few iterations.
After this point, our \selective~approach
processes three frontiers on the GPU using \bu method.
To reduce the overhead of GPU kernel launching, three GPU kernels are
dispatched at once to the OpenCL queue.
This is an in-order queue where the three frontiers are executed one
after the other. In our experiments, these three frontiers usually process
around 80\% of the vertices and the size of the remaining frontiers are likely
to be again too small to be processed on the GPU.
If after processing 3 frontiers on the GPU the amount of processed vertices is
smaller than 90\% (10\% in the CPU phase plus 80\% in the GPU one),
additional kernels (frontiers) are enqueued to the GPU one by one until this condition
is met. Finally, when less than 10\% of the vertices remain,
the CPU is again in charge of running the last frontiers using the \bu method.

By invoking the three GPU kernels in a batch, kernel launching overhead is
reduced at the expense of, possibly, performing more iterations than necessary.
However, for all the graph analyzed, at least 3 iterations were needed after
passing the 10\% barrier, meaning that this will rarely be the case. Note that
the frontiers that will be executed on the GPU, usually account for more than
80\% of the total execution time, making the execution time of the
last iterations negligible. Moreover, since the arrays used to store the vertices, edges, distances and frontiers are allocated in global memory, these arrays are accessible both from the CPU and the GPU. Therefore, there is no need to call OpenCL host-to-device routines to make the arrays accessible to the GPU before launching the kernel. Similarly, we can save device-to-host data transfers after the GPU has processed the offloaded work. This OpenCL feature that allows sharing arrays between the CPU and the GPU, usually known as Zero-Copy-Buffer, greatly contributes to reduce the overhead of the heterogeneous execution.

\begin{comment} %this paragraph does not add new insight. This approach can be
implemented using different combinations of method (i.e. \td and \bu methods):
The first iterations can be explored by using \td method, then, when the
frontier is bigger than a threshold, process some iterations in the GPU using
\td or \bu method, and during the final iterations the \bu method can be used to
complete the exploration in CPU. The threshold will depend on several factors,
including the platform (which means the ratio between Traversed Edges Per Second
on CPU and GPU), as well as the method used (\td or \bu). In some cases, the use
of the \td algorithm will be faster than the \bu algorithm in CPU, and sometimes
the opposite will happen in the GPU.
\end{comment}

\subsection {Concurrent}

In this approach, both devices are used at the same time to process the
frontiers that are large enough. The idea is to further reduce the execution
time by simultaneously exploiting the CPU and the GPU. As in the
\selective~approach, when 10\% of the vertices have been processed,
we switch to a CPU-GPU phase, in which the workload of a
parallel iteration is distributed between the GPU and the CPU cores.
Section~\ref{sec:memory} discusses the
experimental results obtained when applying different workload partitions. In
this phase, in principle, both \td and \bu methods can be used.  If we rely on
the \td method, the frontier is created on the CPU from a global bitmask,
indicating which are the vertices belonging to the frontier. Then, a portion of
those vertices are assigned to the GPU by creating a new GPU-specific bitmask
(this incurs a high overhead). On the other hand, the \bu approach does not
involve any new GPU-specific bitmask creation.
The CPU and the GPU can simultaneously read and write in non-overlapping
regions of the shared arrays allocated in
global memory thanks to OpenCL Zero-Copy-Buffer feature.
More precisely, in the \concurrent~\bu approach,
after assigning two non-overlapping regions of the frontier to the CPU and
the GPU, both devices will process their own set of vertices concurrently.
For each vertex, $i$, the CPU or the GPU has to check
if it has not been visited yet ($dist[i]==\infty$) and in that case,
if it has a neighbor, $v$, in the old frontier, $F_{old}$.
As it will be explained in the next section,
this is accomplished by reading the corresponding flag in a globally allocated
bitmask array. If $v \in F_{old}$, the vertex's distance is updated
($dist[i]=dist[v]+1$) and the vertex is added to the new frontier, $F_{new}$,
updating a different global bitmask (the new frontier bitmask). Since the CPU
and the GPU are traversing different vertices, they are reading different
regions of the shared distance vector and of the old frontier shared bitmask,
as well as writing to different non-overlapping
regions of the distance vector and new frontier bitmask.
Therefore this \bu alternative can be heterogeneously
implemented without introducing any overhead, and it is the chosen approach to
process the large frontiers of the graph. As in the \selective~ approach, when
more than 90\% of the vertices are processed, we switch back to a \bu CPU-only
phase.

\subsection {Asynchronous}
\label{sec:async}

The \async~approach attempts to maximize the utilization of both the
CPU and GPU during the graph traversal by avoiding CPU-GPU
synchronization when changing from one frontier to the next, as it
is the case for both the \concurrent~and the
\selective~approaches. The idea is to put both devices to work in
different parts of the graph from an early iteration (frontier), following the \td method. This
means that both devices can work independently, each writing on a
different array of distances at its own pace, until all the vertices in the graph
have been explored. Once this exploration is completed,
it is necessary to merge the distance arrays of both devices. This
reduction operation only requires to compute the per-element
\emph{min} between the two distance arrays, which consumes a
negligible amount of time.

The drawback of this approach lies on the potential repetition of work
performed in both devices. This is illustrated in Figure~\ref{fig:approaches2}c)
where some vertices (13, 17, 18 and 19) can be reached both from the
CPU's first sub-frontier as well as from the GPU's one. In this particular
example, these vertices will be updated with the same distance in both devices,
and the reduction operation will not change the final value. However, in the
general case, the distance of the vertices, and therefore, the number of
frontiers (or iterations) is likely to be different in both devices. For example, if in
Figure~\ref{fig:approaches2}c) there was and edge connecting vertices 7 and 17,
the private distance vector on the CPU would indicate that vertices 11, 12 and 17
are at distance 3, whereas vertices 15, 16, 18 and 19 are at distance 4. From
GPU's perspective, vertex 17 is at distance 4 and vertices 18 and 19 at distance
5. This is, one device can execute more iterations than the other due to
the partial view of the sub-graph that each one is traversing. The implications
of this behavior will harm the performance as redundant work and more
intra-device synchronization is carried out than in the two previously described
synchronous approaches. In section~\ref{sec:repeated} we evaluate whether the
effect of avoiding system-wide synchronization can overcome the redundant work
as well as the increment in the number of iterations.

\begin{comment}%The new example illustrate the situation better.
It is easy to see this potential
hazard by analyzing a simple example. Lets suppose we start traversing a graph.
The first iteration will always contain a single vertex, which is the source
vertex, referred in this work as \emph{s0}. Now, lets assume that the source
vertex has only two edges, meaning that during the first iteration the distance
for those two vertices will be updated (with a value of one) and those two
vertices will be added to the next frontier. Lets then assume that we want to
split this frontier of two vertices, making the CPUs work on one vertex and the
GPU work on the other vertex, letting each device update its own distance
vector, and adding each discovered vertex to its own new frontier. The evident
problem here is that each device may include in its frontier at some iteration a
vertex that have already been discovered in a previous frontier by the other
device. This will not affect the correctness of the final result, as the join
process at the end will fix the values to its minimum, which is the correct
distance for every vertex.
\end{comment}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% End:
