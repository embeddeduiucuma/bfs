%!TEX root = journal.tex
\section{Experimental Evaluation}
\label{sec:experimental}


\subsection {Environmental setup and input graphs}
\label{sec:setup}

The following platforms were used to run the experiments:
\begin{itemize}

    \item ODROID-XU3: Embedded/mobile platform with a
Samsung Exynos5422, 4 Cortex-A15 2Ghz and 4 Cortex-A7 CPU cores, a
Mali-T628 MP6 GPU, and 2GB of LPDDR3 RAM. This is a development platform
for mobile devices, as it has similar characteristics as many modern mobile phones. It runs Linux Ubuntu 14.04 and GCC 4.8.

  \item Intel\textsuperscript{\textregistered} Core\textsuperscript{\texttrademark} i7-4790: 4 cores at 3.60GHz, 16 GB 1666 MHz DDR3,
and an integrated GPU Intel\textsuperscript{\textregistered} HD Graphics 3000. %with 20 OpenCL compute units.
It runs Linux CentOS 7.0 and GCC 4.8.
%Bigger cores at higher frequency compared to the Core i5, but smaller GPU,
%as this processor is aimed to systems with a discrete GPU.

  \item Intel\textsuperscript{\textregistered} Core\textsuperscript{\texttrademark} i5-4250U: 2 cores at 1.3 GHz, 4 GB 1600 MHz DDR3,
and an integrated GPU Intel\textsuperscript{\textregistered} HD Graphics 5000. % with 40 OpenCL compute units.
It runs MacOS X El Capitan and clang-omp 3.5.
%Please correct if specifications on Odroid are not correct.
%
% \item AMD A10-7850K APU Kaveri DT: 4 Steamroller cores at 3.7 GHz and
%   an integrated Radeon(TM) R7 Graphics. It runs Ubuntu 14.04 and OpenCL 1.2.
\end{itemize}

\noindent In the following sections, we will refer to them as Odroid,
Core i7 and Core i5, respectively.  Power is measured using
libraries provided by the vendors
(Intel\textsuperscript{\textregistered} PCM for Core i7 and Core i5)
and an in-house library that reads the power monitors available in the Odroid.

Table~\ref{table:graphs} shows the graphs used in our experiments.
Well-known sources of graphs were used:
SNAP collection~\cite{snapnets},
Graph500 graphs generators~\cite{graph500},
and synthetic generated graphs from Rodinia Benchmark~\cite{5306797}. These sets of inputs
provide a wide range graphs sizes and structures, including social network and roads graphs.


\begin{table*}[t]
\centering
\begin{center}
\scalebox{0.67}{
\begin{tabular}{ c|l|c|c|c}
    \textbf{Abbreviation} & \textbf{Description} & \textbf{\# Vertices x10e6} & \textbf{\# Edges x10e6} & \textbf{Source}\\ [0.5ex]
    \hline
    amazon & Amazon product network & 0.33 & 0.92 & SNAP \\
    \hline
    youtube & Youtube online social network & 1.13 & 2.98 & SNAP \\
    \hline
    google  & Web graph from Google & 0.875 & 5.105 & SNAP \\
    \hline
    liveJournal & LiveJournal online social network & 3.99 & 34.68 & SNAP \\
    \hline
    wiki-talk & Wikipedia talk (communication) network & 2.39 & 5.02 & SNAP \\
    \hline
    email & Email network from a EU research institution & 0.26 & 0.42 & SNAP \\
    \hline
    graph4M & Rodinia Generated & 4.19 & 25.16 & Rodinia \\
    \hline
    graph8M & Rodinia Generated & 8.38 & 50.34 & Rodinia \\
    \hline
    graph16M & Rodinia Generated & 16.77 & 100.62 & Rodinia \\
    \hline
    kron & Graph500 Kroneker & 1.75 & 41.10 & Graph500 \\
    \hline
    rmat & Graph500 Rmat & 2.13 & 81.16 & Graph500 \\
    \hline
    roadCA & Road network of California & 1.96 & 2.76 & SNAP \\
    \hline
    roadTX & Road network of Texas & 1.37 & 1.91 & SNAP \\
    \hline
    roadPA & Road network of Pennsylvania & 1.08 & 1.54 & SNAP \\
    \hline
    \\
\end{tabular}
} % scalebox
\caption{List of Graphs.}
\label{table:graphs}
\end{center}
\end{table*}

\subsection{Limitations of the Asynchronous heterogenous approach}
\label{sec:repeated}

The \async~ approach described in section~\ref{sec:async}
strives for making the most efficient use of both CPU and GPU working
simultaneously in different parts of the graph at their own pace, and
avoiding costly synchronization across devices on each
iteration. The problem of this strategy is that it may incur in extra work.

%Social network graphs have the particular characteristic of being
%traversed in a small number of iterations and the amount of work
%performed in each iteration may differ in orders of magnitude.  This
%is an interesting characteristic that can give room for intelligent
%work division between GPU and CPU. For instance,

\begin{figure}[]
\begin{center}
\includegraphics[width=1\textwidth]{repeated_youtube}
\vspace*{-7mm}
\caption{Repeated work in the \emph{Asynchronous} approach when splitting the
  frontier at each iteration for the Youtube Graph.}
\label{fig:repeated_youtube}
\end{center}
\end{figure}

%A profiling application was implemented in order to
%understand this behavior and
%discuss whether this approach will be worth or not.

For the Youtube graph, Figure~\ref{fig:repeated_youtube}
shows with red bars the
accumulated percentage of vertices processed at each iteration,
and with blue bars,
the percentage of vertices that would be redundantly visited by both the CPU
and GPU if the frontier was split across both devices at the given iteration.
It is not possible to split the frontier in the first iteration, as it only
contains a single vertex, which is the source.
However,  if the frontier for the
2nd iteration is split, almost all the vertices will be visited by both CPU and
GPU. On the other hand, as the graph is traversed, vertices are less likely to
be visited twice, as less vertices will be left to explore.
%The
%extreme case will be when the frontier is split in the last
%iteration. In this case, only those vertices left to visit, which have a
%connection with vertices that are in both the frontiers explored by
%the GPU and CPU will be repeated.
%
If we switch to the heterogeneous \async~execution at iteration 5, we see that the frontier
is split after only traversing less than 25\% of the graph,
and less than 5\% of the vertices will be visited
by both devices. This characteristic can be considered
as an opportunity for performance gain, as
the less work is done before splitting a frontier,
the more work can be done asynchronously in both devices,
making use of all the resources and avoiding synchronizations.
%In the case of the Youtube graph, for instance,
%it is possible to split the frontier among the devices,
%and each device can run independently, at its own pace,
%on a different part of the graph.
%The same can be applied to other social network graphs that we studied.
%
%It is also important to note that the worst case is being analyzed here,
%which is when the frontier is
%split in half. Assuming that,
Notice that the splitting can take into consideration the relative speed of each
device. For instance, if the GPU performs twice as fast as the CPU
cores, 33\% of the frontier can be given to the CPU and 66\%
to the GPU.
%in which case, the number of repeated vertices will
%decrease.
For our experiments, we explored assigning between 60\% and
80\% of the frontier to the GPU.

However, when testing this approach, we were confronted with the drawbacks described in
section~\ref{sec:async}.
%has an important performance penalty.
When the frontier is split among the devices, each device
 starts processing a frontier of smaller size
of what it would have been if it had not been split. This means
that a device does not explore some
of the vertices corresponding to that frontier,
so the vertices connected to these unexplored vertices are
discovered later in some additional iterations.
As a result, with this approach more
iterations are needed to complete the traversal on each device.
A new iteration represents an implicit
point of intra-device synchronization, that harms the performance,
especially the
GPU one. This results in that the overhead introduced by
the additional iterations dominates over the benefit obtained by
avoiding the system-wide synchronization of the devices in each iteration/frontier.
%It is also
%important to note that, even if the number of iteration will be
%larger, the final result will be correct as, after both devices are
%done traversing its portion of the graph, the results will be joined
%using a \emph{min} operation.
In fact, after testing the \async~approach with several
graphs, for which we switch to the CPU-GPU asynchronous execution when
10\% of the vertices have been processed (similarly as in \selective~and \concurrent), we found that up to 60\% of additional iterations
could be added, which in turn translates into execution times that
were 2x higher than the times obtained with the other two heterogenous
approaches. Thus, this approach was dismissed and not further
analyzed.

In the next subsections we will focus our evaluation in the other two
heterogenous approaches that synchronize at each iteration/frontier.


\subsection{Performance and energy efficiency evaluation}
\label{sec:performance}

An experimental evaluation was made over different platforms in order
to understand the impact of the proposed approaches on performance and
energy efficiency. Figures~\ref{fig:performance} and~\ref{fig:power}
show the performance and energy evaluations for Odroid and Core i7
platforms. Performance results are shown using the metric {\textit
  Millions of Traversed Edges Per Second} (MTEPS)~\cite{7004254}. This
metric, as used in the literature, results from dividing the number of
edges in a graph by the execution time, thus it provides a
normalization of performance across graphs with different number of
vertices and edges, and therefore, different running times.  For
energy efficiency, the metric used was performance per watt, or
Millions of Traverse Edges per Joule (MTEPJ).  For collecting
the results, the BFS algorithm was run 500 times from different source
vertices, $s_0$, and the average was calculated.

\begin{figure*}[t!]
    \begin{center}
       \subfigure[Performance evaluation on Odroid]{%
           \label{fig:results_odroid}
           \includegraphics[width=1\textwidth]{odroid_perfo}
        } \\%
        \subfigure[Performance evaluation on Core i7]{%
            \label{fig:results_i7}
            \includegraphics[width=1\textwidth]{i7_perfo}
        }%
    \end{center}
    \caption{%
            Performance evaluation on Odroid and Core i7.
     }%
   \label{fig:performance}
\end{figure*}


In Figures~\ref{fig:performance} and~\ref{fig:power}, we first
evaluate the homogeneous (or one-device) approaches that are the building blocks of
our heterogenous implementations. For it, we show the evaluation when
BFS is configured with the method \td and launched on CPU (\texttt{CPU
  TD}) or on GPU (\texttt{GPU TD}), and when BFS is configured with
the method \bu and launched on CPU (\texttt{CPU BU}). Results for the
Bottom-Up method on GPU are not shown because the first iterations are
too slow, so the executions take a prohibitive amount of time. Also, for the performance
results we take as baseline \texttt{Best Galois}.  For each graph, it
represents the best result obtained among the different
implementations that Galois offers for BFS~\cite{galois}.  Galois is a
well-optimized and state-of-the-art suite of different algorithms for
graph applications, which makes efficient use of all CPU cores present
in the system, so these results represent a reference for the CPU
executions. As a sidenote, we should mention that Galois has their own
data  representation that is optimized for their algorithms, but to be fair
we did not time graph conversion when reporting here the performance results.

Next, in the figures, we present the evaluation of our heterogeneous approaches
named \selective~and \concurrent.  Also, for the performance
evaluation and to establish a baseline, we compare our heterogeneous approaches
against an \texttt{Oracle} execution
which consider both methods (TD, BU) and devices (CPU, GPU). In
particular, for each graph we collected the time that each method
spent on each iteration (or frontier) when running on each
device. Then, the Oracle is calculated by taking, for each iteration,
the minimum execution time for each method and device, and adding up
those minimums for all the iterations. Thus, ideally the Oracle
represents the minimum execution time when the best method and device
is chosen for each iteration, without incurring any
overhead.


Figure~\ref{fig:results_odroid} shows the performance results obtained
after running different graphs on the Odroid platform. As we see, the
\selective~and the \concurrent~heterogeneous approaches work better than the
implementations where only one device is used.
%The figure depicts,
%from left to right, the \bu and \td methods implemented for CPU cores,
%a \td approach for GPU only, and finally, the \selective~and the
%\concurrent~approaches.
An average speedup of 1.41x and 1.40x was obtained for \selective~and
\concurrent~approaches respectively, with speedups of up to 1.52x and
1.56x for the Rodinia graphs. The speedups have been calculated
against the best of the only one device executions (\texttt{CPU TD},
\texttt{CPU BU} or \texttt{GPU TD}) for each graph.
%%% Ask Luis if reference for speedup is CPU TD
%These speedups are due to
%a better use of the resources, since the \selective~approach, uses the
%best device to perform the work, and with the \concurrent~approach,
%there are certain iterations where all the resources are fully
%utilized.
As we see, although the \concurrent~approach uses both devices
simultaneously, it only does so in some of the iterations. Although
these iterations typically represent around 80\% of the execution
time, the improvement with regards \selective~is small on some of the
graphs (the bigger ones), but on average it does not outperform
\selective. In section~\ref{sec:memory} we further explore this issue
pointing out that the congestion on the memory bus, when both devices
are working concurrently, increases the number of stall cycles seen by
both devices. This, in turn, degrades performance. On the other hand,
an average speedup of 1.57x was obtained by \texttt{Oracle} which
indicates that the overhead of the heterogeneous approaches is below
10\%.

One interesting result is that Galois performed poorly when compared
with the heterogenous approaches in all the social network
graphs. However, Galois performed 4x better in road
networks.  Let's remind that our approaches use CSR to represent the
graphs, while Galois uses more general and complex data structures
(sets and graphs) that take advantage of asynchronous graph traversals.
%This is because Galois has data structures that do not favor locality as much
%as CSR, as it is a more general
%framework with the ability to perform multiple graph algorithms.
The use of CSR provides the ability of exploring a frontier
fast because of locality, but there is a need for synchronization
after each iteration that Galois implementations may avoid.
%when using
%the \td and \bu methods.
Because of this, graphs that are highly connected but need a small
number of iterations like social network graphs,
will benefit more from locality and will likely be less affected due
to synchronization barriers when the data structure used is CSR.
In the case of road networks with lowly connected graphs and a high
number of iterations, the amount of work per iteration will be small
and the synchronization overhead will be more significant in
approaches based on CSR and thus these graphs will
benefit from the asynchronous approaches implemented in Galois.

% In the case of the Amazon and the roads networks,
% the oracle is slower than the \selective~and/or \concurrent~approaches, as the
% oracle is calculated considering all the synchronization barriers after each iteration,
% whereas in the case of the heterogeneous approaches, several kernels are
% launched together, reducing kernel
% launching overhead.

In the case of the Core i7 (Figure~\ref{fig:results_i7}), the results
of the heterogeneous approaches are not as good as the ones obtained
for Odroid, but this is an expected outcome as the GPU capabilities are
not comparable to the computing power of the CPU cores. For this
particular platform, an average speedup of 1.07x and 1.08x are achieved
for the \selective~and \concurrent~approaches respectively, with the
best results obtained by the Rodinia graphs with speedups of 1.14x and 1.46x. On
this architecture, \concurrent~performs better than \selective~because
the memory bus exhibits higher memory bandwith than on the Odroid, so it is not as
congested when both devices work concurrently.


Another platform where we performed the same experiments is Core i5.
%and Kaveri (not shown due to space constraints).
%Finally, the middle point between the Odroid platform and the Core i7
%platform can be the Core i5 processor.
On Core i5, both the CPU and GPU capabilities are bigger than in
the Odroid. The GPU is also bigger in this case when compared with the
Core i7 but the Core i5 has smaller CPU cores compared to the 4
high-end cores in the Core i7. Thus, it is a platform which
potentially can benefit more from heterogeneous approaches. In fact,
we find that improvements of the \selective~and
\concurrent~approaches are 1.31x and 1.33x respectively,
on average for all the graphs.
Figure \ref{fig:performance_power} summarizes the different
performance improvements for Core i5, Core i7, and Odroid.
% For the case of Kaveri, which has similar characteristics in terms of
% compute power ratio between CPU and GPU compared to the Core i5,
% the improvement where close to 1.27x for both the
% \selective~and \concurrent~approaches
% (not shown in figures due to space constrains).


\begin{figure*}[]
    \begin{center}

        \subfigure[Performance comparison]{%
            \label{fig:performanceComp}
            \includegraphics[width=0.45\textwidth]{avg_perfo}
        }
        \subfigure[Energy efficiency comparison]{%
           \label{fig:powerComp}
            \includegraphics[width=0.45\textwidth]{avg_power}
        } %

    \end{center}
\vspace*{-1mm}
    \caption{%
            Performance and Energy efficiency normalized for
            comparison between Odroid, Core i7 and Core i5.
     }%
   \label{fig:performance_power}
\vspace*{-1mm}
\end{figure*}

\begin{figure*}[t!]
    \begin{center}
        \subfigure[Energy efficiency  evaluation on Odroid]{%
           \label{fig:power_odroid}
            \includegraphics[width=1\textwidth]{odroid_power}
        } \\%
        \subfigure[Energy efficiency evaluation on Core i7]{%
            \label{fig:power_i7}
            \includegraphics[width=1\textwidth]{i7_power}
        } %
    \end{center}
    \caption{%
            Energy efficiency evaluation on Odroid and Core i7.
     }%
   \label{fig:power}
\end{figure*}


Another important factor to consider on all platforms is the energy
efficiency obtained.  Figure~\ref{fig:power} shows how both the
\selective~and \concurrent~heterogeneous approaches are on average
1.28x and 1.32x more energy efficient on the Odroid, and 1.23x and
1.17x more energy efficient on Core i7 when they are compared to any
other approach that only uses one device. This energy efficiency can
be attributed to two main factors.  First, these approaches traverse
the graph in less time than any other approach, thus draining less
power for less time. Second, GPU at peak performance drains less power
than the CPU cores under similar stress, and these approaches make
more use of the GPU. However, we also notice that the
\concurrent~approach can be less energy efficient that \selective~when
the former can provide more performance (see for instance Youtube
graph in Odroid or rmat graph in Core i7). This reinforce the fact
that in heterogeneous computing not always minimum execution time
results in minimum energy consumption. Figure~\ref{fig:powerComp}
summarizes the energy efficiency improvements for all the platforms
(including Core i5 where we get 1.21x and 1.23x of
efficiency). Interestingly, on the Core i7 the improvement in energy
efficiency is larger than in performance for the heterogeneous
approaches. On the other hand, on the Odroid and Core i5, the energy
efficiency of the \concurrent~approach is slightly better than
\selective. In any case, the differences between the heterogenous
approaches are not big.
%The reason is
%becuase latter will
%be using both devices at the same time during some iterations. But it is not
%expected to find a big difference,
%as even if both are working at the same time,
%the amount of work that each device is performing is less than in the case of
%the \emph{Selective},
%which will put a single device to work to its full capacity.


\subsection{Performance comparison with related heterogeneous
  implementation}\label{sec:PerComChai}

We also conduct a performance evaluation comparison with a recent BFS
heterogeneous implementation that is part of the Chai suite~\cite{Chai}.  This
suite offers several heterogeneous benchmarks that leverage the latest features
of heterogeneous architectures, and whose aim is to realize the potential of
collaborative execution in these platforms. From now on, we use Chai to name the
BFS benchmark included in the suite. We run their OpenCL-D implementation that
is based on a \td method and a \selective~strategy on top of OpenCL 1.2.
Frontiers are processed on the CPU if they have less than 128 vertices and on
the GPU otherwise. To store the graph, Chai uses $2N$ integers for the array of
vertices and $2M$ integers for the array of edges, so in comparison with our
implementation, Chai uses double the space to represent the edges and the
vertices. Vertex distances and visited/non-visited information are stored in two
additional arrays of size $N$. Chai does not leverage the Zero-Copy-Buffer
feature of OpenCL 1.2, so host-to-device and device-to-host data transfers are
implemented for each frontier computed on the GPU. Both the CPU and GPU
implementations of BFS in Chai are based on two queues that store the $F_{old}$
and $F_{new}$ frontiers. These frontiers are shared by all cores when they are
processed on the cores so reading/writing in the queue is done atomically. In
the GPU, using atomics on global memory is too expensive so each GPU compute
units, CU, has its own queue allocated on GPU local memory. As a result, only
small frontiers can fit in GPU local memory (less than 2048 vertices per CU in
our platforms). Therefore, none of the social network graphs could be
successfully computed with Chai.

However, it was possible to run Chai with our roads graphs, an the results
obtained are presented in
Figure~\ref{fig:results_Chai_comp}. We only could carry out this comparison on Core i7, because Odroid only supports OpenCL 1.1. Although our approaches are particularly
tailored for network graphs, as we can se in the figure, also for road graphs,
all our one device implementation as well as the heterogeneous ones outperform
Chai results. Our best heterogeneous approach is on average, 3.17x
faster than Chai. The maximum speedup is
achieved for the graph roadPA where we get 3.6x speedup over Chai.

% We only could carry out this comparison on Core i7 and Kaveri, because Odroid
% only supports OpenCL 1.1.
%We also tried Chai OpenCL-U implementation that uses
% more advanced features of OpenCL 2.0 like platform atomics on CPU-GPU shared
% buffers,
%but we found that either our bigger graphs could not fit in memory or
% they do not execute correctly.

\begin{figure*}[t!]
    \begin{center}
      \includegraphics[width=1\textwidth]{i7_roads_perfo}
      %\subfigure[Road Graphs and Chai on i7]{%
      %     \label{fig:results_i7_comp_}
      %      \includegraphics[width=1\textwidth]{i7_roads_perfo}} \\ %
      % \subfigure[Road Graphs and Chai on Kaveri]{%
      %     \label{fig:results_i7_comp_}
      %      \includegraphics[width=1\textwidth]{kaveri_roads_perfo}} %
    \end{center}

    \caption{Performance comparison with Chai
      implementation on Core i7.% and Kaveri.
     }%
   \label{fig:results_Chai_comp}
\end{figure*}

\begin{comment}
Anyway, Figure~\ref{fig:results_Chai_comp} summarizes the performance
results when the best one device implementation is chosen (the best of
CPU TD, CPU BU and GPU TD), named \texttt{Best Homogeneous}, then our
best heterogeneous implementation (the best of \selective~and
\concurrent), named \texttt{Best Heterogeneous} and then
\texttt{Chai}, for both Core i7 and Kaveri platforms.  The one device
results are used as baseline. On Core i7 we could not run Chai
OpenCL-D version with the bigger graphs (graph16M, mat and
llivejournal) because they could not fit in memory due to OpenCL
memory allocation limitation in this platform. As we see, our
heterogeneous approaches outperform Chai 2x and 1.3x on average, both
on the Core i7 and Kaveri platforms, respectively. Our heterogeneous
approaches are particularly more performant on i7 platform in which we
can achieve up 3.7x of improvement.  Moreover, on this platform the
best of the homogeneous approaches can still outperform Chai 1.25x on
average.  Chai implementation gets better results on Kaveri, mainly due
to the fact that OpenCL atomics are better implemented on this
platform. This, and the fact that their \td implementation keeps a
compacted representation of the frontiers, which avoids some redundant
work, particularly benefits small graphs. However, execution times
take a hit for bigger graphs because Chai \td implementation performs
more redundant work for these cases, and it also causes more
memory divergencies than our \bu approach when switching to the GPU.
\end{comment}

The main reasons for the not so competitive results in Chai are due to three
main facts. First, Chai data structures, particularly the shared queues,
introduce more overhead and synchronization than our simplest arrays and
bitmasks. Second, more time is spent in data movement when offloading
computations to the GPU because Zero-Copy-Buffer is not used in Chai. These two
previous drawbacks explain why even our homogeneous implementations are faster
than Chai. And third, Chai only uses \td method which may result in more
redundant work and more data divergences on the GPU, as we explained in
sections~\ref{sec:algineff} and~\ref{sec:impl}}. In contrast, our approaches are
designed to switch between different algorithmic versions that better suit each
device, when the application progresses. In other words, our results make the
case of exploring hybrid algorithm implementations, in which an application
consists of different algorithm versions that are specialized to better exploit
the architectural features of the corresponding device. Then, during  the
execution, depending on the platform and the input, the application switches
back and forth between different algorithm versions as well as between different
heterogeneous configurations (CPU, GPU, or CPU+GPU).

The Chai suite has a more advanced implementation of BFS that is based on OpenCL
2.0. This version does exploit Zero-Copy-Buffer and OpenCL 2.0 features as
platforms atomics that allow cheaper collaboration and synchronization between
the CPU and the GPU. However, we found that this implementation also has the GPU
local queue size limitation so it won't be able to run social network graphs.
Unfortunately, there is no OpenCL 2.0 drivers for any of our platforms so we
could not experimentally validate the OpenCL 2.0 implementation for the road
graphs, but since the focus of this paper is on network graphs, we leave this
evaluation for future work.

\subsection{Memory Bandwidth limitations}
\label{sec:memory}

Since heterogeneous platforms provide a shared-memory environment,
the data in memory can be used by both devices at the same time without
requiring memory copies (\concurrent~approach).
This is why it is so appealing to explore implementations in which both devices
 can traverse the graph in parallel. But after implementing the
approach aimed to exploit this characteristic and finding no notorious
improvement, a more comprehensive study was performed around this matter,
in particular on the Odroid platform.

\begin{figure}[]
    \begin{center}

        \subfigure[Memory Bound]{%
            \label{fig:memory_bound}
            \includegraphics[width=0.45\textwidth]{16M_memory_bound}
        } %
        \subfigure[Compute Bound]{%
           \label{fig:compute_bound}
            \includegraphics[width=0.45\textwidth]{16M_compute_bound}
        } %

    \end{center}
\vspace*{-1mm}
    \caption{%
            Comparison between memory bound (regular BFS) and compute
            bound (artificial BFS) in Odroid.
     }%
   \label{fig:memory_bandwidth}
\vspace*{-1mm}
\end{figure}


Figure~\ref{fig:memory_bound} shows how the \concurrent~
approach behaves
%reaches a memory bandwidth limitation
when both devices are working at the same time
(\texttt{CPU+GPU}). This experiment consisted on running the iteration
with the heaviest workload on both devices and measuring times when a
different percentage of the workload is offloaded to the GPU while the
CPU is working on the remaining load. The x-axis represents the
different partitions considered. We also measured the time that each
device would take to finish its part of the workload when working
alone in the system (\texttt{CPU Only} and \texttt{GPU Only}
results). The tests were performed using as input the Rodinia 16M
graph.
%The first bar in the figure corresponds to the execution time
%of the CPU working on its part alone, the second bar corresponds to
%the execution time of the GPU working on its part alone, and finally,
%the last bar shows what is the execution time when both devices are
%working at the same.
In the ideal case, the last bar is expected to
be as big as the max of the first two.  In other words, when both the
CPU and the GPU are working, it is expected that all the work will be
finished after the slowest device is done. But this is not the case,
and when both devices work at the same time, the memory system bus cannot
provide the devices with the necessary memory bandwidth.  When the number
of memory requests per time increases because both devices are
working at the same time, the bus gets congested, producing an
important number of stall cycles on both devices and
preventing the devices to work at their peak
performance. We measured more than 40\% of performance degradation
in this case. This behavior is due to the
nature of the BFS processing, which is heavily memory bound.
%Each explored vertex
%only perform reads and writes to memory, and little to zero
%computation.
To compare this benchmark with a similar but compute bound one, we added dummy operations on the BFS
kernels (several square root calculations).
% in order
%to transform the application into compute bound.
Figure~\ref{fig:compute_bound} shows how in the case of this new compute bound
application, a \concurrent~approach would have the potential to achieve
close to ideal performance on the Odroid platform. Similar results
were observed on the Core i7 and Core i5 platforms.



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% End:
