%!TEX root = journal.tex
\section {Input Characterization}
\label{sec:inputcharacterization}

%BFS is an iterative algorithm. After each iteration,
%a new portion of the graph will be discovered
%and distance values will be updated.
%The traversing of the graph will take as many iterations as
%it is necessary in order to reach every node in the graph.
%In a graph where all edges have a distance
%of one, the number of iterations will be equal
%to the diameter of the graph (i.e., the longest shortest
%path between two vertices).


Graphs can have different shapes and characteristics. The number of vertices and
the number of edges are just two of the many dimensions that can characterize a
graph. Two graphs with similar number of vertices and edges but with a different
distribution of the edges can be notoriously different in terms of computing its
BFS. This is because graphs can represent problems of completely different
natures. A typical example for using a graph is to describe the map of a city, where each intersection of 2 or more streets
represents a vertex and each street itself represents an edge. A city has a
regular pattern of streets, which  means that most of the vertices will have roughly
the same amount of edges, and that for each vertex, around 4 edges reaching that
vertex is expected.


On the other hand, social networks, such us Facebook, Google+, or LinkedIn, are also represented by graphs. Here, each vertex
corresponds to a different person, and each edge represents a connection
(friendship or professional relation) between two people. In this case, one can
expect that a particular person is highly connected in the graph, i.e., this
particular person has several connections. It can also be expected that, as a
particular person is not very active in the network, or do not have many
connections, he or she would be poorly connected. It is clear then that in the
latter case we will find a very irregular structure, with some vertices having
several connections while some others having an small number of connections.

% \begin{figure*}[h!]
\begin{figure*}[]
    \begin{center}

        \subfigure[Roads of California: 4M Vertices]{%
            \label{fig:graph_SF}
            \includegraphics[width=0.45\textwidth]{histo_roadCA}
        }
        \subfigure[New York Map Graph: 271K Vertices]{%
           \label{fig:graph_NY}
           \includegraphics[width=0.45\textwidth]{histo_ny}
        }\\ %  ------- End of the first row ----------------------%

    \end{center}
    \caption{%
            Regular Graphs: Maps of New York and California.
     }%
   \label{fig:regular_graphs}
\end{figure*}


\subsection{Regular Graphs vs. Social Network Graphs}

For this work, well-know sources of graphs were used, such as the SNAP
collection~ \cite{snapnets}, the Graph500 benchmark graphs
generators~\cite{graph500}, and synthetic generated graphs from Rodinia
Benchmark~\cite{5306797}. These sets of inputs provide a wide range of different
graphs, including regular and irregular graphs.
%scale-free and non-scale-free graphs.
For these graphs, we decided to profile information such as the number of
iterations (i.e. frontiers) that the \td BFS method takes to traverse the graphs, the number of
edges that each iteration explores (\texttt{Edges}), the number of edges that
visit a vertex for the first time in each iteration (\texttt{Non-redundant
Edges}), and the number of vertices in the frontier ({\texttt{Frontier
Vertices}). Figures~\ref{fig:regular_graphs}
and~\ref{fig:social_network_graphs} collect this information for graphs that
represent roads and social networks, respectively. Non-redundant edges are illustrated in
Figure~\ref{fig:tdbu_comparison} with a dashed green line that connects some of
the vertices in the frontier. For example, if in the \td method we traverse de frontier from
vertex 5 to 10, vertex 11 is reached for the first time from vertex 5 so this
edge is non-redundant, whereas edge from vertex 7 to vertex 11 is a redundant
one because it ends in vertex 11 once again. Note that in this figure,
no-redundant edges are depicted only for the edges that are traversed when
iterating over the vertices of the frontier with $dist=2$. For a different
frontier a different set of non-redundant
edges would have been obtained.


\begin{figure*}[]
    \begin{center}

 %       \subfigure[Orkut Graph: 3M Vertices]{%
 %           \label{fig:graph_orkut}
 %           \includegraphics[width=0.5\textwidth]{histo_orkut}
 %       }%
        \subfigure[Google Graph: 875K Vertices]{%
           \label{fig:graph_google}
           \includegraphics[width=0.5\textwidth]{histo_google}
        }%
        \subfigure[Youtube Graph: 1.17M Vertices]{%
           \label{fig:graph_Youtube}
           \includegraphics[width=0.5\textwidth]{histo_youtube}
        }\\%  ------- End of the first row ----------------------%

    \end{center}
    \caption{%
            Irregular Graphs: Social Networks.
     }%
   \label{fig:social_network_graphs}
\end{figure*}

As we can see from the previous Figures~\ref{fig:regular_graphs}
and~\ref{fig:social_network_graphs}, there is a notorious difference in how
the BFS algorithm will perform when traversing regular graphs in comparison with
social network graphs. In the case of very regular graphs (i.e. a graph
representing a street map, where every vertex has roughly the same number of
edges), a large number of iterations will be needed in order to reach every vertex in the
graph.  On the other hand, social network graphs (also referred as scale-free
graphs in some specific occasions) will only need around 15 or less iterations
before reaching every vertex.  This behavior can be explained by following the
theory of six degree of separation or more formally, an experimental study of
the small-world problem, which demonstrated empirically how two people are
connected in a social graph after connecting, on average, 5.2 other people
~\cite{travers1969experimental}.  Due to this high connectivity, one can expect that after a small number
of iterations, all the vertices in the graph will be reached. It is important to
note that, however, due to incompleteness in real graphs (as not every person in
the world is present in every social network), more iterations than 6 are needed
in order to reach every vertex.


Additionally, Figure~\ref{fig:regular_graphs} shows that regular graphs have a
similar small number of vertices in the frontier (blue line) in each iteration
(between 4-6 million vertices for the majority of the iterations). Thus, several
iterations are necessary to completely traverse the graph (more than 500).
Similarly, the number of edges traversed on each iteration is comparable across
iterations (red line), therefore all the iterations perform a comparable amount
of work. However, in Figure~\ref{fig:social_network_graphs} for social network
graphs, we see that in addition to the smaller number of iterations, there are
some iterations (
%3 and 4 in the Orkut graph
5 to 7 in the Google graph and 2 to 4 in the Youtube graph) where the amount of
edges explored is one order of magnitude higher than in the rest of the
iterations.  The need for fewer iterations when traversing the entire graph
brings new opportunities to platforms that are very sensitive to synchronization
barriers (implicit or explicit), such as GPUs and heterogenous platforms.
Having fewer iterations means fewer barriers, and relatively high amount of work
per iteration. However, there is still both an advantage and a challenge for
heterogenous platforms, as the number of iterations (and thus, synchronization)
is small, but the load is not evenly distributed among the iterations.


%We can imagine a graph of 1 million vertices
%and two scenarios: a very regular graph and a social network graph.
%In the first case, the traversing of the graph will take
%several iterations, but each iteration
%will perform comparable amount of work.
%In the latter case, only a few iterations will be needed,
%but each iteration will entitle considerable more work than in the first case.
%What it is more,
%some iterations will only process a small number of vertices,
%whereas some other iteration will
%perform work over frontiers that may take up to 50\% of the total
%amount of vertices.
%This means a very irregular work flow, as the amount of work
%performed by each iteration will not be comparable.


%Graphs generated by the Rodinia Generator are fully traversed in less than
%15 iterations, even though they have very different number of nodes
%(2M, 4M, and 8M).
%Besides, in only a small part of the iterations most of these
%graphs are traversed (from 7th to 12th iteration).
%On the other hand, both NY and SF graphs
%are traversed in a very different way, taking around 600 and 1000 iterations
%respectively.
%It is important to notice that NY graphs have only 271K vertices,
%and takes several iterations to complete compared to
%the Rodinia Generated 8M graph.
%As mentioned before, more iterations will result in more points
%for synchronization, which will affect
%the performance on implementations that have considerable
%overhead at the end of each iteration.


\subsection{Algorithmic inefficiencies}\label{sec:algineff}

When implementing parallel versions of the \td method for processing a
frontier, some inefficiencies can result from the effort of designing fully
parallel algorithms. Consider the discovery of vertices in the frontier. In the
case of the \td method it is necessary to explore all the neighbors for each
vertex in the frontier. This means that vertices that have already been
discovered will be explored again.  These vertices could have been visited under
two conditions: either they were discovered on a previous iteration, or
were discovered on the current iteration by some other vertex that is in
the frontier. In any case, there is redundant work involved that we can
quantify.
%As part of this work, an analysis of the amount of
%redundant work was performed.

\begin{comment}
\begin{figure*}[ht!]
    \begin{center}

        \subfigure[Google Graph: 875K Vertices]{%
           \label{fig:graph_google}
           \includegraphics[width=0.5\textwidth]{histo_google}
        }%
        \subfigure[Amazon Graph: 224K Vertices]{%
           \label{fig:graph_amazon}
           \includegraphics[width=0.5\textwidth]{histo_amazon}
        }\\%  ------- End of the first row ----------------------%

    \end{center}
    \caption{%
            Redundant work analysis on BFS.
     }%
   \label{fig:redundant}
\end{figure*}
\end{comment}
%\ref{fig:redundant}

For instance, if we pay attention to the green line of
Figures~\ref{fig:social_network_graphs} and~\ref{fig:regular_graphs}, that
quantify the number of non-redundant edges, we can draw the following
conclusion. For social network graphs (Figure~\ref{fig:social_network_graphs})
the green line is quite far from the red one (even one order of magnitude in the
case of the Google Graph) which clearly indicates that most of the edges
traversed do not lead to new discoveries (i.e. they are redundant). However, in
the case of regular graphs, Figure~\ref{fig:regular_graphs},  this number of
edges explored (red) and new discoveries (green) is comparable.

This analysis helps us to optimize the implementation of the BFS algorithm when
it is used with social network graphs. The idea is to better decide when  it is
convenient to switch to the \bu method in order to avoid redundant work. The \bu
method only needs one of the neighbors of a vertex being processed to belong to
the frontier. Once this occurrence happens, there is no need to keep exploring
other neighbors, thus, reducing the amount of redundant work performed.

In the next section we will explain the approaches we follow to implement a
parallel BFS for heterogenous platforms, taking into account the specific
features of the social network graphs that we have discussed in this section.
Note that the proposed algorithms will also be suitable for highly-connected
graphs since this is actually the feature of the social network graphs that we are
leveraging.

%Switching back and forth between \td and \bu to avoid is the strategy  implemented
%in our parallel heterogeneous approaches, following the heuristic proposed
%by Beamer at al~\cite{Beamer:2012:DBS:2388996.2389013}.

% \begin{figure*}[ht!]
\begin{comment}
\begin{figure*}[]
    \begin{center}

        \subfigure[Rodinia Generated: 4M Vertices]{%
            \label{fig:graph_4M}
            \includegraphics[width=0.5\textwidth]{histo_graph4M}
        }%
        \subfigure[Rodinia Generated: 8M Vertices]{%
            \label{fig:graph_8M}
            \includegraphics[width=0.5\textwidth]{histo_graph8M}
        }\\%  ------- End of the row ----------------------%
        \subfigure[Graph500 Rmat: 2.13M Vertices]{%
            \label{fig:graph_rmat}
            \includegraphics[width=0.5\textwidth]{histo_rmat}
        }%
        \subfigure[Graph500 Kronecker: 1.75M Vertices]{%
            \label{fig:graph_kron}
            \includegraphics[width=0.5\textwidth]{histo_kron}
        }\\ %  ------- End of the row ----------------------%
        \subfigure[Email Graph: 265K Vertices]{%
           \label{fig:graph_email}
           \includegraphics[width=0.5\textwidth]{histo_email}
        }%
        \subfigure[Youtube Graph: 1.17M Vertices]{%
           \label{fig:graph_youtube}
           \includegraphics[width=0.5\textwidth]{histo_youtube}
        }\\ %  ------- End of the row ----------------------%

    \end{center}
    \caption{%
            Social network graphs.
     }%
   \label{fig:graph_structure}
\end{figure*}
\end{comment}
