%!TEX root = journal.tex
\section{Implementation Details}
\label{sec:impl}
\vspace*{-0.2cm}
Our CPU BFS implementations (\td and \bu) are written using C++ and OpenMP,
building on top of Rodinia's BFS benchmark~\cite{5306797}, that we optimized as
explained next.
\begin{figure}[ht!]
\begin{center}
\includegraphics[width=0.7\linewidth]{img/csr}
\caption{Example of Graph, its Adjacency Matrix and the CSR representation.}
\label{fig:csrexample}
\end{center}
\end{figure}

The data structure to store the graph is CSR (Compressed Sparse
Representation)~\cite{CSR}. Figure~\ref{fig:csrexample} shows an example graph,
its corresponding Adjacency Matrix, and the CSR representation of this sparse
matrix. Basically, the graph is stored in two vectors: i) the vector $e$ with
$M$ edges, and ii) the vector $v$ that stores $N$ vertices in a $N + 1$ array.
According to the CSR format, the edges connecting $v[i]$ are stored in $v$ in
the indices indicated by $e[v[i]]...e[v[i+1]-1]$. For example, in the graph of
Figure~\ref{fig:csrexample} vertex 0 has three neighbors, 1, 2 and 3. Since the
Adjacency Matrix has a $1$ in the position ($i,j$) if there is an edge between
the vertices $i$ and $j$, we find ones in columns 1, 2 and 3 of row 0. In
the CSR representation, these three edges are stored in $e[v[0]]...e[v[1]-1]$,
i.e. $e[0]...e[2]$. Likewise, the neighbors of vertex 6 are stored in
$e[v[6]]...e[v[7]-1]=e[15]...e[18]$,
this is, vertices 2, 3, 7 and 8. The number
of edges of each vertex $i$ can be easily computed as $v[i+1]-v[i]$, and
therefore, vector $v$ has $N+1$ positions instead of just $N$: $v[N]$ serves
both to point to the last neighbor of the last vertex,
$N-1$, and to indicate the total
number of edges stored in the CSR representation, $M=22$ in our example. Note
that the edge connecting vertices 0 and 1 is stored in row 0 of the Adjacency
Matrix, whereas the edge connecting vertices 1 and 0 is stored in row 1, so
although the Adjacency Matrix is symmetric,
both edges are stored in vector $e$,
trading memory space for execution time
(with this data structure is faster to traverse all the edges of every vertex).

\begin{comment} %Now this is explained in Figure 5
The \emph{vertices} vector will contain the indexes of the first edge in the
\emph{edges} vector for each vertex.
The \emph{edges} vector contains vertex indexes indicating an
edge between a vertex in the \emph{vertices} vector and a referenced vertex.
In other words, if we want to discover the vertices connected to vertex $i$,
we check the position $i$ in the \emph{vertices} vector. The $ith$ element in
\emph{vertices} will contain an index $e$ in the \emph{edges} vector, which is
the first edge that the vertex $i$ has. If we subtract the element the
$i+1$ in \emph{vertices} with the element $i$ in \emph{vertices}, we
will obtain the number of edges for vertex $i$.
With this information (the index $e$ in the \emph{edges} vector and the number
of edges for vertex $i$),
we know which are the vertices connected to vertex $i$
by exploring the elements from
$e$ to $e + number\_of\_edges\_for\_i$ in \emph{edges}.
\end{comment}

It is also common to have a third vector of size $M$, which contains the
weight/distance of each edge in vector $v$. For the purpose of
this work, which involves social network graphs (usually undirected
and with no weighted edges), and since we are interested in the
implications of the use of Heterogeneous platforms rather than a
particular graph application, we always assume undirected graphs where
every edge  represents a distance of 1. Because of this, we ignore the
third vector and only used the first two, described above.

In order to run the algorithms and compute the results,
three additional vectors of length $N$ are also needed: the distance vector
$dist$ and two bitmask vectors $bm_{old}$ and $bm_{new}$. The bitmasks identify
the vertices of $v$ that belong to the \textit{old} and \textit{new} frontiers,
respectively, as described in Algorithm~\ref{alg:bfs_td}. As mentioned in the
previous section, all these arrays are allocated in global memory to make the
graph accessible to both the CPU and GPU.

Originally, the CPU \td method was implemented using OpenMP
\emph{parallel for} in the loop $i$ (see Algorithm 1), going over the whole
bitmask vector, $bm_{old}$, but processing only the vertices $v[i]$ such that
$bm[i]==true$. If the frontier is small, this is suboptimal since $bm[i]$ is
usually false (since only a few $v[i]$ belong to the frontier). To avoid this
overhead, we optimized the implementation following an inspector-executor
approach. First, at the beginning of each iteration of the serial
\textbf{while}, the bitmask $bm_{old}$ is explored to produce a dense frontier $F_{old}$
(inspector step). Afterwards, this dense frontier is processed in a parallel loop
(executor step). Although the inspector step is implemented in parallel relying
on a prefix sum, we found out that for small graphs (with less than 1M
vertices), the inspector step does not pay off. However, for larger graphs,
which are actually the ones requiring parallel processing, the overhead of
the inspector step is later amortized during the executor step. Our experiments show
that for a graph of 16M vertices, the inspector-executor approach is 3.5 times
faster than the original implementation, and thus, this optimization was used in
the CPU \td implementation. With respect to the CPU \bu implementation, a
parallel \textbf{for all} (see line 8 of Algorithm 2), traverses all the
vertices, $v[i]$, but only when $dist[i]==\infty$ the corresponding neighbors,
$e[v[i]]...e[v[i+1]-1]$, are checked. As soon as one of the neighbors is found
in the old frontier, the vertex $v[i]$ is added to the new one by updating
$bm_{new}[i]=true$ and we skip the reset of the neighbors (thanks to the
\texttt{skip} of line 13 in Algorithm 2).

The GPU as well as the \selective, \concurrent~and \async~heterogeneous implementations use
OpenCL 1.2 and are based on Rodinia's BFS GPU benchmark, but we significantly optimized it to implement our heterogeneous approaches. Next, we summarize the main
optimizations:

\begin{itemize}
\item In the case of the OpenCL \td kernel the
inspector-executor approach does not pay off due to three reasons:  i) the cost
of the inspector step to generate the dense frontier; ii) consecutive vertices
in the dense frontier processed by the same GPU warp contain non-coalesced
memory accesses that are slower to process due to memory divergence; and iii)
the amount of parallelism in a dense frontier is reduced with respect to
processing the original frontier. We have experimentally validated that the best
performance is achieved when the GPU process the original (sparse) frontier.
Although with this approach some of the threads of the GPU warp end up doing
nothing, this control divergence is not as expensive as the memory divergence of
the inspector-executor approach.

\item The \bu OpenCL kernel is essentially equivalent
to the CPU \bu implementation.

\item Notice that OpenCL 1.2 version does not allow coherent read/write operations
over the same global memory locations from both GPU and CPU. However, in our
\concurrent~implementation, during the CPU-GPU \bu phase, the CPU and GPU write
in different non-overlapping regions of the output vectors ($dist$ and
$bm_{new}$) so this is not a problem.

\item For the \async~implementation, private
versions of vectors $dist$, $bm_{new}$ and $bm_{old}$ are generated before
switching to CPU-GPU phase. From this point on, each device read/write from/to
its private copy and at its own pace. When both devices have visited all the
reachable vertices in the corresponding sub-graph, the CPU takes care of the
reduction operation computing the final $dist$ vector.
%with the minimum distance read from the private copies.

\end{itemize}

\begin{comment}
\subsection{Shared Memory in OpenCL}
\label{sec:sharedocl}

For the use of shared memory, it is necessary to do some specific OpenCL API
calls. First, the memory must be allocated through the OpenCL API using special
flags, indicating that such memory buffer might be read by the GPU and the CPU.
If this memory buffer is to be read from the CPU, another API call must be made
to indicate the driver that CPU will do some operation over the data, and thus,
the GPU cannot perform operations on that memory block during this time. This is
done by mapping to the CPU virtual space the physical memory space of the
buffer.

%This is well-known
A pseudo code for this procedure is the following:

\begin{lstlisting}
cl_mem d_mem; 	//Pointer for the GPU
char* map_mem: 	//Pointer for the CPU
d_mem = clCreateBuffer(context, special_flags, size, ... );

/* The memory is ready to be used by the GPU */
char* map_mem = clEnqueueMapBuffer(queue,d_mem,flags,size, ...);

/* Memory is used by the CPU */
clEnqueueUnmapMemObject(queue, d_mem, map_em, ... );

/* The memory is ready to be used by the GPU again*/
\end{lstlisting}


In this case, we are avoiding a copy from one memory space to another, which is
necessary in the case of a discrete GPU connected through a PICe bus. The
downside of this capability is the overhead introduced by the mapping/unmapping
operation, and, what it is more, the limitation that only one device at a time
can be performing operation over that memory block.

The overhead of the mapping/unmapping operation was considered to be negligible,
as, supposedly, it would only involved pointer and flags operations. Remember
that all the management within the map and unmap process is handled by the
OpenCL driver, provided by the vendor of the platform.

\end{comment}

% After running some
% initial tests on the Snapdragon platform, and discovering that the execution
% time in both the \emph{Selective} and \emph{Concurrent} approaches was longer
% that the graph traversing executed in CPU only, the overhead of the map/unmap
% operations was measured.

% In the \concurrent
% approach reads and writes are performed over the same memory locations,
% concurrently, as part of the algorithm. Since the \concurrent approach
% only uses the \bu method concurrently, the only possible conflict will
% occur in those vertices that lays in the limit of the division between
% the region assigned to CPU and the one assigned to GPU. By making sure
% that this limit falls in an aligned memory address, no cache coherence
% problem was found.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "paper"
%%% End:
