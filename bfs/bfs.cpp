/* ============================================================
University of Illinois at Urbana Champaign
Breadth-First-Search Optimized
Author: Luis Remis
Date:   Apr 5 2015
============================================================ */
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <cstring>

#include "Concurrent.h"
#include "Selective.h"
#include "readGraph.h"

#define THRESHOLD 30000
#define ITERATIONS 5

int main(int argc, char * argv[])
{
    srand(time(NULL));

    if(argc<3) {
      std::cout << "Usage: <omp_threads>"
                   "<input_file> <optional_workDivision>\n";
      exit(0);
    }

    int num_omp_threads = atoi(argv[1]);
    std::string input_f (argv[2]);

    float work_division = (float)50/100;
    if (argc > 3) {
        work_division = (float)atoi(argv[3])/100;
    }

    struct Graph* graph;
    graph = readGraphFromFile(input_f.c_str());


    std::ofstream outputPower("logPower.log", std::ofstream::out | std::ofstream::app);
    std::ofstream outputMTEPS("logMTEPS.log", std::ofstream::out | std::ofstream::app);
    std::ofstream outputTime("logTime.log", std::ofstream::out | std::ofstream::app);
    // std::ofstream outputCPUPart("logCPUPart.log", std::ofstream::out | std::ofstream::app);

    Selective sel(THRESHOLD, num_omp_threads);
    Concurrent concurrent(num_omp_threads, work_division);

    int iter = ITERATIONS;
    double total, power;


    // outputMTEPS << graph->name << "\t";
    // total = 0;
    // power = 0;
    // for (int i = 1; i <= iter; i = i*2)
    // {
    //  float cpupart = sel.multiple_BFS(graph, i);
    //  total = sel.getLastRun_ms();
    //  outputMTEPS << (float)graph->no_of_edges*(float)i / (1e-3f*(total) )/ (float)1e6 << "\t";
    //  outputCPUPart << cpupart<< "\t";
    // }
    // outputMTEPS << std::endl;
    // outputCPUPart << std::endl;

    // exit(0);

    std::cout << std::setprecision(4);

    outputPower << graph->name << "\t";
    outputMTEPS << graph->name << "\t";
    outputTime << graph->name << "\t";

    total = 0;
    power = 0;
    for (int i = 0; i < iter; ++i)
    {
        sel.run_bfs_cpu_only(graph);
        total += sel.getLastRun_ms();
        power += sel.getLastRunPower_joules() ;
    }
    outputTime  << total / iter << "\t";
    outputMTEPS << (float)graph->no_of_edges / (1e-3f*(total / iter) )/ 1e6 << "\t";
    outputPower << (float)graph->no_of_edges / ((power / iter) )/ 1e6 << "\t";
    // std::cout << (float)graph->no_of_edges / (1e-3f*(total / iter) )/ 1e6 << "\t";

    total = 0;
    power = 0;
    for (int i = 0; i < iter; ++i)
    {
        sel.run_bfs_cpu_only_bottom_up(graph);
        total += sel.getLastRun_ms();
        power += sel.getLastRunPower_joules() ;
    }
    outputTime  << total / iter << "\t";
    outputMTEPS << (float)graph->no_of_edges / (1e-3f*(total / iter) )/ 1e6 << "\t";
    outputPower << (float)graph->no_of_edges / ((power / iter) )/ 1e6 << "\t";
    // std::cout << (float)graph->no_of_edges / (1e-3f*(total / iter) )/ 1e6 << "\t";

    // total = 0;
    // power = 0;
    // for (int i = 0; i < iter; ++i)
    // {
    //  sel.multiple_BFS(graph, iter);
    //  total += sel.getLastRun_ms();
    //  power += sel.getLastRunPower_joules() ;
    // }
    // outputTime   << total / iter << "\t";
    // outputMTEPS << (float)graph->no_of_edges / (1e-3f*(total / iter) )/ 1e6 << "\t";
    // outputPower << (float)graph->no_of_edges / ((power / iter) )/ 1e6 << "\t";
    // std::cout << (float)graph->no_of_edges / (1e-3f*(total / iter) )/ 1e6 << "\t";

    total = 0;
    power = 0;
    for (int i = 0; i < iter; ++i)
    {
        sel.run_bfs_gpu_only(graph);
        total += sel.getLastRun_ms();
        power += sel.getLastRunPower_joules() ;
    }
    outputTime  << total / iter << "\t";
    outputMTEPS << (float)graph->no_of_edges / (1e-3f*(total / iter) )/ 1e6 << "\t";
    outputPower << (float)graph->no_of_edges / ((power / iter) )/ 1e6 << "\t";
    // std::cout << (float)graph->no_of_edges / (1e-3f*(total / iter) )/ 1e6 << "\t";

    total = 0;
    power = 0;
    for (int i = 0; i < iter; ++i)
    {
        sel.run(graph);
        total += sel.getLastRun_ms();
        power += sel.getLastRunPower_joules() ;
    }
    outputTime  << total / iter << "\t";
    outputMTEPS << (float)graph->no_of_edges / (1e-3f*(total / iter) )/ 1e6 << "\t";
    outputPower << (float)graph->no_of_edges / ((power / iter) )/ 1e6 << "\t";
    // std::cout << (float)graph->no_of_edges / (1e-3f*(total / iter) )/ 1e6 << "\t";

    total = 0;
    power = 0;
    for (int i = 0; i < iter; ++i)
    {
        concurrent.run(graph);
        total += concurrent.getLastRun_ms();
        power += concurrent.getLastRunPower_joules() ;
    }
    outputTime  << total / iter << "\t";
    outputMTEPS << (float)graph->no_of_edges / (1e-3f*(total / iter) )/ 1e6 << "\t";
    outputPower << (float)graph->no_of_edges / ((power / iter) )/ 1e6 << "\t";
    // std::cout << (float)graph->no_of_edges / (1e-3f*(total / iter) )/ 1e6 << "\t";

    // std::cout << std::endl;
    outputTime << std::endl;
    outputMTEPS << std::endl;
    outputPower << std::endl;

    return 0;
}

