/* ============================================================
University of Illinois at Urbana Champaign
Breadth-First-Search Optimized
Author: Luis Remis
Date:	Apr 5 2015
============================================================ */
#ifndef _BFS_BASE_H_
#define _BFS_BASE_H_

#include <stdint.h>

#include "Graph.h"
#include "CLHelper.h"

#ifdef ODROID
#include "../energy-meter/energy_meter.h"
#endif
#ifdef __MACH__
#include <IntelPowerGadget/EnergyLib.h>
#include "../energy-meter_intel/energy_meter.h"
#endif

#ifdef POWER_LINUX
#include "cpucounters.h"
#include "utils.h"
#endif

class BFS_base
{

public:
	BFS_base(uint32_t threshold, uint32_t num_omp_threads);
	virtual void run(struct Graph* graph) = 0;
	float getLastRun_ms();
	float getLastRunPower_watts();
	float getLastRunPower_joules();

	/* Method using only cpu and top down approach, with num_omp_thread threads */
	void run_bfs_cpu_only(struct Graph* graph);

	/* Method using only cpu with queues
		This implementation is serial, just used for checking resutls. 
		The performance is terrible, of course. */
	void run_bfs_cpu_only_queue(struct Graph* graph);

	/* Method using only cpu, with num_omp_thread threads
		Based on heuristics, it uses the top down or the bottom up approach. 
		The heuristic is not implemented yet. */
	void run_bfs_cpu_only_bottom_up(struct Graph* graph);

	/* Method using only gpu, using the mask approach. 
		This is: using masks on every iteration indicating the nodes that belond to
		the frontier. The kernel usus a top down approach.
		*/
	void run_bfs_gpu_only(struct Graph* graph);

	float multiple_BFS(struct Graph* graph, uint32_t sources);

protected:

	uint32_t threshold;
	uint32_t num_omp_threads;
	float last_run_ms;
	float last_run_joules;
	int* localFrontier[8]; // Up to eigh Threads
	int* localFrontierSize;

#if defined(ODROID) || defined(__MACH__)
    struct energy_sample *od_sample;    // structure to sample energy in oroid
#endif
#ifdef POWER_LINUX
    PCM* m;
    std::vector<CoreCounterState> csbefore, csafter;
    std::vector<SocketCounterState> sktbefore, sktafter;
    SystemCounterState sbefore, safter;
#endif


	void startPowerMeasure();
	void endPowerMeasure();

	uint32_t createFrontier(char* h_graph_mask, int* h_cost, int* h_frontier, int no_of_nodes);
	void frontier2Mask(int* h_frontier, char* h_graph_mask, int frontier_size);
	uint32_t countNodesInFrontier(char* h_graph_mask, int no_of_nodes);

	bool run_single_bfs_cpu(struct Graph* graph, 
							char* h_graph_mask,
						 	int *h_cost, 
						 	int* h_frontier, 
						 	int frontier_size, 
						 	int level = 0);

	bool run_single_bfs_cpu(struct Graph* graph, 
							char* h_graph_mask,
							char* h_graph_mask_aux,
						 	int *h_cost,
						 	int level = 0);

	// return number of elements in the new frontier
	uint32_t run_single_bfs_cpu_buttom(struct Graph* graph, 
							char* h_graph_mask,
							char* h_graph_mask_aux,
						 	int *h_cost, 
						 	int level = 0);

	void multiple_BFS_gpu_thread(struct Graph* graph, cl_mem d_graph_nodes, 
															cl_mem d_graph_edges, 
															cl_mem d_graph_mask1, 
															cl_mem d_graph_mask2, 
															cl_mem d_cost, 
															cl_mem d_over,
															std::vector<uint32_t>* sources);	
};

#endif
