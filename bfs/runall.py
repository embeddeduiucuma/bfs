import csv
import os
import re
import subprocess
import platform

os.system('export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/')
os.system('export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:/usr/local/Cellar/llvm/4.0.0/lib/')
os.system('make clean')
os.system('rm log*.log')

platformInUse = platform.platform()

print platformInUse

if platformInUse.find('Darwin') >= 0:
    print 'Platform: MacOS'
    graphsDir = '../data/'
    os.system('make mac')
elif platformInUse.find('centos') >= 0 :
    print 'Platform: CentOS'
    graphsDir = '/home/luisremis/researchData/Graphs'
    os.system('make')
elif platformInUse.find('Ubuntu') >= 0:
    print 'Platform Odroid'
    graphsDir = '/home/lremis/researchData/Graphs'
    os.system('make odroid')
else:
    print 'SYSTEM NOT FOUND!!!'
    quit()


for dirname, dirnames, filenames in os.walk(graphsDir):
    # print path to all subdirectories first.

    for filen in filenames:
        filename =  re.sub(r'(.*).graph', r'\1', filen)
        if filen == filename:
            continue
        print(os.path.join(graphsDir, filen))
        # print filename

        p = subprocess.Popen('export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:/usr/local/Cellar/llvm/4.0.0/lib/ && ./bfs 4 ' + os.path.join(graphsDir, filen) ,
            shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

        retval = p.wait()

        for line in p.stdout.readlines():
            print line
