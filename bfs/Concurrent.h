/* ============================================================
University of Illinois at Urbana Champaign
Breadth-First-Search Optimized
Author: Luis Remis
Date:   Apr 5 2015
============================================================ */
#ifndef _Concurrent_H_
#define _Concurrent_H_

#include "BFS_base.h"

class Concurrent : public BFS_base
{

public:
    Concurrent(unsigned int num_omp_threads, float work_division);
    void run(struct Graph* graph);
    
private:

    float work_division;

    void threadCPU_BU(struct Graph* graph, char* h_graph_mask1, char* h_graph_mask2, int* h_cost, int counter, int level, int threads);
    void concurrent(struct Graph* graph);
    void concurrentMore(struct Graph* graph);
};

#endif