/* ============================================================
University of Illinois at Urbana Champaign
Breadth-First-Search Optimized
Author: Luis Remis
Date:   Apr 5 2015
============================================================ */
#include <cstdlib>
#include <iostream>
#include <string>
#include <sstream>
#include <cstring>
#include <iomanip>
#include <thread>
#include <fstream>

#include "Concurrent.h"

#include "util.h"
#include "CLHelper.h"

#ifdef OPENMP
#include <omp.h>
#endif

#ifdef  PROFILING_ALL
#define PROFILING
#endif

#ifdef  PROFILING
#include "Chrono.h"
#endif

#define MAX_THREADS_PER_BLOCK 32

Concurrent::Concurrent(uint32_t num_omp_threads, float work_division) :
    BFS_base(0,num_omp_threads)
{
    this->work_division = work_division;
}

void Concurrent::run(struct Graph* graph)
{
    uint32_t no_of_nodes = graph->no_of_nodes;
    for (int i = 0; i < num_omp_threads; ++i)
    {
        localFrontier[i] = new int[no_of_nodes /2]; //May be dangerous!
    }

    std::string graphname(graph->name);

    std::ofstream fileConcurrent("logConcurrent_" + graphname + ".log" );

    fileConcurrent << graphname << std::endl;
    concurrent(graph);
    fileConcurrent << "Concurrent: " << last_run_ms << std::endl;
    concurrentMore(graph);
    fileConcurrent << "ConcurrentM: " << last_run_ms << std::endl;

    for (int i = 0; i < num_omp_threads; ++i)
    {
        delete[] localFrontier[i];
    }
}

void Concurrent::threadCPU_BU(struct Graph* graph, char* h_graph_mask, char* h_graph_mask_aux,int* h_cost, int counter, int level, int threads)
{

#ifdef OPENMP
    omp_set_num_threads(threads);
    #pragma omp parallel for
#endif
    for(int tid = graph->no_of_nodes-counter; tid < graph->no_of_nodes; tid++ )
    {
        if( h_cost[tid] == NODE_UNVISITED )
        {
            for(int i=graph->nodes[tid]; i < graph->nodes[tid+1]; i++)
            {
                int id = graph->edges[i];

                if( h_graph_mask[id] == true )
                {
                    h_cost[tid]= level;
                    h_graph_mask_aux[tid]=true;
                    break;
                }
            }
        }
    }

#ifdef OPENMP
    omp_set_num_threads(threads);
    #pragma omp parallel for
#endif
    for (int i = graph->no_of_nodes-counter; i < graph->no_of_nodes; ++i)
    {
        h_graph_mask[i] = false;
    }

}


void Concurrent::concurrent(struct Graph* graph)
{
    uint32_t no_of_nodes = graph->no_of_nodes;
    uint32_t no_of_edges = graph->no_of_edges;
    uint32_t *h_graph_nodes = graph->nodes;
    uint32_t *h_graph_edges = graph->edges;
    uint32_t source = graph->source;

    int*  h_frontier  = new int [no_of_nodes];

    char kernel_file[100]  = "Kernels.cl";
    int total_kernels = 3;
    std::string kernel_names[3] = {"BFS_mask", "BFS_mask_buttom", "BFS_mask_buttom_clean"};
    _clInit(kernel_file, total_kernels, kernel_names);


    cl_mem d_graph_nodes = _clMalloc( (no_of_nodes+1) *sizeof(uint32_t), h_graph_nodes);
    cl_mem d_graph_edges = _clMalloc(  no_of_edges*sizeof(int), h_graph_edges);
    cl_mem d_graph_mask1 = _clMallocRWZC(no_of_nodes*sizeof(char), NULL);
    cl_mem d_graph_mask2 = _clMallocRWZC(no_of_nodes*sizeof(char), NULL);
    cl_mem d_cost        = _clMallocRWZC(no_of_nodes*sizeof(int),  NULL);
    _clFinish();


    int* h_cost;
    char* h_graph_mask1;
    char* h_graph_mask2;

    // END GPU INIT

    try{	// GPU warm up

	    int aux_idx = 0;
	    _clSetArgs(0, aux_idx++, d_graph_nodes);
	    _clSetArgs(0, aux_idx++, d_graph_edges);
	    _clSetArgs(0, aux_idx++, d_graph_mask1);
	    _clSetArgs(0, aux_idx++, d_graph_mask2);
	    _clSetArgs(0, aux_idx++, d_cost);
	    _clSetArgs(0, aux_idx++, &no_of_nodes, sizeof(int));
	    _clSetArgs(0, aux_idx++, &no_of_nodes, sizeof(int));
	    _clInvokeKernel(0, no_of_nodes, MAX_THREADS_PER_BLOCK);
	    _clFinish();

	    h_cost = (int*)clEnqueueMapBuffer(getOCLHandler()->queue, d_cost,
	            CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, no_of_nodes*sizeof(int), 0, NULL, NULL, NULL);

	    h_graph_mask1 = (char*)clEnqueueMapBuffer(getOCLHandler()->queue, d_graph_mask1,
	            CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, no_of_nodes*sizeof(char), 0, NULL, NULL, NULL);

	    h_graph_mask2 = (char*)clEnqueueMapBuffer(getOCLHandler()->queue, d_graph_mask2,
	            CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, no_of_nodes*sizeof(char), 0, NULL, NULL, NULL);

    }
    catch (std::string& s){
        std::cout << "Exception: " << s << std::endl;
    }

    for (uint32_t i = 0; i < no_of_nodes; ++i)
    {
        h_graph_mask1[i] = false;
        h_graph_mask2[i] = false;
        h_cost[i]       = NODE_UNVISITED;
    }

    h_cost[source] = 0;
    h_graph_mask1[source]=true;


    int level = 0;
    int nodesProcessed = 0;



#ifdef  PROFILING
    startPowerMeasure();
    ChronoCpu chrono_total("total_timer");
    chrono_total.tic();
#endif

    bool flagMask = true;
    int kernel_id = 0;
    int kernel_idx = 0;
    uint32_t procNodes = 0;
    bool flag_gpu = false;

    while(true)
    {
        level++;

        if (((double)procNodes) / no_of_nodes < 0.1 || flag_gpu)
        {
            int counter = createFrontier(h_graph_mask1, h_cost, h_frontier, no_of_nodes);
            if (counter == 0)
            {
                break;
            }
            run_single_bfs_cpu(graph, h_graph_mask1, h_cost, h_frontier, counter, level);
            procNodes+= counter;
            // std::cout << counter << std::endl;
        }
        else
        {
        	flag_gpu = true;

            clEnqueueUnmapMemObject(getOCLHandler()->queue, d_graph_mask1, h_graph_mask1, 0, NULL, NULL);
            clEnqueueUnmapMemObject(getOCLHandler()->queue, d_graph_mask2, h_graph_mask2, 0, NULL, NULL);
            clEnqueueUnmapMemObject(getOCLHandler()->queue, d_cost, h_cost, 0, NULL, NULL);

        	// Top Down approach
        	kernel_id  = 0;
            kernel_idx = 0;
            _clSetArgs(kernel_id, kernel_idx++, d_graph_nodes);
            _clSetArgs(kernel_id, kernel_idx++, d_graph_edges);
            if (flagMask)
            {
                _clSetArgs(kernel_id, kernel_idx++, d_graph_mask1);
                _clSetArgs(kernel_id, kernel_idx++, d_graph_mask2);
            }
            else
            {
                _clSetArgs(kernel_id, kernel_idx++, d_graph_mask2);
                _clSetArgs(kernel_id, kernel_idx++, d_graph_mask1);
            }
            flagMask = !flagMask;

            _clSetArgs(kernel_id, kernel_idx++, d_cost);
            _clSetArgs(kernel_id, kernel_idx++, &level, sizeof(int));
            _clSetArgs(kernel_id, kernel_idx++, &no_of_nodes, sizeof(int));
            _clInvokeKernel(kernel_id, no_of_nodes, MAX_THREADS_PER_BLOCK);

            _clFinish();

            level++;

            uint32_t cpu_work = (float)no_of_nodes * (1.0f - work_division) ;
            // std::cout << "cpu work: " << cpu_work << std::endl;

            char* mask1_cpu;
            char* mask2_cpu;

            if (flagMask)
            {
                mask1_cpu = h_graph_mask1;
                mask2_cpu = h_graph_mask2;
            }
            else
            {
                mask1_cpu = h_graph_mask2;
                mask2_cpu = h_graph_mask1;
            }

            std::thread cpuThread(&Concurrent::threadCPU_BU, this,
                graph, mask1_cpu, mask2_cpu, h_cost, cpu_work , level, 3);

            kernel_id  = 1;
            kernel_idx = 0;
            _clSetArgs(kernel_id, kernel_idx++, d_graph_nodes);
            _clSetArgs(kernel_id, kernel_idx++, d_graph_edges);
            if (flagMask)
            {
                _clSetArgs(kernel_id, kernel_idx++, d_graph_mask1);
                _clSetArgs(kernel_id, kernel_idx++, d_graph_mask2);
            }
            else
            {
                _clSetArgs(kernel_id, kernel_idx++, d_graph_mask2);
                _clSetArgs(kernel_id, kernel_idx++, d_graph_mask1);
            }
            flagMask = !flagMask;

            int gpu_nodes = no_of_nodes-cpu_work;

            _clSetArgs(kernel_id, kernel_idx++, d_cost);
            _clSetArgs(kernel_id, kernel_idx++, &level, sizeof(int));
            _clSetArgs(kernel_id, kernel_idx++, &gpu_nodes, sizeof(int));

            if (no_of_nodes-cpu_work != 0)
            {
                _clInvokeKernel(kernel_id, no_of_nodes-cpu_work, MAX_THREADS_PER_BLOCK);
                _clFlush();
            }

            _clFinish();
            cpuThread.join();

            level++;

            kernel_id = 2;
            kernel_idx = 0;
           	if (flagMask)
            {
                _clSetArgs(kernel_id, kernel_idx++, d_graph_mask2);
            }
            else
            {
                _clSetArgs(kernel_id, kernel_idx++, d_graph_mask1);
            }
            _clSetArgs(kernel_id, kernel_idx++, &no_of_nodes, sizeof(int));
            _clInvokeKernel(kernel_id, no_of_nodes, MAX_THREADS_PER_BLOCK);

            kernel_id  = 1;
            kernel_idx = 0;
            _clSetArgs(kernel_id, kernel_idx++, d_graph_nodes);
            _clSetArgs(kernel_id, kernel_idx++, d_graph_edges);
            if (flagMask)
            {
                _clSetArgs(kernel_id, kernel_idx++, d_graph_mask1);
                _clSetArgs(kernel_id, kernel_idx++, d_graph_mask2);
            }
            else
            {
                _clSetArgs(kernel_id, kernel_idx++, d_graph_mask2);
                _clSetArgs(kernel_id, kernel_idx++, d_graph_mask1);
            }
            flagMask = !flagMask;

            _clSetArgs(kernel_id, kernel_idx++, d_cost);
            _clSetArgs(kernel_id, kernel_idx++, &level, sizeof(int));
            _clSetArgs(kernel_id, kernel_idx++, &no_of_nodes, sizeof(int));

            _clInvokeKernel(kernel_id, no_of_nodes, MAX_THREADS_PER_BLOCK);

            if (!flagMask)
            {
            	h_graph_mask1 = (char*)clEnqueueMapBuffer(getOCLHandler()->queue, d_graph_mask2,
					CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, no_of_nodes*sizeof(char), 0, NULL, NULL, NULL);
            }
            else{
            	h_graph_mask1 = (char*)clEnqueueMapBuffer(getOCLHandler()->queue, d_graph_mask1,
					CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, no_of_nodes*sizeof(char), 0, NULL, NULL, NULL);
            }

            h_cost = (int*)clEnqueueMapBuffer(getOCLHandler()->queue, d_cost,
                CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, no_of_nodes*sizeof(int), 0, NULL, NULL, NULL);

            _clFinish();
        }

    }



#ifdef  PROFILING
    chrono_total.tac();
    last_run_ms = chrono_total.getElapsedStats().totalTime_ms;
    endPowerMeasure();
#endif

    //--result varification
    if(serial_correctness(graph, h_cost))
        std::cout << "Failed: concurrent" << std::endl;

    //--4 release cl resources.
    _clFree(d_graph_nodes);
    _clFree(d_graph_edges);
    _clFree(d_graph_mask1);
    _clFree(d_graph_mask2);
    _clFree(d_cost);
    _clRelease();

    delete[] h_frontier;

    return;
}

void Concurrent::concurrentMore(struct Graph* graph)
{
    uint32_t no_of_nodes = graph->no_of_nodes;
    uint32_t no_of_edges = graph->no_of_edges;
    uint32_t *h_graph_nodes = graph->nodes;
    uint32_t *h_graph_edges = graph->edges;
    uint32_t source = graph->source;

    int*  h_frontier  = new int [no_of_nodes];

    char kernel_file[100]  = "Kernels.cl";
    int total_kernels = 3;
    std::string kernel_names[3] = {"BFS_mask", "BFS_mask_buttom", "BFS_mask_buttom_clean"};
    _clInit(kernel_file, total_kernels, kernel_names);


    cl_mem d_graph_nodes = _clMalloc( (no_of_nodes+1) *sizeof(uint32_t), h_graph_nodes);
    cl_mem d_graph_edges = _clMalloc(  no_of_edges*sizeof(int), h_graph_edges);
    cl_mem d_graph_mask1 = _clMallocRWZC(no_of_nodes*sizeof(char), NULL);
    cl_mem d_graph_mask2 = _clMallocRWZC(no_of_nodes*sizeof(char), NULL);
    cl_mem d_cost        = _clMallocRWZC(no_of_nodes*sizeof(int),  NULL);
    _clFinish();


    int* h_cost;
    char* h_graph_mask1;
    char* h_graph_mask2;

    // END GPU INIT

    try{    // GPU warm up

        int aux_idx = 0;
        _clSetArgs(0, aux_idx++, d_graph_nodes);
        _clSetArgs(0, aux_idx++, d_graph_edges);
        _clSetArgs(0, aux_idx++, d_graph_mask1);
        _clSetArgs(0, aux_idx++, d_graph_mask2);
        _clSetArgs(0, aux_idx++, d_cost);
        _clSetArgs(0, aux_idx++, &no_of_nodes, sizeof(int));
        _clSetArgs(0, aux_idx++, &no_of_nodes, sizeof(int));
        _clInvokeKernel(0, no_of_nodes, MAX_THREADS_PER_BLOCK);
        _clFinish();

        h_cost = (int*)clEnqueueMapBuffer(getOCLHandler()->queue, d_cost,
                CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, no_of_nodes*sizeof(int), 0, NULL, NULL, NULL);

        h_graph_mask1 = (char*)clEnqueueMapBuffer(getOCLHandler()->queue, d_graph_mask1,
                CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, no_of_nodes*sizeof(char), 0, NULL, NULL, NULL);

        h_graph_mask2 = (char*)clEnqueueMapBuffer(getOCLHandler()->queue, d_graph_mask2,
                CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, no_of_nodes*sizeof(char), 0, NULL, NULL, NULL);

    }
    catch (std::string& s){
        std::cout << "Exception: " << s << std::endl;
    }

    for (uint32_t i = 0; i < no_of_nodes; ++i)
    {
        h_graph_mask1[i] = false;
        h_graph_mask2[i] = false;
        h_cost[i]       = NODE_UNVISITED;
    }

    h_cost[source] = 0;
    h_graph_mask1[source]=true;


    int level = 0;
    int nodesProcessed = 0;



#ifdef  PROFILING
    startPowerMeasure();
    ChronoCpu chrono_total("total_timer");
    chrono_total.tic();
#endif

    bool flagMask = true;
    int kernel_id = 0;
    int kernel_idx = 0;
    uint32_t procNodes = 0;
    bool flag_gpu = false;

    while(true)
    {
        level++;

        if (((double)procNodes) / no_of_nodes < 0.10 || flag_gpu)
        {
            int counter = createFrontier(h_graph_mask1, h_cost, h_frontier, no_of_nodes);
            if (counter == 0)
            {
                break;
            }
            run_single_bfs_cpu(graph, h_graph_mask1, h_cost, h_frontier, counter, level);
            procNodes+= counter;
            // std::cout << counter << std::endl;
        }
        else
        {
            flag_gpu = true;

            clEnqueueUnmapMemObject(getOCLHandler()->queue, d_graph_mask1, h_graph_mask1, 0, NULL, NULL);
            clEnqueueUnmapMemObject(getOCLHandler()->queue, d_graph_mask2, h_graph_mask2, 0, NULL, NULL);
            clEnqueueUnmapMemObject(getOCLHandler()->queue, d_cost, h_cost, 0, NULL, NULL);

            // Top Down approach
            kernel_id  = 1;
            kernel_idx = 0;
            _clSetArgs(kernel_id, kernel_idx++, d_graph_nodes);
            _clSetArgs(kernel_id, kernel_idx++, d_graph_edges);
            if (flagMask)
            {
                _clSetArgs(kernel_id, kernel_idx++, d_graph_mask1);
                _clSetArgs(kernel_id, kernel_idx++, d_graph_mask2);
            }
            else
            {
                _clSetArgs(kernel_id, kernel_idx++, d_graph_mask2);
                _clSetArgs(kernel_id, kernel_idx++, d_graph_mask1);
            }
            flagMask = !flagMask;

            _clSetArgs(kernel_id, kernel_idx++, d_cost);
            _clSetArgs(kernel_id, kernel_idx++, &level, sizeof(int));
            _clSetArgs(kernel_id, kernel_idx++, &no_of_nodes, sizeof(int));
            _clInvokeKernel(kernel_id, no_of_nodes, MAX_THREADS_PER_BLOCK);

            kernel_id = 2;
            kernel_idx = 0;
            if (flagMask)
            {
                _clSetArgs(kernel_id, kernel_idx++, d_graph_mask2);
            }
            else
            {
                _clSetArgs(kernel_id, kernel_idx++, d_graph_mask1);
            }
            _clSetArgs(kernel_id, kernel_idx++, &no_of_nodes, sizeof(int));
            _clInvokeKernel(kernel_id, no_of_nodes, MAX_THREADS_PER_BLOCK);

            _clFinish();

            uint32_t cpu_work = (float)no_of_nodes * (1.0f - work_division) ;
            // std::cout << "cpu work: " << cpu_work << std::endl;

            int steps = 0;

            while (steps < 2) // Change the number of steps as parameter. 2 is best.
            {
                char* mask1_cpu;
                char* mask2_cpu;

                level++;
                steps++;

                if (flagMask)
                {
                    mask1_cpu = h_graph_mask1;
                    mask2_cpu = h_graph_mask2;
                }
                else
                {
                    mask1_cpu = h_graph_mask2;
                    mask2_cpu = h_graph_mask1;
                }

                std::thread cpuThread(&Concurrent::threadCPU_BU, this,
                    graph, mask1_cpu, mask2_cpu, h_cost, cpu_work , level, 4);

                kernel_id  = 1;
                kernel_idx = 0;
                _clSetArgs(kernel_id, kernel_idx++, d_graph_nodes);
                _clSetArgs(kernel_id, kernel_idx++, d_graph_edges);
                if (flagMask)
                {
                    _clSetArgs(kernel_id, kernel_idx++, d_graph_mask1);
                    _clSetArgs(kernel_id, kernel_idx++, d_graph_mask2);
                }
                else
                {
                    _clSetArgs(kernel_id, kernel_idx++, d_graph_mask2);
                    _clSetArgs(kernel_id, kernel_idx++, d_graph_mask1);
                }
                flagMask = !flagMask;

                int gpu_nodes = no_of_nodes-cpu_work;

                _clSetArgs(kernel_id, kernel_idx++, d_cost);
                _clSetArgs(kernel_id, kernel_idx++, &level, sizeof(int));
                _clSetArgs(kernel_id, kernel_idx++, &gpu_nodes, sizeof(int));

                if (no_of_nodes-cpu_work != 0)
                {
                    _clInvokeKernel(kernel_id, no_of_nodes-cpu_work, MAX_THREADS_PER_BLOCK);
                }


                _clFinish();
                cpuThread.join();

                kernel_id = 2;
                kernel_idx = 0;
                if (flagMask)
                {
                    _clSetArgs(kernel_id, kernel_idx++, d_graph_mask2);
                }
                else
                {
                    _clSetArgs(kernel_id, kernel_idx++, d_graph_mask1);
                }
                _clSetArgs(kernel_id, kernel_idx++, &no_of_nodes, sizeof(int));
                _clInvokeKernel(kernel_id, no_of_nodes, MAX_THREADS_PER_BLOCK);
                _clFinish();

                // ++level;

            }

            if (!flagMask)
            {
                h_graph_mask1 = (char*)clEnqueueMapBuffer(getOCLHandler()->queue, d_graph_mask2,
                    CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, no_of_nodes*sizeof(char), 0, NULL, NULL, NULL);
            }
            else{
                h_graph_mask1 = (char*)clEnqueueMapBuffer(getOCLHandler()->queue, d_graph_mask1,
                    CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, no_of_nodes*sizeof(char), 0, NULL, NULL, NULL);
            }

            h_cost = (int*)clEnqueueMapBuffer(getOCLHandler()->queue, d_cost,
                CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, no_of_nodes*sizeof(int), 0, NULL, NULL, NULL);

            _clFinish();
        }

    }



#ifdef  PROFILING
    chrono_total.tac();
    last_run_ms = chrono_total.getElapsedStats().totalTime_ms;
    endPowerMeasure();
#endif

    //--result varification
    // serial_correctness(graph, h_cost);

    //--4 release cl resources.
    _clFree(d_graph_nodes);
    _clFree(d_graph_edges);
    _clFree(d_graph_mask1);
    _clFree(d_graph_mask2);
    _clFree(d_cost);
    _clRelease();

    delete[] h_frontier;

    return;
}