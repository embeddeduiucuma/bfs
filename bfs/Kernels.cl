/* ============================================================
University of Illinois at Urbana Champaign
Breadth-First-Search Optimized
Author: Luis Remis
Date:	Apr 5 2015
============================================================ */
#pragma OPENCL EXTENSION cl_khr_byte_addressable_store: enable
//Structure to hold a node information

#define NODE_UNVISITED   (20000)

__kernel void BFS_frontier( const __global int* g_nodes,
					const __global int*  g_edges, 
					__global char* d_mask, 
					__global int*  d_cost, 
					__global int*  d_frontier, 
					const int total_frontier)
{
	const int gid = get_global_id(0);

	if ( gid >= total_frontier)
		return;

	const int node_idx = d_frontier[gid];

	for(int i=g_nodes[node_idx]; i<g_nodes[node_idx+1]; i++)
	{
		int id = g_edges[i];
		
		if( d_cost[id] == NODE_UNVISITED )
		{
			d_cost[id]=d_cost[node_idx]+1;
			d_mask[id]=true;
		}
	}
}

__kernel void BFS_mask( 
					const __global int* g_nodes,
					const __global int*  g_edges, 
					__global char* d_mask,
					__global char* d_mask_next, 
					__global int* d_cost, 
					const  int level,
					const  int no_of_nodes)
{
	int gid = get_global_id(0);

	if( gid<no_of_nodes && d_mask[gid])
	{
		d_mask[gid]=false;

		for(int i=g_nodes[gid]; i<g_nodes[gid+1]; i++)
		{
			int id = g_edges[i];

			if(d_cost[id] == NODE_UNVISITED)
			{
				d_cost[id]=level;
				d_mask_next[id]=true;
			}
		}
	}	
}

__kernel void BFS_mask_buttom( 
					const __global int* g_nodes,
					const __global int*  g_edges, 
					__global char* d_mask,
					__global char* d_mask_next, 
					__global int*  d_cost, 
					const  int level,
					const  int no_of_nodes)
{
	int gid = get_global_id(0);

	if( gid<no_of_nodes && d_cost[gid] == NODE_UNVISITED )
	{
		for(int i=g_nodes[gid]; i<g_nodes[gid+1]; i++)
		{
			int id = g_edges[i];

			if(d_mask[id] == true)
			{
				d_cost[gid]=level;
				d_mask_next[gid]=true;
				break;
			}
		}
	}
}

/* This kernel is not necessary since we will never try to reach a same 
	node, once it has been discovered in the frontier, it make no sense that it 
	appears again. This is only valid for the bottom up approach. */
__kernel void BFS_mask_buttom_clean(
					__global char* d_mask,
					const  int no_of_nodes)
{
	int gid = get_global_id(0);

	if( gid<no_of_nodes )
	{
		d_mask[gid] = false;
	}
}

__kernel void BFS_cost_clean(
					__global int* d_cost,
					const  int no_of_nodes)
{
	int gid = get_global_id(0);

	if( gid<no_of_nodes )
	{
		d_cost[gid] = NODE_UNVISITED;
	}
}

__kernel void BFS_frontier_with_offset(
					const __global int* g_nodes,
					const __global int*  g_edges, 
					__global char* d_mask, 
					__global int* d_cost, 
					__global int* d_frontier, 
					const int total_frontier, 
					const int level,
					const int offset)
{
	if ( get_global_id(0) >= total_frontier)
		return;

	const int node_idx = d_frontier[get_global_id(0)+offset];

	for(int i=g_nodes[node_idx]; i<g_nodes[node_idx+1]; i++)
	{
		int id = g_edges[i];
		
		if( d_cost[id] == NODE_UNVISITED )
		{
			d_cost[id]=level;
			d_mask[id]=true;
		}
	}
}

__kernel void BFS_mask_with_over_flag( 
					const __global int* g_nodes,
					const __global int*  g_edges, 
					__global char* d_mask,
					__global char* d_mask_next, 
					__global int*  d_cost, 
					const  int level,
					const  int no_of_nodes, 
					__global char* over)
{
	int gid = get_global_id(0);

	if( gid<no_of_nodes && d_mask[gid])
	{
		d_mask[gid]=false;

		for(int i=g_nodes[gid]; i<g_nodes[gid+1]; i++)
		{
			int id = g_edges[i];

			if(d_cost[id] == NODE_UNVISITED)
			{
				d_cost[id]=level;
				d_mask_next[id]=true;
				*over = false;
			}
		}
	}	
}

__kernel void simple_write_odd( __global char* write )
{
	unsigned int gid = get_global_id(0) * 2 + 1;
	// if (gid%2 == 1) write[gid] = 2;
	write[gid] = 2;
}

__kernel void simple_write_all( __global char* write )
{
	unsigned int gid = get_global_id(0);
	write[gid] = 2;
}

