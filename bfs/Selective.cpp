/* ============================================================
University of Illinois at Urbana Champaign
Breadth-First-Search Optimized
Author: Luis Remis
Date:	Apr 5 2015
============================================================ */
#include <cstdlib>
#include <iostream>
#include <string>
#include <cstring>
#include <fstream>

#include "Selective.h"

#include "util.h"
#include "CLHelper.h"

#ifdef OPENMP
#include <omp.h>
#endif

#ifdef  PROFILING_ALL
#define PROFILING
#endif

#ifdef  PROFILING
#include "Chrono.h"
#endif

#define MAX_THREADS_PER_BLOCK 32

Selective::Selective(uint32_t threshold, uint32_t num_omp_threads) :
	BFS_base(threshold, num_omp_threads)
{
}

void Selective::run(struct Graph* graph)
{
	uint32_t no_of_nodes = graph->no_of_nodes;
	for (int i = 0; i < num_omp_threads; ++i)
	{
		localFrontier[i] = new int[no_of_nodes/2] ;
	}

	std::string graphname(graph->name);

	std::ofstream fileSelective("logSelective_" + graphname + ".log");

	fileSelective << graphname << std::endl;
	selective(graph);
	fileSelective << "Selective: " << last_run_ms << std::endl;
	selectiveBU(graph);
	fileSelective << "SelectiveBU: " << last_run_ms << std::endl;
	fileSelective.close();

	for (int i = 0; i < num_omp_threads; ++i)
	{
		delete[] localFrontier[i] ;
	}
}

void Selective::selective(struct Graph* graph)
{
	uint32_t no_of_nodes = graph->no_of_nodes;
    uint32_t no_of_edges = graph->no_of_edges;
    uint32_t *h_graph_nodes = graph->nodes;
    uint32_t *h_graph_edges = graph->edges;
    uint32_t source = graph->source;

    char* h_graph_mask;
    int*  h_cost;
    int*  h_frontier 	= new int [no_of_nodes];

	// GPU INICIALIZATION

	char kernel_file[100]  = "Kernels.cl";
    int total_kernels = 3;
    std::string kernel_names[3] = {"BFS_mask", "BFS_mask_buttom", "BFS_mask_buttom_clean"};
	_clInit(kernel_file, total_kernels, kernel_names);

	cl_mem d_graph_nodes = _clMalloc( (no_of_nodes+1)*sizeof(uint32_t), h_graph_nodes);
	cl_mem d_graph_edges = _clMalloc(no_of_edges*sizeof(uint32_t), h_graph_edges);
	cl_mem d_graph_mask1 = _clMallocRWZC(no_of_nodes*sizeof(char), NULL);
	cl_mem d_graph_mask2 = _clMallocRWZC(no_of_nodes*sizeof(char), NULL);
	cl_mem d_cost 		 = _clMallocRWZC(no_of_nodes*sizeof(int),  h_cost);

	// END GPU INIT

	// GPU WARM UP
	try {
		int idx_aux = 0;
		int level_aux = 1;
        _clSetArgs(0, idx_aux++, d_graph_nodes);
        _clSetArgs(0, idx_aux++, d_graph_edges);
		_clSetArgs(0, idx_aux++, d_graph_mask1);
        _clSetArgs(0, idx_aux++, d_graph_mask2);
        _clSetArgs(0, idx_aux++, d_cost);
        _clSetArgs(0, idx_aux++, &level_aux, sizeof(int));
        _clSetArgs(0, idx_aux++, &no_of_nodes, sizeof(int));
        _clInvokeKernel(0, (no_of_nodes), MAX_THREADS_PER_BLOCK);

        _clFinish();

	    h_cost = (int*)clEnqueueMapBuffer(getOCLHandler()->queue, d_cost,
				CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, no_of_nodes*sizeof(int), 0, NULL, NULL, NULL);

		h_graph_mask = (char*)clEnqueueMapBuffer(getOCLHandler()->queue, d_graph_mask2,
				CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, no_of_nodes*sizeof(char), 0, NULL, NULL, NULL);

		_clFinish();

		for (uint32_t i = 0; i < no_of_nodes; ++i)
	    {
	        h_graph_mask[i] = false;
	    }

	    clEnqueueUnmapMemObject(getOCLHandler()->queue, d_graph_mask2, h_graph_mask, 0, NULL, NULL);

		h_graph_mask = (char*)clEnqueueMapBuffer(getOCLHandler()->queue, d_graph_mask1,
				CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, no_of_nodes*sizeof(char), 0, NULL, NULL, NULL);

		_clFinish();

 	}
    catch (std::string& s){
        std::cout << "Exception: " << s << std::endl;
    }


	for (uint32_t i = 0; i < no_of_nodes; ++i)
    {
        h_graph_mask[i] = false;
        h_cost[i]       = NODE_UNVISITED;
    }

	h_graph_mask[source]=true;
    h_cost[source] = 0;

#ifdef	PROFILING
	startPowerMeasure();
	ChronoCpu chrono_total("total_timer");
	chrono_total.tic();
#endif

	uint32_t procNodes = 0;
	uint32_t level = 0;
	bool flag_gpu = false;
	bool flagMask = true;

	while(true)
	{
		if ( ((double)procNodes) / no_of_nodes < 0.1 || flag_gpu ) // 10% of the graph or more, big enough for scale free.
		{
			++level;
			int counter = createFrontier(h_graph_mask, h_cost, h_frontier, no_of_nodes);
			bool finish = run_single_bfs_cpu(graph, h_graph_mask, h_cost, h_frontier, counter, level);

			procNodes += counter;
			// std::cout << counter << std::endl;
			if (finish)
			{
				break;
			}
		}
		else
		{
			flag_gpu = true;

			clEnqueueUnmapMemObject(getOCLHandler()->queue, d_cost, h_cost, 0, NULL, NULL);
			clEnqueueUnmapMemObject(getOCLHandler()->queue, d_graph_mask1, h_graph_mask, 0, NULL, NULL);

			int steps = 0;
            //bool h_over = false;
            bool flagMask = true;
            while (steps < 1)			//4 rounds in the GPU
            {
            	steps++;
            	level++;

                int kernel_id  = 0;
                int kernel_idx = 0;
                _clSetArgs(kernel_id, kernel_idx++, d_graph_nodes);
                _clSetArgs(kernel_id, kernel_idx++, d_graph_edges);

                if (flagMask)
                {
                    _clSetArgs(kernel_id, kernel_idx++, d_graph_mask1);
                    _clSetArgs(kernel_id, kernel_idx++, d_graph_mask2);
                }
                else
                {
                    _clSetArgs(kernel_id, kernel_idx++, d_graph_mask2);
                    _clSetArgs(kernel_id, kernel_idx++, d_graph_mask1);
                }
                flagMask = !flagMask;

                _clSetArgs(kernel_id, kernel_idx++, d_cost);
                _clSetArgs(kernel_id, kernel_idx++, &level, sizeof(int));
                _clSetArgs(kernel_id, kernel_idx++, &no_of_nodes, sizeof(int));
                _clInvokeKernel(kernel_id, (no_of_nodes), MAX_THREADS_PER_BLOCK);
            }

            steps = 0;

            while( steps < 3)
            {
            	level++;
            	steps++;

	            int kernel_id = 1;
	            int kernel_idx = 0;
	            _clSetArgs(kernel_id, kernel_idx++, d_graph_nodes);
	            _clSetArgs(kernel_id, kernel_idx++, d_graph_edges);

	            if (flagMask)
                {
                    _clSetArgs(kernel_id, kernel_idx++, d_graph_mask1);
                    _clSetArgs(kernel_id, kernel_idx++, d_graph_mask2);
                }
                else
                {
                    _clSetArgs(kernel_id, kernel_idx++, d_graph_mask2);
                    _clSetArgs(kernel_id, kernel_idx++, d_graph_mask1);
                }

                flagMask = !flagMask;

	            _clSetArgs(kernel_id, kernel_idx++, d_cost);
	            _clSetArgs(kernel_id, kernel_idx++, &level, sizeof(int) );
	            _clSetArgs(kernel_id, kernel_idx++, &no_of_nodes, sizeof(int));

	            _clInvokeKernel(kernel_id, no_of_nodes, MAX_THREADS_PER_BLOCK);

	            if (steps == 1)
	            {
	            	kernel_id = 2;
		            kernel_idx = 0;
		           	if (!flagMask)
	                {
	                    _clSetArgs(kernel_id, kernel_idx++, d_graph_mask1);
	                }
	                else
	                {
	                    _clSetArgs(kernel_id, kernel_idx++, d_graph_mask2);
	                }
		            _clSetArgs(kernel_id, kernel_idx++, &no_of_nodes, sizeof(int));
		            _clInvokeKernel(kernel_id, no_of_nodes, MAX_THREADS_PER_BLOCK);
	            }


            }

			h_cost = (int*)clEnqueueMapBuffer(getOCLHandler()->queue, d_cost,
				CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, no_of_nodes*sizeof(int), 0, NULL, NULL, NULL);

            if (!flagMask)
            {
            	h_graph_mask = (char*)clEnqueueMapBuffer(getOCLHandler()->queue, d_graph_mask2,
					CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, no_of_nodes*sizeof(char), 0, NULL, NULL, NULL);
            }
            else{
            	h_graph_mask = (char*)clEnqueueMapBuffer(getOCLHandler()->queue, d_graph_mask1,
					CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, no_of_nodes*sizeof(char), 0, NULL, NULL, NULL);
            }

            _clFinish();
		}
	}

#ifdef	PROFILING
	chrono_total.tac();
	last_run_ms = chrono_total.getElapsedStats().totalTime_ms;
	endPowerMeasure();
#endif

	//--result varification
	if(serial_correctness(graph, h_cost))
        std::cout << "Failed: selective" << std::endl;

	//--4 release cl resources.
	_clFree(d_graph_nodes);
	_clFree(d_graph_edges);
	_clFree(d_graph_mask1);
	_clFree(d_graph_mask2);
	_clFree(d_cost);
	_clRelease();

    delete[] h_frontier;

	return ;
}

uint32_t Selective::counteUnvisitedNodes(struct Graph* graph, int* cost)
{
	int counter[num_omp_threads];
	for (int i = 0; i < num_omp_threads; ++i)
	{
		counter[i] = 0;
	}

#ifdef OPENMP
    omp_set_num_threads(num_omp_threads);
    #pragma omp parallel for
#endif
    for (int i = 0; i < graph->no_of_nodes; ++i)
    {
        if (cost[i] == NODE_UNVISITED)
        {
        	counter[int(omp_get_thread_num())]++;
        };
    }

    for (int i = 1; i < num_omp_threads; ++i)
    {
    	counter[0] = counter[0] + counter[i];
    }

    return counter[0];
}

void Selective::selectiveBU(struct Graph* graph)
{
	uint32_t no_of_nodes = graph->no_of_nodes;
    uint32_t no_of_edges = graph->no_of_edges;
    uint32_t *h_graph_nodes = graph->nodes;
    uint32_t *h_graph_edges = graph->edges;
    uint32_t source = graph->source;

    char* h_graph_mask;
    int*  h_cost;
    int*  h_frontier 	= new int [no_of_nodes];

	// GPU INICIALIZATION

	char kernel_file[100]  = "Kernels.cl";
    int total_kernels = 3;
    std::string kernel_names[3] = {"BFS_mask", "BFS_mask_buttom", "BFS_mask_buttom_clean"};
	_clInit(kernel_file, total_kernels, kernel_names);

	cl_mem d_graph_nodes = _clMalloc( (no_of_nodes+1)*sizeof(uint32_t), h_graph_nodes);
	cl_mem d_graph_edges = _clMalloc(no_of_edges*sizeof(uint32_t), h_graph_edges);
	cl_mem d_graph_mask1 = _clMallocRWZC(no_of_nodes*sizeof(char), NULL);
	cl_mem d_graph_mask2 = _clMallocRWZC(no_of_nodes*sizeof(char), NULL);
	cl_mem d_cost 		 = _clMallocRWZC(no_of_nodes*sizeof(int),  h_cost);

	// END GPU INIT

	// GPU WARM UP
	try {
		int idx_aux = 0;
		int level_aux = 1;
        _clSetArgs(0, idx_aux++, d_graph_nodes);
        _clSetArgs(0, idx_aux++, d_graph_edges);
		_clSetArgs(0, idx_aux++, d_graph_mask1);
        _clSetArgs(0, idx_aux++, d_graph_mask2);
        _clSetArgs(0, idx_aux++, d_cost);
        _clSetArgs(0, idx_aux++, &level_aux, sizeof(int));
        _clSetArgs(0, idx_aux++, &no_of_nodes, sizeof(int));
        _clInvokeKernel(0, (no_of_nodes), MAX_THREADS_PER_BLOCK);

        _clFinish();

	    h_cost = (int*)clEnqueueMapBuffer(getOCLHandler()->queue, d_cost,
				CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, no_of_nodes*sizeof(int), 0, NULL, NULL, NULL);

		h_graph_mask = (char*)clEnqueueMapBuffer(getOCLHandler()->queue, d_graph_mask2,
				CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, no_of_nodes*sizeof(char), 0, NULL, NULL, NULL);

		_clFinish();

		for (uint32_t i = 0; i < no_of_nodes; ++i)
	    {
	        h_graph_mask[i] = false;
	    }

	    clEnqueueUnmapMemObject(getOCLHandler()->queue, d_graph_mask2, h_graph_mask, 0, NULL, NULL);

		h_graph_mask = (char*)clEnqueueMapBuffer(getOCLHandler()->queue, d_graph_mask1,
				CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, no_of_nodes*sizeof(char), 0, NULL, NULL, NULL);

		_clFinish();

 	}
    catch (std::string& s){
        std::cout << "Exception: " << s << std::endl;
    }


	for (uint32_t i = 0; i < no_of_nodes; ++i)
    {
        h_graph_mask[i] = false;
        h_cost[i]       = NODE_UNVISITED;
    }

	h_graph_mask[source]=true;
    h_cost[source] = 0;

#ifdef	PROFILING
	startPowerMeasure();
	ChronoCpu chrono_total("total_timer");
	chrono_total.tic();
#endif

	uint32_t procNodes = 0;
	uint32_t level = 0;
	bool flag_gpu = false;
	int totalSteps = 4;
	bool flagMask = true;

	while(true)
	{
		if ( ((double)procNodes) / no_of_nodes < 0.1 || flag_gpu ) // 10% of the graph or more, big enough for scale free.
		{
			++level;
			int counter = createFrontier(h_graph_mask, h_cost, h_frontier, no_of_nodes);
			bool finish = run_single_bfs_cpu(graph, h_graph_mask, h_cost, h_frontier, counter, level);

			procNodes += counter;
			// std::cout << counter << std::endl;
			if (finish)
			{
				break;
			}
		}
		else
		{
			flag_gpu = true;

			clEnqueueUnmapMemObject(getOCLHandler()->queue, d_cost, h_cost, 0, NULL, NULL);
			clEnqueueUnmapMemObject(getOCLHandler()->queue, d_graph_mask1, h_graph_mask, 0, NULL, NULL);

			int steps = 0;
            //bool h_over = false;

            while( steps < totalSteps)
            {
            	level++;
            	steps++;

	            int kernel_id = 1;
	            int kernel_idx = 0;
	            _clSetArgs(kernel_id, kernel_idx++, d_graph_nodes);
	            _clSetArgs(kernel_id, kernel_idx++, d_graph_edges);

	            if (flagMask)
                {
                    _clSetArgs(kernel_id, kernel_idx++, d_graph_mask1);
                    _clSetArgs(kernel_id, kernel_idx++, d_graph_mask2);
                }
                else
                {
                    _clSetArgs(kernel_id, kernel_idx++, d_graph_mask2);
                    _clSetArgs(kernel_id, kernel_idx++, d_graph_mask1);
                }

                flagMask = !flagMask;

	            _clSetArgs(kernel_id, kernel_idx++, d_cost);
	            _clSetArgs(kernel_id, kernel_idx++, &level, sizeof(int) );
	            _clSetArgs(kernel_id, kernel_idx++, &no_of_nodes, sizeof(int));

	            _clInvokeKernel(kernel_id, no_of_nodes, MAX_THREADS_PER_BLOCK);

	            // if (steps == 1)
	            {
	            	kernel_id = 2;
		            kernel_idx = 0;
		           	if (!flagMask)
	                {
	                    _clSetArgs(kernel_id, kernel_idx++, d_graph_mask1);
	                }
	                else
	                {
	                    _clSetArgs(kernel_id, kernel_idx++, d_graph_mask2);
	                }
		            _clSetArgs(kernel_id, kernel_idx++, &no_of_nodes, sizeof(int));
		            _clInvokeKernel(kernel_id, no_of_nodes, MAX_THREADS_PER_BLOCK);
	            }

            }

			h_cost = (int*)clEnqueueMapBuffer(getOCLHandler()->queue, d_cost,
				CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, no_of_nodes*sizeof(int), 0, NULL, NULL, NULL);

            if (!flagMask)
            {
            	h_graph_mask = (char*)clEnqueueMapBuffer(getOCLHandler()->queue, d_graph_mask2,
					CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, no_of_nodes*sizeof(char), 0, NULL, NULL, NULL);
            }
            else{
            	h_graph_mask = (char*)clEnqueueMapBuffer(getOCLHandler()->queue, d_graph_mask1,
					CL_FALSE, CL_MAP_WRITE | CL_MAP_READ, 0, no_of_nodes*sizeof(char), 0, NULL, NULL, NULL);
            }

            _clFinish();
            // std::cout << 1.0f-(double(counteUnvisitedNodes(graph, h_cost)) / double(graph->no_of_nodes)) << std::endl;
            // exit(0);

            if ( 1.0f-(double(counteUnvisitedNodes(graph, h_cost)) / double(graph->no_of_nodes)) < 0.85f)
            {
            	// std::cout<< "vamos por el segundo " << std::endl;
            	// std::cout << 1.0f-(double(counteUnvisitedNodes(graph, h_cost)) / double(graph->no_of_nodes)) << std::endl;
            	flag_gpu = false;
            	totalSteps = totalSteps / 2;
            }
		}
	}

#ifdef	PROFILING
	chrono_total.tac();
	last_run_ms = chrono_total.getElapsedStats().totalTime_ms;
	endPowerMeasure();
#endif

	//--result varification
	// serial_correctness(graph, h_cost);

	//--4 release cl resources.
	_clFree(d_graph_nodes);
	_clFree(d_graph_edges);
	_clFree(d_graph_mask1);
	_clFree(d_graph_mask2);
	_clFree(d_cost);
	_clRelease();

    delete[] h_frontier;

	return ;
}

