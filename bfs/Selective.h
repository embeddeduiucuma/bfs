/* ============================================================
University of Illinois at Urbana Champaign
Breadth-First-Search Optimized
Author: Luis Remis
Date:	Apr 5 2015
============================================================ */
#ifndef _Selective_H_
#define _Selective_H_

#include "BFS_base.h"

class Selective : public BFS_base
{

public:
	Selective(unsigned int threshold, unsigned int num_omp_threads);
	void run(struct Graph* graph);

private:

	void selective(struct Graph* graph);
	void selectiveBU(struct Graph* graph);

	uint32_t counteUnvisitedNodes(struct Graph* graph, int* cost);


};

#endif